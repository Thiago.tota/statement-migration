﻿using System.Collections.Generic;

namespace AplMigrationBal.Helper.Extensions
{
    public static class StringExtensions
    {
        static List<string> badChars = new List<string> { "\0", "\u0000", "\u001d", "\u0001", "\u001a", "\u007f", "\u0013", "\u0014", "\u0016", "#REF!", "ë" };

        public static string Clean(this string value)
        {            
            foreach (var i in badChars)
                value = value.Replace(i, "");

            return value.Trim();
        }

        public static string CleanWithSpaces(this string value)
        {
            foreach (var i in badChars)
                value = value.Replace(i, "");

            return value;
        }

        private static string IdToString(string id, char type)
        {
            var padSizeAdd = type.Equals('G') ? 0 : 2;

            return id.ToString().PadLeft((6 + padSizeAdd), '0');
        }

        public static string ClientIdToString(this string value)
        {
            return IdToString(value, 'C');
        }

        public static string GroupIdToString(this string value)
        {
            return IdToString(value, 'G');
        }
    }
}

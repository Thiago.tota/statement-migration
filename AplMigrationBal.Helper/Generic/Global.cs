﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace AplMigrationBal.Helper.Generic
{
    public static class Global
    {
        private static Enum.Environment Environment { get; set; }

        public static string NewMongoGuid => Guid.NewGuid().ToString().Replace("-", "").Substring(0, 24);

        public static string CollectionTempStatementToProcess => "tempStatementToProcess";

        public static string CollectionTempExplanatoryNoteToProcess => "tempExplanatoryNoteToProcess";

        public static string CollectionAccountPlan => "planocontas";

        public static object CollectionAccountPlanFromTo => "accoutPlanFromTo";

        public static string StatementSkipedMsg => "Descartado;Cliente tipo;{0};Identificador;{1};Referência;{2}/{3};Tipo;{4};Situação;{5};não salvo devido a existência do registro; Cliente tipo;{6};Identificador;{7};Referência;{8}/{9};Tipo;{10};Situação;{11}";

        public static string StatementErrorMsg => "Erro;Cliente tipo;{0};Identificador;{1};Referência;{2}/{3};Tipo;{4};Situação;{5};não salvo devido a erro desconhecido";

        public static string StatementSavedMsg => "Sucesso;Cliente tipo;{0};Identificador;{1};Referência;{2}/{3};Tipo;{4};Situação;{5};salvo com sucesso";

        //Parameters --> ClienteType;ClientId;Reference;Type;Status
        public static string StatementCheckNotFoundMsg => "{0};{1};{2};{3};{4};Não encontrado.";

        //Parameters --> ClienteType;ClientId;Reference;Type;Status;Origem;Fonte;AccountId;ExpecetedValue;
        public static string StatementCheckAccountNotFoundMsg => "{0};{1};{2};{3};{4};{5};{6};Conta não encontrada;{7};Valor esperado;{8}";

        //Parameters --> ClienteType;ClientId;Reference;Type;Status;Fonte;Group;AccountId;ExpecetedValue;FoundValue
        public static string StatementCheckValueErrorMsg => "{0};{1};{2};{3};{4};{5};{6}Conta divergente;{7}; Valor esperado;{8};Valor encontrado;{9}";


        public static void SetEnvironment(Enum.Environment environment)
        {
            Environment = environment;
        }

        public static string ApiUrlBal
        {
            get
            {
                switch (Environment)
                {
                    case Enum.Environment.Homologacao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Api HOMOL"];
                    case Enum.Environment.Producao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Api PROD"];
                    default:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Api TI"];
                }
            }
        }

        public static string MongoDbUrlBal
        {
            get
            {
                switch (Environment)
                {
                    case Enum.Environment.Homologacao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Url HOMOL"];
                    case Enum.Environment.Producao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Url PROD"];
                    default:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Url TI"];
                }
            }
        }

        public static string MongoDbDatabaseNameBal
        {
            get
            {
                switch (Environment)
                {
                    case Enum.Environment.Homologacao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.DatabaseName HOMOL"];
                    case Enum.Environment.Producao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.DatabaseName PROD"];
                    default:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.DatabaseName TI"];
                }
            }
        }

        public static string MongoCollectionNameBal
        {
            get
            {
                switch (Environment)
                {
                    case Enum.Environment.Homologacao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Collection HOMOL"];
                    case Enum.Environment.Producao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Collection PROD"];
                    default:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.Statement.Collection TI"];
                }
            }
        }

        public static string AccountPlanUrlApiBal
        {
            get
            {
                switch (Environment)
                {
                    case Enum.Environment.Homologacao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.AccountPlan.Url HOMOL"];
                    case Enum.Environment.Producao:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.AccountPlan.Url PROD"];
                    default:
                        return ConfigurationManager.AppSettings["AplMigrationBal.Comunication.AccountPlan.Url TI"];
                }
            }
        }

        private static string basePath = string.Empty;
        public static string BasePath
        {
            get
            {
                if (string.IsNullOrEmpty(basePath))
                    if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["BasePath"]))
                        basePath = System.Environment.CurrentDirectory + @"\";
                    else
                        basePath = ConfigurationManager.AppSettings["BasePath"];

                return basePath;
            }
            set
            {
                basePath = value;
            }
        }

        public static string SummaryFileName
        {
            get
            {
                string summaryPath = BasePath + ConfigurationManager.AppSettings["ReportPath"];
                if (!Directory.Exists(summaryPath))
                    Directory.CreateDirectory(summaryPath);

                return summaryPath + string.Format("SumarioImportacao_{0}.txt", DateTime.Now.ToString().Replace("/", "").Replace(":", ""));
            }
        }

        public static string StatementCheckSummaryFileName(string aditionalText = "")
        {
            string summaryPath = BasePath + ConfigurationManager.AppSettings["ReportPath"];
            if (!Directory.Exists(summaryPath))
                Directory.CreateDirectory(summaryPath);

            return summaryPath + string.Format("RelatorioBatimento_{0}{1}.txt", aditionalText, DateTime.Now.ToString().Replace("/", "").Replace(":", ""));
        }

        public static string StatementRiskFileName(string aditionalText = "")
        {
            string summaryPath = BasePath + ConfigurationManager.AppSettings["ReportPath"];
            if (!Directory.Exists(summaryPath))
                Directory.CreateDirectory(summaryPath);

            return summaryPath + string.Format("ArquivoRisco_{0}{1}.txt", aditionalText, DateTime.Now.ToString().Replace("/", "").Replace(":", ""));
        }

        private static int bufferBalanco = 0;
        public static int BufferStatementToSave
        {
            get
            {
                if (bufferBalanco == 0)
                {
                    bufferBalanco = int.TryParse(ConfigurationManager.AppSettings["BufferStatementToSave"], out int aux) ? aux : 1;
                }
                return bufferBalanco;
            }
            set
            {
                bufferBalanco = value;
            }
        }

        private static int bufferStatementToProcess = 0;
        public static int BufferStatementToProcess
        {
            get
            {
                if (bufferStatementToProcess == 0)
                {
                    bufferStatementToProcess = int.TryParse(ConfigurationManager.AppSettings["BufferStatementsToProcess"], out int aux) ? aux : bufferBalanco;
                }
                return bufferStatementToProcess;
            }
            set
            {
                bufferStatementToProcess = value;
            }
        }

        private static bool? jsonOutputSave = null;
        public static bool JsonOutputSave
        {
            get
            {
                if (jsonOutputSave == null)
                {
                    jsonOutputSave = bool.TryParse(ConfigurationManager.AppSettings["SaveOutputJsonFile"], out bool aux) ? aux : false;
                }
                return (bool)jsonOutputSave;
            }
        }

        private static string jsonOutputPath = string.Empty;
        public static string JsonOutputPath
        {
            get
            {
                if (string.IsNullOrEmpty(jsonOutputPath))
                    jsonOutputPath = BasePath + ConfigurationManager.AppSettings["JsonOutputPath"];

                return jsonOutputPath;
            }
            set
            {
                jsonOutputPath = value;
            }
        }

        public static string GetJsonOutputName(string identificacao = "")
        {
            StringBuilder sb = new StringBuilder();

            if (!Directory.Exists(JsonOutputPath))
                Directory.CreateDirectory(JsonOutputPath);

            sb.Append(jsonOutputPath);
            if (string.IsNullOrEmpty(identificacao))
                sb.Append(string.Format("{0}_{1}.json", DateTime.Now.ToString().Replace("/", "").Replace(":", ""), Guid.NewGuid()));
            else
            {
                sb.Append(string.Format("{0}_{1}_{2}.json", identificacao, DateTime.Now.ToString().Replace("/", "").Replace(":", ""), Guid.NewGuid()));
            }

            return sb.ToString();
        }

        public static string GetErrorStatementFilePath(string statement)
        {
            string path = BasePath + @"\Error\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return string.Concat(path, statement.Substring(statement.LastIndexOf('\\')));
        }

        private static string accountPlanPath = string.Empty;
        public static string AccountPlanPath
        {
            get
            {
                if (string.IsNullOrEmpty(accountPlanPath))
                    accountPlanPath = BasePath + ConfigurationManager.AppSettings["AccountPlanPath"];

                if (!Directory.Exists(accountPlanPath))
                    Directory.CreateDirectory(accountPlanPath);

                return accountPlanPath;
            }
            set
            {
                accountPlanPath = value;
            }
        }

        private static string dataPath = string.Empty;
        public static string DataPath
        {
            get
            {
                if (string.IsNullOrEmpty(dataPath))
                    dataPath = BasePath + ConfigurationManager.AppSettings["DataPath"];

                return dataPath;
            }
            set
            {
                dataPath = value;
            }
        }

        public static Enum.ComunicationBy SaveStatementBy
        {
            get
            {
                return (Enum.ComunicationBy)System.Enum.Parse(typeof(Enum.ComunicationBy), ConfigurationManager.AppSettings["SavetatementBy"], true);
            }
        }

        public static Enum.ComunicationBy AccountPlanBy
        {
            get
            {
                return (Enum.ComunicationBy)System.Enum.Parse(typeof(Enum.ComunicationBy), ConfigurationManager.AppSettings["AccountPlanBy"], true);
            }
        }

        private static string statementDatePath = string.Empty;
        public static string StatementDatePath
        {
            get
            {
                if (string.IsNullOrEmpty(statementDatePath))
                    statementDatePath = BasePath + ConfigurationManager.AppSettings["StatementDatePath"];

                return statementDatePath;
            }
            set
            {
                statementDatePath = value;
            }
        }

        private static string statementDateFile = string.Empty;
        public static string StatementDateFile
        {
            get
            {
                if (string.IsNullOrEmpty(statementDateFile))
                    statementDateFile = ConfigurationManager.AppSettings["StatementDateFile"];

                return statementDateFile;
            }
            set
            {
                statementDateFile = value;
            }
        }
    }
}

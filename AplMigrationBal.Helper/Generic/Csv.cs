﻿using AplMigrationBal.Helper.Interface;
using System.Collections.Generic;
using System.Linq;

namespace AplMigrationBal.Helper.Generic
{
    public static class Csv
    {
        public static List<T> GetListFromCsv<T>(string fullFieName, char[] separators, bool skipHeader = false) where T : ICsv, new()
        {
            if (!System.IO.File.Exists(fullFieName))
                return null;

            int skip = skipHeader ? 1 : 0;

            var obj = new T();

            List<T> list = new List<T>(System.IO.File.ReadLines(fullFieName, Util.GetEncoding(fullFieName)).Skip(skip).Select(f => obj.FromCsv<T>(f, separators)));

            return list;
        }

        public static bool CreateCsv<T>(List<T> data, string fullFileName, char separator) where T : ICsv, new()
        {
            var obj = new T();
            List<string> dataToWrite = new List<string>();

            dataToWrite.Add(obj.WriteHeader(separator));
            data.ForEach(f => dataToWrite.Add(f.WriteCsv(separator)));

            System.IO.File.WriteAllLines(fullFileName, dataToWrite);

            return true;
        }
    }
}

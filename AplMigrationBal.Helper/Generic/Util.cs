﻿using System.IO;
using System.Text;

namespace AplMigrationBal.Helper.Generic
{
    public static class Util
    {
        public static Encoding GetEncoding(string fullFieName)
        {
            Encoding encoding = Encoding.Default;

            using (var file = new StreamReader(fullFieName, encoding))
            {
                encoding = file.CurrentEncoding;
            }

            return encoding;
        }
    }
}

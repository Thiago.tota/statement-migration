﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AplMigrationBal.Helper.Generic
{
    public static class Json
    {
        public static string Serialize(object data, Enum.JsonFormatting formatting = Enum.JsonFormatting.None)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();

            jsonSerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            // jsonSerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            return JsonConvert.SerializeObject(data, (Formatting)System.Enum.Parse(typeof(Formatting), formatting.ToString(), true), jsonSerializerSettings);
        }

        public static T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}

﻿using AplMigrationBal.Helper.Logger;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace AplMigrationBal.Helper.Generic
{
    public static class File
    {
        public static bool WriteFile(string fileName, string fileData, bool append = false)
        {
            bool result = false;
            try
            {
                StreamWriter sw = new StreamWriter(fileName, append);

                if (append)
                    sw.WriteLine(fileData);
                else
                    sw.Write(fileData);

                sw.Close();

                result = true;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        public static IEnumerable<string> ReadFile(string fullFieName)
        {
            IEnumerable<string> result = null;

            try
            {
                if (System.IO.File.Exists(fullFieName))
                    result = System.IO.File.ReadLines(fullFieName, Util.GetEncoding(fullFieName));
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        public static bool DeleteFile(string fullFieName)
        {
            bool result = false;
            try
            {
                if (System.IO.File.Exists(fullFieName))
                    System.IO.File.Delete(fullFieName);

                result = true;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }
    }
}

﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplMigrationBal.Helper.Serializer
{
    public class CustomDateTimeSerializer:DateTimeSerializer
    {
        public override DateTime Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var obj = base.Deserialize(context, args);

            return new DateTime(obj.Year, obj.Month, obj.Day, 0, 0, 0, DateTimeKind.Utc);
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, DateTime value)
        {
            var utcValue = new DateTime(value.Year, value.Month, value.Day, 0, 0, 0, DateTimeKind.Utc);
            base.Serialize(context, args, utcValue);
        }
    }
}

﻿using AplMigrationBal.Helper.Logger;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Diagnostics;

namespace AplMigrationBal.Helper.Serializer
{
    public sealed class StringOrNumericSerializer : IBsonSerializer
    {
        public Type ValueType { get; } = typeof(string);

        public object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            try
            {
                var bsonType = context.Reader.GetCurrentBsonType();
                switch (bsonType)
                {
                    case MongoDB.Bson.BsonType.Null:
                        context.Reader.ReadNull();
                        return null;
                    case MongoDB.Bson.BsonType.String:
                        return context.Reader.ReadString();
                    case MongoDB.Bson.BsonType.Int32:
                        return context.Reader.ReadInt32().ToString();
                    case MongoDB.Bson.BsonType.Int64:
                        return context.Reader.ReadInt64().ToString();
                    case MongoDB.Bson.BsonType.Decimal128:
                        return context.Reader.ReadDecimal128().ToString();
                    case MongoDB.Bson.BsonType.Double:
                        return context.Reader.ReadDouble().ToString();
                    case MongoDB.Bson.BsonType.Document:
                        context.Reader.ReadStartDocument();
                        context.Reader.ReadString();
                        context.Reader.ReadEndDocument();
                        return string.Empty;
                    default:
                        throw new BsonSerializationException(string.Format("Cannot deserialize from {0}", context.Reader.CurrentBsonType));
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                return string.Empty;
            }
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            if (value != null)
                context.Writer.WriteString(value.ToString());
            else
                context.Writer.WriteNull();
        }
    }
}

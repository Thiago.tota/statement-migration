﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;

namespace AplMigrationBal.Helper.Serializer
{
    public sealed class BsonArraySerializer : IBsonSerializer
    {
        public Type ValueType { get; } = typeof(Array);

        public object Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var bsonType = context.Reader.GetCurrentBsonType();
            switch (bsonType)
            {
                case MongoDB.Bson.BsonType.Array:
                    context.Reader.ReadStartArray();
                    var array = new BsonArray();

                    while (context.Reader.ReadBsonType() != BsonType.EndOfDocument)
                    {
                        var val = context.Reader.ReadString();
                        array.Add(val);
                    }
                    context.Reader.ReadEndArray();
                    return array;
                default:
                    return context;
                    throw new BsonSerializationException(string.Format("Cannot deserialize from {0}", context.Reader.CurrentBsonType));
            }
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            if (value != null)
                context.Writer.WriteString(value.ToString());
            else
                context.Writer.WriteNull();
        }
    }
}

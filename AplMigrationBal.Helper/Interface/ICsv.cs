﻿namespace AplMigrationBal.Helper.Interface
{
    public interface ICsv
    {
        T FromCsv<T>(string line, char[] separator) where T : new();
        string WriteCsv(char separator);
        string WriteHeader(char separator);
    }
}

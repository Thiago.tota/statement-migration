﻿using NLog;

namespace AplMigrationBal.Helper.Logger
{
    public static class Log
    {
        public static NLog.Logger Instance { get; private set; }

        static Log()
        {
            LogManager.ReconfigExistingLoggers();
            Instance = LogManager.GetCurrentClassLogger();
        }
    }
}

﻿namespace AplMigrationBal.Helper.Enum
{
    //
    // Summary:
    //     Specifies formatting options for the Newtonsoft.Json.JsonTextWriter.
    public enum JsonFormatting
    {
        //
        // Summary:
        //     No special formatting is applied. This is the default.
        None = 0,
        //
        // Summary:
        //     Causes child objects to be indented according to the Newtonsoft.Json.JsonTextWriter.Indentation
        //     and Newtonsoft.Json.JsonTextWriter.IndentChar settings.
        Indented = 1
    }
}

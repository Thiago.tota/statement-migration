﻿namespace AplMigrationBal.Helper.Enum
{
    public enum StatementStatus : byte
    {
        Todos = 0,
        Definitivo,
        Provisorio,
        Provisório
    }
}

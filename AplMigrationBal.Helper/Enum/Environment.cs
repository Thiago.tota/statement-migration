﻿namespace AplMigrationBal.Helper.Enum
{
    public enum Environment : byte
    {
        TI = 0,
        Homologacao,
        Producao
    }
}

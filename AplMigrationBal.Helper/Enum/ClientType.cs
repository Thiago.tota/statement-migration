﻿using System.ComponentModel;

namespace AplMigrationBal.Helper.Enum
{
    public enum ClienteType : byte
    {
        [Description("Empresa")]
        E,
        [Description("GrupoEconomico")]
        G,
        [Description("ControladorEstrangeiro")]
        C,
        Todos
    }
}

﻿namespace AplMigrationBal.Helper.Enum
{
    public enum StatementToSaveAction : byte
    {
        ContinueValidation = 0,
        Discard,
        Save
    }
}

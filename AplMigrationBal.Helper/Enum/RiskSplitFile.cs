﻿namespace AplMigrationBal.Helper.Enum
{
    public enum RiskSplitFile : byte
    {
        Y = 0, //yearly
        S,     //semiannually
        Q,     //quarterly
        M      //monthly
    }
}

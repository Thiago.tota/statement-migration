﻿namespace AplMigrationBal.Helper.Enum
{
    public enum AccountGroup : byte
    {
        Ativo,
        Passivo,
        Dre,
        Mutacao,
        FluxoCaixa,
        Indicadores,
        Highlights
    }
}

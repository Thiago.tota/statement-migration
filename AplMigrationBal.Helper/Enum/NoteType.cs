﻿namespace AplMigrationBal.Helper.Enum
{
    public enum NoteType : byte
    {
        NotaExplicativa,
        Auditoria
    }
}

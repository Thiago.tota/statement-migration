﻿namespace AplMigrationBal.Helper.Enum
{
    public enum StatementSource : byte
    {
        Default = 0,
        upload,
        Upload,
        serasa,
        Serasa,
        ImportacaoMainframe,
        ImportacaoSQL,
        ImportacaoSerasa
    }
}

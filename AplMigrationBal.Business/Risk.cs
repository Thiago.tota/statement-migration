﻿using AplMigrationBal.Business.RiskIntegration;
using AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents;
using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AplMigrationBal.Business
{
    public class Risk
    {
        #region Private Properties

        private Statement BusinessStatement = null;
        private RiskIntervalPeriodsOfTimeGenerator IntervalGenerator;

        #endregion

        public Risk(Statement businessStament, RiskIntervalPeriodsOfTimeGenerator intervalGenerator, Helper.Enum.Environment environmentToGenerateFile)
        {
            BusinessStatement = businessStament;
            IntervalGenerator = intervalGenerator;
            Global.SetEnvironment(environmentToGenerateFile);
        }

        public bool GenerateFile()
        {
            try
            {
                List<DateTime> fileDatesSplit = IntervalGenerator.GenerateDates();

                for (int i = 0; i < fileDatesSplit.Count; i = i + 2)
                {
                    StringBuilder stringBuilder = new StringBuilder();
                    string contentLine;
                    int totalRegisters = 0;

                    var statements = BusinessStatement.GetStatementByDateRange(fileDatesSplit[i], fileDatesSplit[i + 1]);

                    RiskContentGeneratorBase header = new HeaderRiskFileContentGenerator(fileDatesSplit[fileDatesSplit.Count - 1]);
                    contentLine = header.GenerateContent();
                    if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                    {
                        stringBuilder.AppendLine(contentLine);
                        totalRegisters++;
                    }

                    if (statements.Any())
                    {
                        statements.ToList().ForEach(item =>
                        {
                            RiskContentGeneratorBase headerClient = new HeaderRiskClientContentGenerator(item);
                            contentLine = headerClient.GenerateContent();
                            if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                            {
                                totalRegisters++;
                                stringBuilder.AppendLine(contentLine);
                            }

                            RiskContentGeneratorBase headerBalance = new HeaderRiskBalanceContentGeneration(item);
                            contentLine = headerBalance.GenerateContent();
                            if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                            {
                                totalRegisters++;
                                stringBuilder.AppendLine(contentLine);
                            }

                            item.Dados.Contas.Ativo.ForEach(account =>
                            {
                                contentLine = new ContentAccountRiskGenerator((IStatement)account, 0).GenerateContent();
                                if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                                {
                                    totalRegisters++;
                                    stringBuilder.AppendLine(contentLine);
                                }
                            });

                            item.Dados.Contas.Passivo.ForEach(account =>
                            {
                                contentLine = new ContentAccountRiskGenerator((IStatement)account, 0).GenerateContent();
                                if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                                {
                                    totalRegisters++;
                                    stringBuilder.AppendLine(contentLine);
                                }
                            });

                            item.Dados.Contas.Dre.ForEach(account =>
                            {
                                contentLine = new ContentAccountRiskGenerator((IStatement)account, 0).GenerateContent();
                                if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                                {
                                    totalRegisters++;
                                    stringBuilder.AppendLine(contentLine);
                                }
                            });

                            item.Dados.Contas.Mutacao.ForEach(account =>
                            {
                                contentLine = new ContentAccountRiskGenerator((IStatement)account, 0).GenerateContent();
                                if (!string.IsNullOrEmpty(contentLine) && !string.IsNullOrWhiteSpace(contentLine))
                                {
                                    totalRegisters++;
                                    stringBuilder.AppendLine(contentLine);
                                }
                            });
                        });
                    }

                    totalRegisters++;
                    stringBuilder.AppendLine(new TrailerRiskContentGenerator(totalRegisters).GenerateContent());

                    string fileName = Global.StatementRiskFileName();
                    File.WriteFile(fileName, Regex.Replace(stringBuilder.ToString(), @"[\r\n]*^\s*$[\r\n]*", string.Empty, RegexOptions.Multiline), true);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                return false;
            }
            return true;
        }

        private char GenerateContentToFile(List<Model.Json.Statement> statements)
        {
            throw new NotImplementedException();
        }
    }
}

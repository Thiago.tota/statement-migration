﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Model.DTO;
using AplMigrationBal.Model.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AplMigrationBal.Business
{
    public class Statement
    {
        #region Private Properties

        Comunication.IComunication comunication;

        #endregion

        #region Constructor

        public Statement()
        {
            comunication = Comunication.Singleton.Instance();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Verifica se o status é Definitivo ou Provisório. Definitivo sobrescreve Provisório.
        /// </summary>
        /// <param name="statementToSave"></param>
        /// <param name="statementSaved"></param>
        /// <returns></returns>
        private Helper.Enum.StatementToSaveAction CheckStatementStatus(Model.DTO.StatementKey statementToSave, Model.DTO.StatementKey statementSaved)
        {
            if (statementSaved.Status.Equals(Helper.Enum.StatementStatus.Provisorio) || statementSaved.Status.Equals(Helper.Enum.StatementStatus.Provisório))
                return Helper.Enum.StatementToSaveAction.Save;
            else if (statementToSave.Status.Equals(Helper.Enum.StatementStatus.Definitivo))
                return Helper.Enum.StatementToSaveAction.Save;
            else
                return Helper.Enum.StatementToSaveAction.Discard;
        }

        /// <summary>
        /// Verifica o tipo é Balanço ou Balancete. Balanço sobrescreve Balancete.
        /// </summary>
        /// <param name="statementToSave"></param>
        /// <param name="statementSaved"></param>
        /// <returns></returns>
        private Helper.Enum.StatementToSaveAction CheckStatementType(Model.DTO.StatementKey statementToSave, Model.DTO.StatementKey statementSaved)
        {
            switch (statementSaved.Type)
            {
                case Helper.Enum.StatementType.Balanco:
                    if (statementToSave.Type.Equals(Helper.Enum.StatementType.Balancete))
                        return Helper.Enum.StatementToSaveAction.Discard;
                    else
                        return Helper.Enum.StatementToSaveAction.ContinueValidation;
                default: //Helper.Enum.StatementType.Balancete:
                    if (statementToSave.Type.Equals(Helper.Enum.StatementType.Balanco))
                        return Helper.Enum.StatementToSaveAction.Save;
                    else
                        return Helper.Enum.StatementToSaveAction.ContinueValidation;
            }
        }

        /// <summary>
        /// Verifica a origem, deve seguir regra de sobrescrita abaixo:
        ///                                             Sobrescrever 
        /// **Fonte**           Serasa | Importação mainframe | Importação SQL | Upload de planilha
        /// Upload de planilha     X   |           X          |         X      |           X
        /// Importação SQL         X   |           X          |         X      |
        /// Importação MF          X   |           X          |                |
        /// Serasa                 X   |                      |                |           
        /// </summary>
        /// <param name="statementToSave"></param>
        /// <param name="statementSaved"></param>
        /// <returns></returns>
        private Helper.Enum.StatementToSaveAction CheckStatementSource(Helper.Enum.StatementSource statementToSave, Model.DTO.StatementKey statementSaved)
        {
            switch (statementSaved.Source)
            {
                case Helper.Enum.StatementSource.upload:
                case Helper.Enum.StatementSource.Upload:
                    return Helper.Enum.StatementToSaveAction.Discard;
                case Helper.Enum.StatementSource.ImportacaoSerasa:
                case Helper.Enum.StatementSource.Serasa:
                case Helper.Enum.StatementSource.serasa:
                    if (statementToSave == Helper.Enum.StatementSource.ImportacaoMainframe || statementToSave == Helper.Enum.StatementSource.ImportacaoSQL)
                        return Helper.Enum.StatementToSaveAction.Save;
                    else
                        return Helper.Enum.StatementToSaveAction.ContinueValidation;
                case Helper.Enum.StatementSource.ImportacaoSQL:
                    if (statementToSave == Helper.Enum.StatementSource.ImportacaoMainframe)
                        return Helper.Enum.StatementToSaveAction.Save;
                    else if (statementToSave == Helper.Enum.StatementSource.ImportacaoSQL)
                        return Helper.Enum.StatementToSaveAction.ContinueValidation;
                    else
                        return Helper.Enum.StatementToSaveAction.Discard;
                default: //Helper.Enum.StatementSource.ImportacaoMainframe:
                    if (statementToSave == Helper.Enum.StatementSource.ImportacaoMainframe)
                        return Helper.Enum.StatementToSaveAction.ContinueValidation;
                    else
                        return Helper.Enum.StatementToSaveAction.Discard;
            }
        }

        #endregion

        #region Public Methods

        public List<Model.DTO.StatementKey> CheckStatementsToSave(List<Model.DTO.StatementKey> statementKeys, Helper.Enum.StatementSource statementSource, out StringBuilder returnMessages)
        {
            List<Model.DTO.StatementKey> keys = new List<Model.DTO.StatementKey>();
            StringBuilder stringBuilder = new StringBuilder();
            Comunication.IComunication comunication = Comunication.Singleton.Instance();
            List<Model.DTO.StatementKey> statementKeysToDelete = new List<StatementKey>();

            var duplicatedKeys = statementKeys.GroupBy(f => new { f.ClientId, f.ClientType, f.Month, f.Year }).Where(g => g.Count() > 1);

            foreach (var item in duplicatedKeys)
            {
                var statements = statementKeys.Where(f => f.ClientId.Equals(item.Key.ClientId) && f.ClientType.Equals(item.Key.ClientType) && f.Month.Equals(item.Key.Month) && f.Year.Equals(item.Key.Year)).ToList();
                var bestStatement = statements.First();

                foreach (var statement in statements.Skip(1))
                {
                    Helper.Enum.StatementToSaveAction action;

                    action = CheckStatementType(statement, bestStatement);

                    if (action.Equals(Helper.Enum.StatementToSaveAction.ContinueValidation))
                        action = CheckStatementStatus(statement, bestStatement);

                    if (action.Equals(Helper.Enum.StatementToSaveAction.Save))
                    {
                        statementKeysToDelete.Add(bestStatement);

                        statementKeys.RemoveAll(f => f.ClientId.Equals(bestStatement.ClientId) && f.ClientType.Equals(bestStatement.ClientType) && f.Month.Equals(bestStatement.Month)
                           && f.Year.Equals(bestStatement.Year) && f.Source.Equals(bestStatement.Source) && f.Type.Equals(bestStatement.Type) && f.Status.Equals(bestStatement.Status));

                        stringBuilder.AppendLine(string.Format(Global.StatementSkipedMsg, bestStatement.ClientType, bestStatement.ClientId, bestStatement.Month, bestStatement.Year, bestStatement.Type, bestStatement.Status,
                           statement.ClientType, statement.ClientId, statement.Month, statement.Year, statement.Type, statement.Status));

                        bestStatement = statement;
                    }
                    else
                    {
                        statementKeysToDelete.Add(statement);

                        statementKeys.RemoveAll(f => f.ClientId.Equals(statement.ClientId) && f.ClientType.Equals(statement.ClientType) && f.Month.Equals(statement.Month)
                            && f.Year.Equals(statement.Year) && f.Source.Equals(statement.Source) && f.Type.Equals(statement.Type) && f.Status.Equals(statement.Status));

                        stringBuilder.AppendLine(string.Format(Global.StatementSkipedMsg, statement.ClientType, statement.ClientId, statement.Month, statement.Year, statement.Type, statement.Status,
                            bestStatement.ClientType, bestStatement.ClientId, bestStatement.Month, bestStatement.Year, bestStatement.Type, bestStatement.Status));
                    }
                }
            }

            List<Model.Json.Statement> savedStatements = comunication.GetStatement(statementKeys, true);

            foreach (var item in statementKeys)
            {
                Model.Json.Statement statement;
                Helper.Enum.StatementToSaveAction action;

                //Não substituir if abaixo por ternário dentro da expressão lambda. Em alguns casos não obedece o filtro da data se usar ternário.
                if (item.ClientType.Equals(Helper.Enum.ClienteType.E))
                {
                    statement = savedStatements.Where(f => f.Dados.Referencia.Equals(new DateTime(item.Year, item.Month, 1, 0, 0, 0, DateTimeKind.Utc))
                                        && f.Cliente.TipoCliente.Equals(item.ClientType)
                                        && f.Cliente.Cnpj.Equals(item.ClientId)).FirstOrDefault();
                }
                else
                {
                    statement = savedStatements.Where(f => f.Dados.Referencia.Equals(new DateTime(item.Year, item.Month, 1, 0, 0, 0, DateTimeKind.Utc))
                                        && f.Cliente.TipoCliente.Equals(item.ClientType)
                                        && f.Cliente.CodigoGrupoEconomico.Equals(item.ClientId)).FirstOrDefault();
                }

                if (statement == null)
                {
                    keys.Add(item);
                    continue;
                }

                action = CheckStatementSource(statementSource, new Model.DTO.StatementKey
                {
                    ClientId = Convert.ToDouble(statement.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? statement.Cliente.Cnpj : statement.Cliente.CodigoGrupoEconomico),
                    ClientType = statement.Cliente.TipoCliente,
                    Month = (byte)statement.Dados.Referencia.Month,
                    Year = statement.Dados.Referencia.Year,
                    Status = statement.Dados.Status,
                    Type = statement.Dados.Tipo,
                    Source = statement.Origem.Fonte
                });

                if (action.Equals(Helper.Enum.StatementToSaveAction.ContinueValidation))
                {
                    action = CheckStatementType(item, new Model.DTO.StatementKey
                    {
                        ClientId = Convert.ToDouble(statement.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? statement.Cliente.Cnpj : statement.Cliente.CodigoGrupoEconomico),
                        ClientType = statement.Cliente.TipoCliente,
                        Month = (byte)statement.Dados.Referencia.Month,
                        Year = statement.Dados.Referencia.Year,
                        Status = statement.Dados.Status,
                        Type = statement.Dados.Tipo,
                        Source = statement.Origem.Fonte
                    });
                }

                if (action.Equals(Helper.Enum.StatementToSaveAction.ContinueValidation))
                {
                    action = CheckStatementStatus(item, new Model.DTO.StatementKey
                    {
                        ClientId = Convert.ToDouble(statement.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? statement.Cliente.Cnpj : statement.Cliente.CodigoGrupoEconomico),
                        ClientType = statement.Cliente.TipoCliente,
                        Month = (byte)statement.Dados.Referencia.Month,
                        Year = statement.Dados.Referencia.Year,
                        Status = statement.Dados.Status,
                        Type = statement.Dados.Tipo,
                        Source = statement.Origem.Fonte
                    });
                }

                if (action.Equals(Helper.Enum.StatementToSaveAction.Save))
                {
                    keys.Add(item);
                    continue;
                }
                else
                {
                    var clientId = statement.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? statement.Cliente.Cnpj : statement.Cliente.CodigoGrupoEconomico;
                    stringBuilder.AppendLine(string.Format(Global.StatementSkipedMsg, item.ClientType, item.ClientId, item.Month, item.Year, item.Type, item.Status,
                        statement.Cliente.TipoCliente, clientId, statement.Dados.Referencia.Month,
                        statement.Dados.Referencia.Year, statement.Dados.Tipo, statement.Dados.Status));
                }
            }

            returnMessages = stringBuilder;
            statementKeysToDelete.AddRange(statementKeys.Except(keys));
            DeleteUnneededStatementKey(statementKeysToDelete);

            return keys;
        }

        /// <summary>
        /// Salva o registro na base
        /// </summary>
        /// <param name="statements"></param>
        /// <returns></returns>
        public bool SaveStatements(List<Model.Json.Statement> statements)
        {
            comunication.SaveStatement(statements);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statements"></param>
        /// <returns></returns>
        public bool LoadStatementsToDataBase(IEnumerable<IStatementDataFile> statements)
        {
            comunication.LoadStatementsToDataBase(statements);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="explanatoryNotes"></param>
        /// <returns></returns>
        public bool LoadExplanatoryNotesToDataBase(IEnumerable<IExplanatoryNoteDataFile> explanatoryNotes)
        {
            if (explanatoryNotes.Count() > 0)
                return comunication.LoadExplanatoryNotesToDataBase(explanatoryNotes);
            else
                return true;
        }

        public List<StatementKey> GetStatementKeys()
        {
            return comunication.GetStatementKeys();
        }

        public StatementToProcess GetStatementToProcess(List<StatementKey> statementKeys)
        {
            return comunication.GetStatementToProcess(statementKeys);
        }

        public List<Model.Json.Statement> GetStatementByDateRange(DateTime initialDate, DateTime endDate)
        {
            return comunication.FindStatmentByDate(initialDate, endDate).ToList();
        }

        public bool DeleteStatementToProcess(List<StatementKey> statementKeys)
        {
            if (statementKeys.Count() > 0)
                return comunication.DeleteStatementToProcess(statementKeys);
            else
                return true;
        }

        public bool CleanStatementToProcess()
        {
            return DeleteDuplicatedStatements() && comunication.CleanStatementToProcess();
        }

        public bool SaveStatementDateFile(string path, string fileName)
        {
            StringBuilder sb = new StringBuilder();

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var result = comunication.GetLatestStatementByClient();

            foreach (var item in result)
            {
                sb.AppendLine((item.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? item.Cliente.Cnpj.ToString().PadLeft(9, '0') : item.Cliente.CodigoGrupoEconomico.ToString().PadLeft(9, '0'))
                    + item.Dados.Referencia.AddMonths(1).AddDays(-1).ToString("yyyyMMdd") + item.Dados.Tipo.ToString().ToUpper().PadRight(9, ' ')
                    + (item.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? "C" : "G") + item.Dados.DataAtualizacao.ToString("yyyyMMdd"));
            }
            Helper.Generic.File.WriteFile(path + fileName, sb.ToString());
            return true;
        }

        public bool DeleteUnneededStatementKey(List<Model.DTO.StatementKey> statementKeys)
        {
            statementKeys.ForEach(s => comunication.DeleteMany<Model.DTO.StatementDataFile>(f => f.Month.Equals(s.Month) && f.Year.Equals(s.Year)
                                && f.ClientId.Equals(s.ClientId) && f.ClientType.Equals(s.ClientType) && f.Status.Equals(s.Status)
                                && f.Type.Equals(s.Type), Global.CollectionTempStatementToProcess));

            statementKeys.ForEach(s => comunication.DeleteMany<Model.DTO.ExplanatoryNoteDataFile>(f => f.Month.Equals(s.Month) && f.Year.Equals(s.Year)
                                && f.ClientId.Equals(s.ClientId) && f.ClientType.Equals(s.ClientType) && f.StatementStatus.Equals(s.Status)
                                && f.StatementType.Equals(s.Type), Global.CollectionTempExplanatoryNoteToProcess));

            return true;
        }

        public bool CheckDuplicatedStatements()
        {
            var statements = comunication.GetDuplicatedStatements();
            return statements.Count() > 0;
        }

        public bool DeleteDuplicatedStatements()
        {
            var statements = comunication.GetDuplicatedStatements();

            while (statements.Count > 0)
            {
                foreach (var item in statements)
                {
                    comunication.DeleteOne<Model.Json.Statement>(d => d.Cliente.TipoCliente == item.Cliente.TipoCliente && d.Cliente.Cnpj == item.Cliente.Cnpj
                                       && d.Cliente.CodigoGrupoEconomico == item.Cliente.CodigoGrupoEconomico && d.Dados.Referencia == item.Dados.Referencia
                                       && d.Dados.Tipo == item.Dados.Tipo && d.Dados.Status == item.Dados.Status, Global.MongoCollectionNameBal);
                }

                statements = comunication.GetDuplicatedStatements();
            }

            return statements.Count() == 0;
        }

        #endregion
    }
}

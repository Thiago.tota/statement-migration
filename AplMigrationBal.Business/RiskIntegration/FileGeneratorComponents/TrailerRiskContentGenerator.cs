﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents
{
    public class TrailerRiskContentGenerator : RiskContentGeneratorBase
    {
        private int quantityRegisters;

        public TrailerRiskContentGenerator(int quantityRegisters):base("99")
        {
            this.quantityRegisters = quantityRegisters;
        }
        public override string GenerateContent()
        {
            return TypeRegister + this.quantityRegisters.ToString().PadLeft(6, '0');
        }
    }
}

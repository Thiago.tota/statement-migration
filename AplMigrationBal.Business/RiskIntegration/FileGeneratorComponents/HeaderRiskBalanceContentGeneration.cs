﻿namespace AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents
{
    public class HeaderRiskBalanceContentGeneration : RiskContentGeneratorBase
    {
        private AplMigrationBal.Model.Json.Statement statment;

        private const string _type_balanco = "BAL";
        private const string _type_balancete = "BLC";

        public HeaderRiskBalanceContentGeneration(AplMigrationBal.Model.Json.Statement statment) : base("02")
        {
            this.statment = statment;
        }
        public override string GenerateContent()
        {
            string tipoBalanco = GetBalanceType();
            string statusBalanco = GetStatusBalance();

            return TypeRegister + statment.Dados.Referencia.Year.ToString().PadLeft(4, '0')
                + statment.Dados.Referencia.Month.ToString().PadLeft(2, '0') + tipoBalanco
                + statment.Origem.Descricao.Substring(0, 3).ToUpper()
                + statment.Dados.DataAtualizacao.ToString("yyyyMMdd")
                + statusBalanco;
        }

        private string GetStatusBalance()
        {
            string statusBalanco = string.Empty;

            switch (statment.Dados.Status)
            {
                case Helper.Enum.StatementStatus.Definitivo:
                    {
                        statusBalanco = "D";
                        break;
                    }

                case Helper.Enum.StatementStatus.Provisorio:
                case Helper.Enum.StatementStatus.Provisório:
                    {
                        statusBalanco = "P";
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

            return statusBalanco;
        }

        private string GetBalanceType()
        {
            string tipoBalanco = string.Empty;

            switch (statment.Dados.Tipo)
            {
                case Helper.Enum.StatementType.Balanco:
                    {
                        tipoBalanco = _type_balanco;
                        break;
                    }
                case Helper.Enum.StatementType.Balancete:
                    {
                        tipoBalanco = _type_balancete;
                        break;
                    }
            }

            return tipoBalanco;
        }
    }
}

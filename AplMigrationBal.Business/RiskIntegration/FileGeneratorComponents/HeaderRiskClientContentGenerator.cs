﻿using AplMigrationBal.Helper.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AplMigrationBal.Model.Json;

namespace AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents
{
    public class HeaderRiskClientContentGenerator : RiskContentGeneratorBase
    {
        private Model.Json.Statement statment;

        private const string _client_type = "C";

        private const string _group_type = "G";

        public HeaderRiskClientContentGenerator(Model.Json.Statement statment):base("01")
        {
            this.statment = statment;
                
        }

        public override string GenerateContent()
        {
            string tipoCliente;
            string codigoCliente;
            switch(statment.Cliente.TipoCliente)
            {
                case Helper.Enum.ClienteType.E:
                    {
                        tipoCliente = _client_type;
                        codigoCliente = statment.Cliente.Cnpj.ToString();
                        break;
                    }
                case Helper.Enum.ClienteType.G:
                    {
                        tipoCliente = _group_type;
                        codigoCliente = statment.Cliente.CodigoGrupoEconomico.ToString();
                        break;
                    }
                default:
                    {
                        tipoCliente = string.Empty;
                        codigoCliente = string.Empty;
                        break;
                    }
            }

            return this.TypeRegister + tipoCliente + codigoCliente.PadLeft(9,'0');
        }
    }
}

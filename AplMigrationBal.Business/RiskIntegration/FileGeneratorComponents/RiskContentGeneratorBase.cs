﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents
{
    public abstract class RiskContentGeneratorBase
    {
        protected string TypeRegister { get; set; }

        public RiskContentGeneratorBase(string typeRegister)
        {
            this.TypeRegister = typeRegister;
        }

        public abstract string GenerateContent();
    }
}

﻿using AplMigrationBal.Model.Interface;
using System;

namespace AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents
{
    public class ContentAccountRiskGenerator : RiskContentGeneratorBase
    {
        private IStatement statment;
        private double? porcentagem;

        public ContentAccountRiskGenerator(IStatement statment, double? porcentagem) : base("03")
        {
            this.statment = statment;
            this.porcentagem = porcentagem;
        }

        public override string GenerateContent()
        {
            string sinal = "+";
            if (statment.Valor.Contains("-"))
            {
                sinal = "-";
                statment.Valor = statment.Valor.Replace("-", "").Trim();
            }

            string valorPorcentual = "000,0";
            if (this.porcentagem > 0)
            {
                valorPorcentual = (Math.Truncate(this.porcentagem.Value) / 10).ToString();

                valorPorcentual = valorPorcentual.PadLeft(5, '0');
            }

            return (TypeRegister + statment.Conta.ToString().PadLeft(5, '0') + sinal + statment.Valor.ToString().PadLeft(13, '0') + valorPorcentual);
        }
    }
}

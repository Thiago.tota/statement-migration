﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents
{
    public class HeaderRiskFileContentGenerator : RiskContentGeneratorBase
    {
        private DateTime baseDate;
        private const string _register_type = "00";

        public HeaderRiskFileContentGenerator(DateTime baseDate):base("00")
        {
            this.baseDate = baseDate;
        }
        public override string GenerateContent()
        {
            return _register_type + baseDate.ToString("yyyyMMdd");
        }
    }
}

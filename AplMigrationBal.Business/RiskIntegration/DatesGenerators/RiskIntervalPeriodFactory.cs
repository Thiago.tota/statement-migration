﻿using AplMigrationBal.Helper.Enum;
using System;

namespace AplMigrationBal.Business.RiskIntegration
{
    public class RiskIntervalPeriodFactory
    {
        public static RiskIntervalPeriodsOfTimeGenerator GetInstance(DateTime baseDate, int periodOfTime, RiskSplitFile riskSpliFile)
        {
            RiskIntervalPeriodsOfTimeGenerator result;

            switch (riskSpliFile)
            {
                case RiskSplitFile.M:
                    {
                        result = new RiskIntervalMonthsGenerator(periodOfTime, baseDate);
                        break;
                    }

                //case RiskSplitFile.Q:
                //    {
                //        result = new RiskIntervalTrimesterGenerator(periodOfTime, baseDate);
                //        break;
                //    }

                case RiskSplitFile.S:
                    {
                        result = new RiskIntervalSemesterGenerator(periodOfTime, baseDate);
                        break;
                    }

                case RiskSplitFile.Y:
                    {
                        result = new RiskIntervalYearsGenerator(periodOfTime, baseDate);
                        break;
                    }

                default:
                    {
                        result = null;
                        break;
                    }
            }

            return result;
        }
    }
}

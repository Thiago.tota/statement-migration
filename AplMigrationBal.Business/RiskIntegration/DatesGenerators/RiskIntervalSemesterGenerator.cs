﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AplMigrationBal.Business.RiskIntegration
{
    public class RiskIntervalSemesterGenerator : RiskIntervalPeriodsOfTimeGenerator
    {
        public RiskIntervalSemesterGenerator(int semesters, DateTime baseDate) : base(semesters, baseDate)
        {

        }

        public override List<DateTime> GenerateDates()
        {
            List<DateTime> results = new List<DateTime>();
            DateTime actualDate = EndDate;

            if (PeriodOfTime == 0)
            {
                int years = actualDate.Year - this.DateOfInitialDatabase.Year;
                PeriodOfTime = (years * 2) + 2;
            }

            for (int i = 0; i < PeriodOfTime; i++)
            {
                if (i == 0)
                {
                    if (actualDate.Month > 6)
                    {
                        results.Add(new DateTime(actualDate.Year, 6, 1, 0, 0, 0));
                    }
                    else
                    {
                        results.Add(new DateTime(actualDate.Year, 1, 1, 0, 0, 0));
                    }

                    results.Add(actualDate);
                    actualDate = actualDate.AddMonths(-6);
                }
                else
                {
                    if (actualDate.Month > 6)
                    {
                        results.Add(new DateTime(actualDate.Year, 6, 1, 0, 0, 0));
                        results.Add(new DateTime(actualDate.Year, 12, DateTime.DaysInMonth(actualDate.Year, 12), 0, 0, 0));
                    }
                    else
                    {
                        results.Add(new DateTime(actualDate.Year, 1, 1));
                        results.Add(new DateTime(actualDate.Year, 5, DateTime.DaysInMonth(actualDate.Year, 5), 0, 0, 0));
                    }

                    actualDate = actualDate.AddMonths(-6);
                }
            }

            return results.OrderBy(p => p).ToList();
        }
    }
}

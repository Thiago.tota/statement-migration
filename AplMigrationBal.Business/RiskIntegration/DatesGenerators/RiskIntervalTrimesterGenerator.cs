﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AplMigrationBal.Business.RiskIntegration
{
    public class RiskIntervalTrimesterGenerator : RiskIntervalPeriodsOfTimeGenerator
    {
        public RiskIntervalTrimesterGenerator(int periodTrimesters, DateTime baseDate) : base(periodTrimesters, baseDate)
        {

        }

        public override List<DateTime> GenerateDates()
        {
            DateTime startMonth;
            List<DateTime> results = new List<DateTime>();

            if (!BeginOfDatabase())
            {
                startMonth = EndDate.AddMonths(-((PeriodOfTime * 3) - 1));
                startMonth = new DateTime(startMonth.Year, startMonth.Month, 1, 0, 0, 0, DateTimeKind.Utc);
            }
            else
            {
                startMonth = DateOfInitialDatabase;
            }

            results.Add(startMonth);

            if (MustGenerateMoreDates())
            {
                DateTime actualMonth = startMonth;

                DateTime endMonth = new DateTime(EndDate.Year, EndDate.Month, 1, 0, 0, 0, DateTimeKind.Utc);

                while (actualMonth < endMonth)
                {
                    actualMonth = actualMonth.AddMonths(3);
                    results.Add(actualMonth);
                }
            }

            results.Add(EndDate);

            return results.OrderBy(p => p).ToList();
        }

        private List<DateTime> GetInitialAndEndOfSemester(DateTime date)
        {
            int month = date.Month;
            List<DateTime> results = new List<DateTime>();

            if (month >= 1 && month <= 3)
            {
                results.Add(new DateTime(date.Year, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            }
            //else if (month >= 4 && month <= 6)
            //{

            //}
            //else if (month >= 7 && month <= 9)
            //{

            //}
            //else
            //{

            //}

            return results;
        }

    }
}

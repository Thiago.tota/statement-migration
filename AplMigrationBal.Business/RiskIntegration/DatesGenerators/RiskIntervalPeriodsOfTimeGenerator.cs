﻿using System;
using System.Collections.Generic;

namespace AplMigrationBal.Business.RiskIntegration
{
    public abstract class RiskIntervalPeriodsOfTimeGenerator
    {
        protected DateTime EndDate { get; set; }
        protected int PeriodOfTime { get; set; }
        protected DateTime DateOfInitialDatabase { get; set; }

        public abstract List<DateTime> GenerateDates();

        /// <summary>
        /// Identify is the algoritm must have generate more then one interval of date
        /// </summary>
        /// <returns></returns>
        protected bool MustGenerateMoreDates()
        {
            return PeriodOfTime != 1 && PeriodOfTime >= 0;
        }

        protected bool BeginOfDatabase()
        {
            return PeriodOfTime == 0;
        }

        public RiskIntervalPeriodsOfTimeGenerator(int periodOfTime, DateTime date)
        {
            PeriodOfTime = periodOfTime;
            EndDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, DateTimeKind.Utc);
            DateOfInitialDatabase = new DateTime(2013, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AplMigrationBal.Business.RiskIntegration
{
    public class RiskIntervalYearsGenerator : RiskIntervalPeriodsOfTimeGenerator
    {
        public RiskIntervalYearsGenerator(int periodYears, DateTime baseDate) : base(periodYears, baseDate)
        {

        }

        public override List<DateTime> GenerateDates()
        {
            DateTime startYear;
            List<DateTime> results = new List<DateTime>();

            if (!BeginOfDatabase())
            {
                startYear = EndDate.AddYears(-(PeriodOfTime - 1));
                startYear = new DateTime(startYear.Year, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            }
            else
            {
                startYear = DateOfInitialDatabase;
            }

            results.Add(startYear);

            if (MustGenerateMoreDates())
            {
                DateTime actualYear = startYear;

                DateTime endYear = new DateTime(EndDate.Year, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                while (actualYear < endYear)
                {
                    results.Add(new DateTime(actualYear.Year, 12, 31, 0, 0, 0, DateTimeKind.Utc));
                    actualYear = actualYear.AddYears(1);
                    results.Add(new DateTime(actualYear.Year, actualYear.Month, actualYear.Day, 0, 0, 0, DateTimeKind.Utc));
                }
            }

            results.Add(EndDate);

            return results.OrderBy(p => p).ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AplMigrationBal.Business.RiskIntegration
{
    public class RiskIntervalMonthsGenerator : RiskIntervalPeriodsOfTimeGenerator
    {
        public RiskIntervalMonthsGenerator(int periodMonths, DateTime baseDate) : base(periodMonths, baseDate)
        {

        }

        public override List<DateTime> GenerateDates()
        {
            DateTime startMonth;
            List<DateTime> results = new List<DateTime>();

            if (!BeginOfDatabase())
            {
                startMonth = EndDate.AddMonths(-(PeriodOfTime - 1));
                startMonth = new DateTime(startMonth.Year, startMonth.Month, 1, 0, 0, 0, DateTimeKind.Utc);
            }
            else
            {
                startMonth = this.DateOfInitialDatabase;
            }

            results.Add(startMonth);

            if (MustGenerateMoreDates())
            {
                DateTime actualMonth = startMonth;

                DateTime endMonth = new DateTime(EndDate.Year, EndDate.Month, 1, 0, 0, 0, DateTimeKind.Utc);

                while (actualMonth < endMonth)
                {
                    DateTime lastDayInCurrentMonth = new DateTime(actualMonth.Year, actualMonth.Month, DateTime.DaysInMonth(actualMonth.Year, actualMonth.Month));
                    results.Add(lastDayInCurrentMonth);

                    actualMonth = actualMonth.AddMonths(1);
                    results.Add(actualMonth);
                }

            }

            results.Add(EndDate);

            return results.OrderBy(p => p).ToList();
        }
    }
}

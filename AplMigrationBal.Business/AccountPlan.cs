﻿using AplMigrationBal.Helper.Enum;
using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace AplMigrationBal.Business
{
    public class AccountPlan
    {

        #region Private Properties

        Comunication.IComunication comunication;

        #endregion

        #region Constructor

        public AccountPlan()
        {
            comunication = Comunication.Singleton.Instance();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accounts"></param>
        /// <returns></returns>
        private Model.Json.AccountPlan CreateAccountPlan(List<Model.DTO.AccountPlanItens> accounts)
        {
            Model.Json.AccountPlan accountPlanJson = new Model.Json.AccountPlan();

            #region Ativo
            accountPlanJson.Group.Ativo.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.Ativo) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.Ativo)
            {
                ProcessChildren(item, accounts);
            }

            #endregion

            #region Passivo

            accountPlanJson.Group.Passivo.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.Passivo) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.Passivo)
            {
                ProcessChildren(item, accounts);
            }

            #endregion

            #region Dre

            accountPlanJson.Group.Dre.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.Dre) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.Dre)
            {
                ProcessChildren(item, accounts);
            }

            #endregion

            #region Mutacao

            accountPlanJson.Group.Mutacao.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.Mutacao) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.Mutacao)
            {
                ProcessChildren(item, accounts);
            }

            #endregion

            #region FluxoCaixa

            accountPlanJson.Group.FluxoCaixa.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.FluxoCaixa) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.FluxoCaixa)
            {
                ProcessChildren(item, accounts);
            }
            #endregion

            #region Indicadores

            accountPlanJson.Group.Indicadores.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.Indicadores) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.Indicadores)
            {
                ProcessChildren(item, accounts);
            }

            #endregion

            #region Highlights

            accountPlanJson.Group.Highlights.AddRange(accounts.Where(f => f.Group.Equals(Helper.Enum.AccountGroup.Highlights) && f.Level.Equals(0)).OrderBy(o => o.Order).Select(s => new Model.Json.Account
            {
                AccountCode = s.Account,
                Blank = s.Blank,
                Description = s.Description,
                Expand = s.Expand,
                Group = s.Group.ToString(),
                Level = s.Level,
                Order = s.Order,
                Parent = s.Parent,
                Show = s.Show
            }));

            foreach (var item in accountPlanJson.Group.Highlights)
            {
                ProcessChildren(item, accounts);
            }

            #endregion

            return accountPlanJson;
        }

        internal IEnumerable<Model.DTO.AccountPlanFromTo> GetAccountPlanFromTo(object collectionAccountPlanFromTo, Helper.Enum.AccountPlanFromTo accountPlanFromTo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Recursive method to process childrem
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="accounts"></param>
        /// <returns></returns>
        private bool ProcessChildren(Model.Json.Account parent, List<Model.DTO.AccountPlanItens> accounts)
        {
            bool result = false;

            try
            {
                foreach (var item in accounts.Where(f => f.Parent.Equals(parent.AccountCode)).OrderBy(o => o.Order))
                {
                    var child = new Model.Json.Account
                    {
                        AccountCode = item.Account,
                        Blank = item.Blank,
                        Description = item.Description,
                        Expand = item.Expand,
                        Group = item.Group.ToString(),
                        Level = item.Level,
                        Order = item.Order,
                        Parent = item.Parent,
                        Show = item.Show
                    };

                    if (accounts.Count(c => c.Parent.Equals(item.Account)) > 0)
                    {
                        ProcessChildren(child, accounts);
                    }

                    parent.Accounts.Add(child);
                }
                result = true;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountPlanJson"></param>
        /// <returns></returns>
        private Model.DTO.AccountPlan CreateAccountPlan(Model.Json.AccountPlan accountPlanJson)
        {
            Model.DTO.AccountPlan accountPlan = new Model.DTO.AccountPlan();
            accountPlan.Name = accountPlanJson.Id;

            #region Ativo
            accountPlanJson.Group.Ativo.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.Ativo)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }

            #endregion

            #region Passivo

            accountPlanJson.Group.Passivo.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.Passivo)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }

            #endregion

            #region Dre

            accountPlanJson.Group.Dre.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.Dre)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }

            #endregion

            #region Mutacao

            accountPlanJson.Group.Mutacao.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.Mutacao)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }

            #endregion

            #region FluxoCaixa

            accountPlanJson.Group.FluxoCaixa.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.FluxoCaixa)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }
            #endregion

            #region Indicadores

            accountPlanJson.Group.Indicadores.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.Indicadores)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }

            #endregion

            #region Highlights

            accountPlanJson.Group.Highlights.ForEach(f => accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
            {
                Account = f.AccountCode,
                Blank = f.Blank,
                Description = f.Description,
                Expand = f.Expand,
                Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), f.Group, true),
                Level = f.Level,
                Order = f.Order,
                Parent = f.Parent,
                Show = f.Show
            }));

            foreach (var item in accountPlanJson.Group.Highlights)
            {
                ProcessChildren(accountPlan, item.Accounts);
            }

            #endregion

            accountPlan.Accounts = accountPlan.Accounts.OrderBy(o => o.Order).ToList();
            return accountPlan;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountPlan"></param>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private bool ProcessChildren(Model.DTO.AccountPlan accountPlan, List<Model.Json.Account> nodes)
        {
            bool result = false;

            try
            {
                if (nodes.Count() > 0)
                {
                    foreach (var item in nodes)
                    {
                        accountPlan.Accounts.Add(new Model.DTO.AccountPlanItens
                        {
                            Account = item.AccountCode,
                            Blank = item.Blank,
                            Description = item.Description,
                            Expand = item.Expand,
                            Group = (Helper.Enum.AccountGroup)Enum.Parse(typeof(Helper.Enum.AccountGroup), item.Group, true),
                            Level = item.Level,
                            Order = item.Order,
                            Parent = item.Parent,
                            Show = item.Show
                        });

                        if (item.Accounts.Count() > 0)
                            ProcessChildren(accountPlan, item.Accounts);
                    };
                }
                result = true;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }
        #endregion

        #region Public Methods
        public Model.Json.AccountPlan GetAccountPlanJson(string collectionAccountPlan = "")
        {
            Model.Json.AccountPlan accountPlanJson = new Model.Json.AccountPlan();

            switch (Global.AccountPlanBy)
            {
                case ComunicationBy.MongoDb:
                    accountPlanJson = comunication.Find<Model.Json.AccountPlan>(f => !f.Id.Equals(null), collectionAccountPlan).OrderByDescending(o => o.CreatedAt).ThenByDescending(o => o.UpdatedAt).FirstOrDefault();
                    break;
                case ComunicationBy.WebAPI:
                    accountPlanJson = new Comunication.AccountPlan().GetAccountPlan<Model.Json.AccountPlan>(Global.AccountPlanUrlApiBal);
                    break;
            }

            return accountPlanJson;
        }

        public Model.DTO.AccountPlan GetAccountPlan(string collectionAccountPlan = "")
        {
            Model.Json.AccountPlan accountPlanJson = new Model.Json.AccountPlan();

            switch (Global.AccountPlanBy)
            {
                case ComunicationBy.MongoDb:
                    accountPlanJson = comunication.Find<Model.Json.AccountPlan>(f => !f.Id.Equals(null), collectionAccountPlan).OrderByDescending(o => o.CreatedAt).ThenByDescending(o => o.UpdatedAt).FirstOrDefault();
                    break;
                case ComunicationBy.WebAPI:
                    accountPlanJson = new Comunication.AccountPlan().GetAccountPlan<Model.Json.AccountPlan>(Global.AccountPlanUrlApiBal);
                    break;
            }

            return CreateAccountPlan(accountPlanJson);
        }

        public Model.Json.AccountPlan GenerateAccountPlain(string fullPath)
        {
            var accountPlan = Csv.GetListFromCsv<Model.DTO.AccountPlanItens>(fullPath, new char[] { ';' }, true);

            Model.Json.AccountPlan accountPlanJson = CreateAccountPlan(accountPlan);
            accountPlanJson._id = Global.NewMongoGuid;
            accountPlanJson.CreatedAt = DateTime.UtcNow;
            accountPlanJson.UpdatedAt = DateTime.UtcNow;

            accountPlanJson.Id = fullPath.Substring(fullPath.LastIndexOf('\\') + 1);
            if (accountPlanJson.Id.Substring(accountPlanJson.Id.Length - 4).ToLower().Equals(".csv"))
                accountPlanJson.Id = accountPlanJson.Id.Substring(0, accountPlanJson.Id.Length - 4);

            return accountPlanJson;
        }

        public bool SaveAccountPlan(Model.Json.AccountPlan accountPlanJson, string collectionAccountPlan, bool async)
        {
            if (comunication.Find<Model.Json.AccountPlan>(f => f.Id.Equals(accountPlanJson.Id), collectionAccountPlan).Count() > 0)
            {
                List<KeyValuePair<Expression<Func<Model.Json.AccountPlan, object>>, object>> keyValuePairs = new List<KeyValuePair<Expression<Func<Model.Json.AccountPlan, object>>, object>>
                {
                    new KeyValuePair<Expression<Func<Model.Json.AccountPlan, object>>, object>(f => f.Id, string.Format("{0}_{1}", accountPlanJson.Id, DateTime.UtcNow)),
                    new KeyValuePair<Expression<Func<Model.Json.AccountPlan, object>>, object>(f => f.UpdatedAt, string.Format("{0}", accountPlanJson.UpdatedAt))
                };

                comunication.UpdateOne(f => f.Id.Equals(accountPlanJson.Id), keyValuePairs, collectionAccountPlan, false);
            }

            return comunication.SaveOne(accountPlanJson, collectionAccountPlan, async);
        }

        public bool SaveAccountPlan(Model.Json.AccountPlan accountPlanJson, string fullFileName)
        {
            return Csv.CreateCsv(CreateAccountPlan(accountPlanJson).Accounts, fullFileName, ';');
        }

        #endregion
    }
}

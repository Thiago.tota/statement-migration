﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO;
using AplMigrationBal.Model.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace AplMigrationBal.Business
{
    public class Import
    {
        #region Private properties

        private Statement businessStatement = null;
        private Summary summary;
        private IEnumerable<IStatementDataFile> StatementData = null;
        private IEnumerable<IExplanatoryNoteDataFile> NoteData = null;

        private Model.DTO.AccountPlan AccountPlanData = new Model.DTO.AccountPlan();
        private IEnumerable<Model.DTO.AccountPlanFromTo> AccountFromToData;
        private InputImportSettings InputImportSettings;

        #endregion

        #region Private methods

        private bool LoadAccountPlan(string fullFileName)
        {
            AccountPlanData.Name = fullFileName.Substring(fullFileName.LastIndexOf('\\') + 1);
            if (AccountPlanData.Name.Substring(AccountPlanData.Name.Length - 4).ToLower().Equals(".csv"))
                AccountPlanData.Name = AccountPlanData.Name.Substring(0, AccountPlanData.Name.Length - 4);

            AccountPlanData.Accounts = Csv.GetListFromCsv<Model.DTO.AccountPlanItens>(fullFileName, new char[] { ';' }, true);
            return AccountPlanData.Accounts.Count() > 0;
        }

        private bool LoadAccountPlan()
        {
            AccountPlanData = new AccountPlan().GetAccountPlan(Global.CollectionAccountPlan);
            return AccountPlanData.Accounts.Count() > 0;
        }

        private bool LoadAccountPlanFromTo(string fullFileName)
        {
            AccountFromToData = Csv.GetListFromCsv<Model.DTO.AccountPlanFromTo>(fullFileName, new char[] { ';' }, true);
            return AccountFromToData.Count() > 0;
        }

        private bool LoadAccountPlanFromTo(Helper.Enum.AccountPlanFromTo accountPlanFromTo)
        {
            AccountFromToData = new AccountPlan().GetAccountPlanFromTo(Global.CollectionAccountPlanFromTo, accountPlanFromTo);
            return AccountFromToData.Count() > 0;
        }

        private bool LoadStatement(string fullFileName, Helper.Enum.StatementSource statementSource)
        {
            IEnumerable<IStatementDataFile> statementData = null;

            switch (statementSource)
            {
                case Helper.Enum.StatementSource.ImportacaoMainframe:
                    statementData = Csv.GetListFromCsv<Model.DTO.Mainframe.Statement>(fullFileName, new char[] { ';' });
                    break;
                case Helper.Enum.StatementSource.ImportacaoSQL:
                    statementData = Csv.GetListFromCsv<Model.DTO.SQL.Statement>(fullFileName, new char[] { ';' });
                    break;
                case Helper.Enum.StatementSource.ImportacaoSerasa:
                case Helper.Enum.StatementSource.Serasa:
                case Helper.Enum.StatementSource.serasa:
                    statementData = Csv.GetListFromCsv<Model.DTO.Serasa.Statement>(fullFileName, new char[] { ';' });
                    break;
            }

            statementData = ApplyFilterStatementData(statementData);

            summary.TotalClients = statementData.Select(f => new { f.ClientId, f.ClientType }).Distinct().Count();
            summary.TotalLines = statementData.Count();

            if (statementData.Count() > 0)
                return businessStatement.LoadStatementsToDataBase(statementData);
            else
                return true;
        }

        private bool LoadNote(string fullFileName, Helper.Enum.StatementSource statementSource, bool hasExplanatoryNote = false)
        {
            IEnumerable<IExplanatoryNoteDataFile> noteData = null;

            switch (statementSource)
            {
                case Helper.Enum.StatementSource.ImportacaoMainframe:
                    if (hasExplanatoryNote)
                        noteData = Csv.GetListFromCsv<Model.DTO.Mainframe.ExplanatoryNote>(fullFileName, new char[] { ';' });
                    else
                        noteData = new List<Model.DTO.Mainframe.ExplanatoryNote>();
                    break;
                case Helper.Enum.StatementSource.ImportacaoSQL:
                    if (hasExplanatoryNote)
                        noteData = Csv.GetListFromCsv<Model.DTO.SQL.ExplanatoryNote>(fullFileName, new char[] { ';' });
                    else
                        noteData = new List<Model.DTO.Mainframe.ExplanatoryNote>();
                    break;
                case Helper.Enum.StatementSource.ImportacaoSerasa:
                    if (hasExplanatoryNote)
                        noteData = Csv.GetListFromCsv<Model.DTO.Serasa.ExplanatoryNote>(fullFileName, new char[] { ';' });
                    else
                        noteData = new List<Model.DTO.Mainframe.ExplanatoryNote>();
                    break;
            }

            noteData = ApplyFilterExplanatoryNoteData(noteData);

            if (noteData.Count() > 0)
                return businessStatement.LoadExplanatoryNotesToDataBase(noteData);
            else
                return true;
        }

        private bool Transform()
        {
            StringBuilder statementSavingMessage = new StringBuilder();

            List<Model.Json.Statement> statements = new List<Model.Json.Statement>();

            List<StatementKey> statementKeys = businessStatement.GetStatementKeys();
            //statementKeys.Add(new StatementKey { ClientId = 7689002, Month = 12, Year = 2017 });
            while (statementKeys.Count() > 0 && statementKeys.Count() > statements.Count())
            {
                statementKeys = businessStatement.CheckStatementsToSave(statementKeys, InputImportSettings.StatementSource, out statementSavingMessage);

                var statementToProcess = businessStatement.GetStatementToProcess(statementKeys);
                StatementData = statementToProcess.Statements;
                NoteData = statementToProcess.ExplanatoryNotes;

                foreach (var statementKey in statementKeys)
                {
                    try
                    {
                        Model.Json.Statement statement = new Model.Json.Statement();
                        List<Model.DTO.Statement> fullStatement = new List<Model.DTO.Statement>();

                        statement.Id = Global.NewMongoGuid;

                        #region  Cliente

                        statement.Cliente.TipoCliente = statementKey.ClientType;

                        if (statementKey.ClientType.Equals(Helper.Enum.ClienteType.E))
                            statement.Cliente.Cnpj = statementKey.ClientId;
                        else
                            statement.Cliente.CodigoGrupoEconomico = statementKey.ClientId;

                        statement.Dados.Confidencial = "Não";

                        #endregion

                        #region Origem

                        statement.Origem.Conta = "B";
                        statement.Origem.Descricao = InputImportSettings.StatementSource == Helper.Enum.StatementSource.ImportacaoSerasa ? "SERASA" : "DAC";
                        statement.Dados.Fonte = statement.Origem.Descricao;
                        statement.Origem.Fonte = InputImportSettings.StatementSource;

                        #endregion

                        #region  Dados

                        statement.Dados.Referencia = new DateTime(statementKey.Year, statementKey.Month, 1, 0, 0, 0, DateTimeKind.Utc);
                        statement.Dados.Tipo = statementKey.Type;
                        statement.Dados.Status = statementKey.Status;
                        statement.Dados.Moeda = "R$ MIL";
                        statement.Dados.Contas.NotaExplicativa.AddRange(GetExplanatoryNote(statementKey, 0));
                        statement.Dados.Contas.Auditoria.AddRange(GetExplanatoryNote(statementKey, 0, Helper.Enum.NoteType.Auditoria));
                        statement.Dados.PlanoDeContas = AccountPlanData.Name;
                        statement.Dados.DataAtualizacao = DateTime.UtcNow;

                        #endregion

                        IEnumerable<IStatementDataFile> statementAccounts = StatementData.Where(f => f.Year.Equals(statementKey.Year) && f.Month.Equals(statementKey.Month)
                                                                  && f.Type.Equals(statementKey.Type) && f.ClientType.Equals(statementKey.ClientType)
                                                                  && f.ClientId.Equals(statementKey.ClientId) && f.Status.Equals(statementKey.Status));

                        foreach (IStatementDataFile item in statementAccounts) //Lista de contas de um balanço
                        {
                            statement.Dados.Analista = item.Analist;

                            #region  Balancos

                            IEnumerable<AccountPlanFromTo> listaPlanoContasDePara = AccountFromToData.Where(f => f.OriginAccount == item.AccountId);
                            foreach (var planoContasDePara in listaPlanoContasDePara)
                            {
                                IEnumerable<Model.DTO.AccountPlanItens> listaPlanoContas = AccountPlanData.Accounts.Where(f => (f.Account == planoContasDePara.SumAccount || f.Account == planoContasDePara.Account));

                                foreach (var accountPlan in listaPlanoContas)
                                {
                                    double value = (planoContasDePara.Mutiplier != null && planoContasDePara.Mutiplier != 0) ? Convert.ToDouble(item.Value * planoContasDePara.Mutiplier) : item.Value;

                                    #region Verificar somatória

                                    if (planoContasDePara.SumAccount != null && planoContasDePara.SumAccount > 0 && accountPlan.Account == planoContasDePara.SumAccount)
                                    {
                                        SummarizeAccount(statementKey, fullStatement, accountPlan.Group, planoContasDePara, accountPlan.Order, value, item.Percentage);
                                    }

                                    #endregion

                                    #region Inserir registro conta

                                    if (accountPlan.Account > 0 && item.AccountId == accountPlan.Account || planoContasDePara.SumAccount == null)
                                    {
                                        fullStatement.Add(GetFilledStatement(accountPlan.Account, accountPlan.Group,
                                                            GetExplanatoryNote(statementKey, item.AccountId), accountPlan.Order, item.Percentage, value));
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }

                        #region Calcula DRE caso for migração Serasa
                        try
                        {
                            //if (InputImportSettings.StatementSource.Equals(Helper.Enum.StatementSource.ImportacaoSerasa))
                            //{
                            var receitaOperacionalLiquida = fullStatement.Where(f => f.Conta == 32000).FirstOrDefault();

                            if (receitaOperacionalLiquida != null && !string.IsNullOrEmpty(receitaOperacionalLiquida.Valor) && receitaOperacionalLiquida.Valor != "0")
                            {
                                foreach (var item in fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Dre) && !string.IsNullOrEmpty(f.Valor)))
                                {
                                    item.Porcentagem = Math.Round(double.Parse(item.Valor) / double.Parse(receitaOperacionalLiquida.Valor) * 100, 0);
                                }
                            }
                            //}
                        }
                        catch (Exception ex)
                        {
                            Log.Instance.Error(ex, string.Format("receitaOperacionalLiquida:{0} - {1}", fullStatement.Where(f => f.Conta == 32000).FirstOrDefault().Valor, new StackTrace().GetFrame(0).GetMethod().Name));
                        }

                        #endregion

                        #region Divide por 1000

                        fullStatement.ForEach(f => f.Valor = (!string.IsNullOrEmpty(f.Valor) && f.Valor != "0") ? (double.Parse(f.Valor) / 1000).ToString("0") : f.Valor);

                        #endregion

                        #region  Verifica contas calculadas

                        ulong accountNumber;

                        #region 71006 = (34000 + 50001 + 99903 + 34001 + 34003 + 36001 + 38000) / 32000

                        accountNumber = 71006;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 32000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.0%");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 71012 = (2100299 + 2200199 - 1100199 - 1102199 - 1201899) / 71004
                        //Esta conta deve ser realizada antes da conta 71004
                        accountNumber = 71012;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 71004).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 91016 = (2100299 + 2200199 - 1100199 - 1102199 - 1201899) / 71004;
                        accountNumber = 91016;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 71004).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 71015 = (21000 + 22000) / 25000
                        accountNumber = 71015;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 25000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 91018 = (21000 + 22000) / 25000
                        accountNumber = 91018;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 25000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 71016 = 11000 / 21000
                        accountNumber = 71016;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 21000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 91019 = 11000 / 21000
                        accountNumber = 91019;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 21000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 71017 = (11000 - 1100499) / 21000
                        accountNumber = 71017;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 21000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 71018 = (11000 + 12000) / (21000 + 22000)
                        accountNumber = 71018;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 21000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            retorno = fullStatement.Where(f => f.Conta == 22000).FirstOrDefault();

                            divisor += retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion                       

                        #region 71014 = (34000 + 50001 + 99903) / (82100 * -1)
                        accountNumber = 71014;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 82100).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor) * -1;

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 91017 = (34000 + 50001 + 99903) / (82100 * -1)
                        accountNumber = 91017;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 82100).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor) * -1;

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.00");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 91006 = (34000 + 99903 + 50001) / 32000
                        accountNumber = 91006;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 32000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.0%");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #region 71004 = (34000 + 99903 + 50001) / 32000
                        accountNumber = 71004;

                        if (fullStatement.Where(f => f.Conta == accountNumber).Count() > 0)
                        {
                            var retorno = fullStatement.Where(f => f.Conta == 32000).FirstOrDefault();

                            double divisor = retorno == null ? 0 : Convert.ToDouble(retorno.Valor);

                            if (divisor != 0)
                            {
                                fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor =
                                    (Convert.ToDouble(fullStatement.Where(f => f.Conta == accountNumber).FirstOrDefault().Valor) / divisor).ToString("0.0%");
                            }
                            else
                            {
                                fullStatement.RemoveAll(f => f.Conta == accountNumber);
                            }
                        }
                        #endregion

                        #endregion

                        #region  Ordena as constas existentes em cada entiade antes de adicionar o balanço

                        statement.Dados.Contas.Ativo = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Ativo)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.Ativo
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        statement.Dados.Contas.Passivo = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Passivo)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.Passivo
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        statement.Dados.Contas.Dre = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Dre)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.Dre
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            Porcentagem = f.Porcentagem,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        statement.Dados.Contas.Mutacao = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Mutacao)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.Mutacao
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        statement.Dados.Contas.FluxoCaixa = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.FluxoCaixa)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.FluxoCaixa
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        statement.Dados.Contas.Indicadores = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Indicadores)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.Indicadores
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        statement.Dados.Contas.Highlights = fullStatement.Where(f => f.Grupo.Equals(Helper.Enum.AccountGroup.Highlights)).OrderBy(f => f.Ordem)
                                                        .Select(f => new Model.Json.Highlights
                                                        {
                                                            Conta = f.Conta,
                                                            Valor = f.Valor,
                                                            NotaVinculadaValor = f.NotaVinculadaValor,
                                                            Ordem = f.Ordem
                                                        }).ToList();

                        #endregion

                        statements.Add(statement);
                        summary.StatementYear.Add(statementKey.Year);

                        if (!summary.StatementClient.Exists(f => f.ClienteType.Equals(statementKey.ClientType) && f.ClientId.Equals(statementKey.ClientId) && f.Year.Equals(statementKey.Year)))
                            summary.StatementClient.Add(new StatementClient() { ClienteType = statementKey.ClientType, ClientId = statementKey.ClientId, Year = statementKey.Year });

                        if (statements.Count >= Global.BufferStatementToSave)
                        {
                            SaveStatement(statements, businessStatement, statementSavingMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                        throw;
                    }
                }

                statementKeys = businessStatement.GetStatementKeys();
            }

            if (statements.Count > 0)
            {
                SaveStatement(statements, businessStatement, statementSavingMessage);
            }

            summary.StatementSavingMessage = statementSavingMessage.ToString();

            //Clean temp table afer process
            businessStatement.CleanStatementToProcess();

            return true;
        }

        private void SaveStatement(List<Model.Json.Statement> statements, Statement businessStatement, StringBuilder statementSavingMessage)
        {
            List<StatementKey> statementKeys = new List<StatementKey>();

            try
            {
                summary.TotalStatements += statements.Count();

                statements.ForEach(f => statementKeys.Add(new StatementKey
                {
                    ClientId = Convert.ToDouble(f.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? f.Cliente.Cnpj : f.Cliente.CodigoGrupoEconomico),
                    ClientType = f.Cliente.TipoCliente,
                    Month = (byte)f.Dados.Referencia.Month,
                    Year = f.Dados.Referencia.Year,
                    Status = f.Dados.Status,
                    Type = f.Dados.Tipo,
                    Source = f.Origem.Fonte
                }));

                businessStatement.SaveStatements(statements);
                statements.ForEach(f => statementSavingMessage.AppendLine(string.Format(Global.StatementSavedMsg, f.Cliente.TipoCliente, f.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? f.Cliente.Cnpj : f.Cliente.CodigoGrupoEconomico,
                                    f.Dados.Referencia.Month, f.Dados.Referencia.Year, f.Dados.Tipo, f.Dados.Status)));
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                //GravarBalancoNaoSalvo(InputImportSettings.Statement, statements, out StringBuilder returnMessages);
                //statementSavingMessage.AppendLine(returnMessages.ToString());
                throw;
            }
            finally
            {
                //SaveImportCheckpoint(InputImportSettings.Statement, statements);
                businessStatement.DeleteStatementToProcess(statementKeys);// 10 - 2.7 s | 10 - 2,2 s | 30 - 8,8s
                statements.Clear();
            }
        }

        private void SummarizeAccount(StatementKey key, List<Model.DTO.Statement> statements, Helper.Enum.AccountGroup group, AccountPlanFromTo accountPlanFromTo, uint order, double value, double? percentage = null)
        {
            try
            {
                Model.DTO.AccountPlanItens conta;
                if (statements.Where(f => f.Conta == accountPlanFromTo.SumAccount).Count() > 0)
                {
                    statements.Where(f => f.Conta == accountPlanFromTo.SumAccount).FirstOrDefault().Valor =
                        (Convert.ToDouble(statements.Where(f => f.Conta == accountPlanFromTo.SumAccount).FirstOrDefault().Valor) + value).ToString();
                    statements.Where(f => f.Conta == accountPlanFromTo.SumAccount).FirstOrDefault().Porcentagem =
                        (statements.Where(f => f.Conta == accountPlanFromTo.SumAccount).FirstOrDefault().Porcentagem + percentage);
                }
                else
                    statements.Add(
                        GetFilledStatement((ulong)accountPlanFromTo.SumAccount, group,
                        GetExplanatoryNote(key, (ulong)accountPlanFromTo.SumAccount),
                        order, percentage, value));

                foreach (var dePara in AccountFromToData.Where(f => f.OriginAccount == accountPlanFromTo.SumAccount))
                {
                    if (dePara != null && dePara.SumAccount != null && dePara.SumAccount > 0)
                    {
                        conta = AccountPlanData.Accounts.Where(f => f.Account == dePara.SumAccount).FirstOrDefault();
                        SummarizeAccount(key, statements, conta.Group, dePara, conta.Order, dePara.Mutiplier != null ? Convert.ToDouble(value * (double)dePara.Mutiplier) : value, percentage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
        }

        private Model.DTO.Statement GetFilledStatement(ulong? account, Helper.Enum.AccountGroup group, IEnumerable<Model.Json.Note> noteLinkedToValue, uint order, double? percentage, double value)
        {
            return new Model.DTO.Statement
            {
                Conta = account,
                Grupo = group,
                NotaVinculadaValor = noteLinkedToValue.ToList(),
                Ordem = order,
                Porcentagem = percentage,
                Valor = value.ToString()
            };
        }

        private List<Model.Json.Note> GetExplanatoryNote(StatementKey statementKey, ulong? accountId, Helper.Enum.NoteType noteType = Helper.Enum.NoteType.NotaExplicativa)
        {
            List<Model.Json.Note> obj = new List<Model.Json.Note>();

            var listaNotas = NoteData.Where(f => f.Year.Equals(statementKey.Year) && f.Month.Equals(statementKey.Month) && f.StatementType == statementKey.Type &&
                                             f.StatementStatus == statementKey.Status && f.ClientType == statementKey.ClientType && f.ClientId.Equals(statementKey.ClientId) &&
                                             f.AccountId == accountId &&
                                             (noteType == Helper.Enum.NoteType.NotaExplicativa ? f.TypeCode == "NOTA EXPL" : f.TypeCode != "NOTA EXPL")).OrderBy(o => o.Order);

            string[] separator = new string[] { @"\n" };
            foreach (var item in listaNotas)
            {
                foreach (var nota in item.Value.Replace(@"<br\><br\>", @"\n").Split(separator, StringSplitOptions.None))
                    obj.Add(new Model.Json.Note() { Valor = nota });
            }

            return obj;
        }

        private void SaveReport(Summary summary, Helper.Enum.StatementSource statementSource)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("Sumarização {0} gerada em: {1}", statementSource, DateTime.Now));
            sb.AppendLine();

            sb.AppendLine(string.Format("Total de linhas do arquivo: {0}", summary.TotalLines));
            sb.AppendLine(string.Format("Total de balanços: {0}", summary.TotalStatements));
            sb.AppendLine(string.Format("Total de clientes: {0}", summary.TotalClients));
            sb.AppendLine(string.Format("Tempo de processamento: {0}", summary.ProcessingTime));
            sb.AppendLine();

            sb.AppendLine("Clientes por ano");
            foreach (var item in summary.StatementClient.OrderBy(f => f.Year).Select(f => f.Year).Distinct())
                sb.AppendLine(string.Format("Ano: {0} - Clientes: {1}", item, summary.StatementClient.Count(f => f.Year.Equals(item))));
            sb.AppendLine();

            sb.AppendLine("Balanco por ano");
            foreach (var item in summary.StatementYear.OrderBy(f => f.ToString()).Distinct())
                sb.AppendLine(string.Format("Ano: {0} - Balanços: {1}", item, summary.StatementYear.Count(f => f.Equals(item))));
            sb.AppendLine();

            sb.AppendLine(string.Format("Buffer de gravação de registro: {0}", Global.BufferStatementToSave));
            sb.AppendLine();

            sb.AppendLine("Mensagens de gravação:");
            sb.AppendLine(summary.StatementSavingMessage);
            sb.AppendLine();

            sb.AppendLine("FIM");
            sb.AppendLine(string.Empty);

            File.WriteFile(Global.SummaryFileName, sb.ToString());
        }

        private void GravarBalancoNaoSalvo(string statementFileName, List<Model.Json.Statement> statements, out StringBuilder returnMessages)
        {
            StringBuilder sb = new StringBuilder();

            statements.ForEach(f => sb.AppendLine(string.Format("{0};{1};{2};{3};{4};{5}", f.Dados.Referencia.Year, f.Dados.Referencia.Month, f.Dados.Tipo, f.Cliente.TipoCliente,
                f.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? f.Cliente.Cnpj : f.Cliente.CodigoGrupoEconomico, f.Dados.Status)));

            File.WriteFile(Global.GetErrorStatementFilePath(statementFileName), sb.ToString(), true);

            sb.Clear();
            statements.ForEach(f => sb.AppendLine(string.Format(Global.StatementErrorMsg, f.Cliente.TipoCliente, f.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? f.Cliente.Cnpj : f.Cliente.CodigoGrupoEconomico,
                f.Dados.Referencia.Month, f.Dados.Referencia.Year, f.Dados.Tipo, f.Dados.Status)));

            returnMessages = sb;
        }

        private IEnumerable<IStatementDataFile> ApplyFilterStatementData(IEnumerable<IStatementDataFile> list)
        {
            if (InputImportSettings.ClientId != null && InputImportSettings.ClientId.Count > 0)
                list = list.Where(f => InputImportSettings.ClientId.Contains(f.ClientId)).OrderBy(o => o.AccountId);

            switch (InputImportSettings.ClientType)
            {
                case Helper.Enum.ClienteType.E:
                    list = list.Where(f => f.ClientType == Helper.Enum.ClienteType.E);
                    break;
                case Helper.Enum.ClienteType.G:
                    list = list.Where(f => f.ClientType == Helper.Enum.ClienteType.G);
                    break;
            }

            switch (InputImportSettings.StatementStatus)
            {
                case Helper.Enum.StatementStatus.Definitivo:
                    list = list.Where(f => f.Status.Equals(Helper.Enum.StatementStatus.Definitivo));
                    break;
                case Helper.Enum.StatementStatus.Provisorio:
                case Helper.Enum.StatementStatus.Provisório:
                    list = list.Where(f => !f.Status.Equals(Helper.Enum.StatementStatus.Definitivo));
                    break;
            }

            #region Remove duplicities

            var distincted = list.Select(s => new
                    { s.AccountId, s.Analist, s.ClientId, s.ClientType, s.Month, s.Percentage, s.Source, s.Status, s.Type, s.Value, s.Year, s.UpdatedDate }
                    ).Distinct().ToList();

            List<StatementDataFile> statementDataFile = new List<StatementDataFile>();
            distincted.ForEach(f => statementDataFile.Add(new StatementDataFile
            {
                Id = Global.NewMongoGuid,
                AccountId = f.AccountId,
                Analist = f.Analist,
                ClientId = f.ClientId,
                ClientType = f.ClientType,
                Month = f.Month,
                Year = f.Year,
                Source = f.Source,
                Status = f.Status,
                Type = f.Type,
                Value = f.Value,
                Percentage = f.Percentage,
                UpdatedDate = f.UpdatedDate
            }));

            #endregion

            return statementDataFile;
        }

        private IEnumerable<IExplanatoryNoteDataFile> ApplyFilterExplanatoryNoteData(IEnumerable<IExplanatoryNoteDataFile> list)
        {
            if (InputImportSettings.ClientId != null && InputImportSettings.ClientId.Count > 0)
                list = list.Where(f => InputImportSettings.ClientId.Contains(f.ClientId));

            switch (InputImportSettings.ClientType)
            {
                case Helper.Enum.ClienteType.E:
                    list = list.Where(f => f.ClientType != Helper.Enum.ClienteType.E);
                    break;
                case Helper.Enum.ClienteType.G:
                    list = list.Where(f => f.ClientType == Helper.Enum.ClienteType.G);
                    break;
            }

            switch (InputImportSettings.StatementStatus)
            {
                case Helper.Enum.StatementStatus.Definitivo:
                    list = list.Where(f => f.StatementStatus.Equals(Helper.Enum.StatementStatus.Definitivo));
                    break;
                case Helper.Enum.StatementStatus.Provisorio:
                case Helper.Enum.StatementStatus.Provisório:
                    list = list.Where(f => !f.StatementStatus.Equals(Helper.Enum.StatementStatus.Definitivo));
                    break;
            }

            return list;
        }

        private bool StartImportByEnvironment()
        {
            businessStatement = new Statement();

            for (int i = 0; i < InputImportSettings.Statement.Count(); i++)
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                summary = new Model.DTO.Summary();

                if (InputImportSettings.ExplanatoryNote.Count > i)
                    InputImportSettings.HasExplanatoryNotes = true;
                else
                    InputImportSettings.HasExplanatoryNotes = false;

                businessStatement.CleanStatementToProcess();
                LoadStatement(InputImportSettings.Statement[i], InputImportSettings.StatementSource);
                LoadNote(InputImportSettings.HasExplanatoryNotes ? InputImportSettings.ExplanatoryNote[i] : string.Empty, InputImportSettings.StatementSource, InputImportSettings.HasExplanatoryNotes);
                LoadAccountPlan();
                LoadAccountPlanFromTo(InputImportSettings.FromTo);

                Transform();

                stopwatch.Stop();
                summary.ProcessingTime = stopwatch.Elapsed;

                SaveReport(summary, InputImportSettings.StatementSource);
            }

            return true;
        }
        #endregion

        #region Public methods

        public bool StartImport(InputImportSettings entrada)
        {
            try
            {
                InputImportSettings = entrada;

                foreach (var item in InputImportSettings.Environment)
                {
                    Helper.Generic.Global.SetEnvironment(item);
                    StartImportByEnvironment();
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                return false;
            }
        }

        #endregion
    }
}

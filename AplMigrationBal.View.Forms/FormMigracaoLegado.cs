﻿using AplMigrationBal.Business;
using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AplMigrationBal.View.Forms
{
    public partial class FormMigracaoLegado : Form
    {
        List<KeyValuePair<string, string>> StatementList = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> ExplanatoryNoteList = new List<KeyValuePair<string, string>>();

        public FormMigracaoLegado()
        {
            InitializeComponent();
        }

        public FormMigracaoLegado(Model.DTO.InputImportSettings entradaImportacao)
        {
            InitializeComponent();
            IniciarImportacao(entradaImportacao);
            Application.Exit();
        }

        #region Public Methods

        public void IniciarImportacao(Model.DTO.InputImportSettings entradaImportacao)
        {
            try
            {
                statusStrip.Items.Clear();
                statusStrip.Items.Add("Importando dados");

                Import importacao = new Import();

                Task.Run(() =>
                {
                    Helper.Generic.Global.JsonOutputPath = entradaImportacao.JsonOutputPath;

                    bool result = importacao.StartImport(entradaImportacao);

                    if (result)
                    {
                        statusStrip.Items.Clear();
                        statusStrip.Items.Add("Importação finalizada com sucesso.");
                    }
                    else
                    {
                        statusStrip.Items.Clear();
                        statusStrip.Items.Add("Erro ao tentar realizar importação.");
                    }
                });

                buttonInicioImportacao.Enabled = true;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
        }

        #endregion

        private void ButtonInicioImportacao_Click(object sender, EventArgs e)
        {
            int qtdTentativas = 0;
            Model.DTO.InputImportSettings importSettings = new Model.DTO.InputImportSettings();

            importSettings.FromTo = textBoxDePara.Text;
            importSettings.Statement = StatementList.Select(s => s.Value).ToList();
            importSettings.ExplanatoryNote = ExplanatoryNoteList.Select(s => s.Value).ToList();
            importSettings.HasExplanatoryNotes = importSettings.ExplanatoryNote.Count > 0;
            importSettings.StatementStatus = Helper.Enum.StatementStatus.Todos;
            importSettings.ClientType = Helper.Enum.ClienteType.Todos;
            importSettings.JsonOutputPath = textBoxJsonSaida.Text;

            #region Source
            if (radioButtonCsvMainframe.Checked)
                importSettings.StatementSource = Helper.Enum.StatementSource.ImportacaoMainframe;
            else if (radioButtonCsvSqlServer.Checked)
                importSettings.StatementSource = Helper.Enum.StatementSource.ImportacaoSQL;
            else if (radioButtonCsvSerasa.Checked)
                importSettings.StatementSource = Helper.Enum.StatementSource.ImportacaoSerasa;
            #endregion

            #region Ambiente
            importSettings.Environment.Clear();

            if (checkBoxAmbienteTI.Checked)
                importSettings.Environment.Add(Helper.Enum.Environment.TI);

            if (checkBoxAmbienteHomologacao.Checked)
                importSettings.Environment.Add(Helper.Enum.Environment.Homologacao);

            if (checkBoxAmbienteProducao.Checked)
                importSettings.Environment.Add(Helper.Enum.Environment.Producao);


            if (importSettings.Environment.Count <= 0)
            {
                statusStrip.Items.Clear();
                statusStrip.Items.Add("Selecione pelo menos 1 ambiente para carga");
                return;
            }
            #endregion

            #region Tipo balanço
            if (radioButtonTipoBalancoNormal.Checked)
                importSettings.StatementStatus = Helper.Enum.StatementStatus.Definitivo;
            else if (radioButtonTipoBalancoProvisorio.Checked)
                importSettings.StatementStatus = Helper.Enum.StatementStatus.Provisorio;
            #endregion

            #region Tipo cliente
            if (radioButtonTipoClienteEmpresa.Checked)
                importSettings.ClientType = Helper.Enum.ClienteType.E;
            else if (radioButtonTipoClienteGrupo.Checked)
                importSettings.ClientType = Helper.Enum.ClienteType.G;
            #endregion

            #region Clientes específicos
            if (checkBoxClientesEspecificos.Checked)
            {
                if (!string.IsNullOrEmpty(textBoxClientesEspecificos.Text))
                {
                    var cli = textBoxClientesEspecificos.Text.Split(';');
                    if (cli.Length > 0)
                    {
                        importSettings.ClientId = new List<double>();
                        foreach (var c in cli)
                        {
                            if (!string.IsNullOrEmpty(c))
                                importSettings.ClientId.Add(Convert.ToUInt64(c));
                        }
                    }
                }
            }
            #endregion

            TryAgain:
            try
            {
                buttonInicioImportacao.Enabled = false;
                statusStrip.Items.Clear();
                statusStrip.Items.Add("Carregando - Tentativa " + qtdTentativas);

                IniciarImportacao(importSettings);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                statusStrip.Items.Clear();
                statusStrip.Items.Add("Erro aoprocessar balanço");
                if (qtdTentativas < 3)
                {
                    qtdTentativas++;
                    goto TryAgain;
                }
                throw;
            }
        }

        private void ButtonSelectPlanoContasDePara_Click(object sender, EventArgs e)
        {
            openFileDialogDePara.ShowDialog();
            textBoxDePara.Text = openFileDialogDePara.FileName;
        }

        private void ButtonSelectBalanco_Click(object sender, EventArgs e)
        {
            if (openFileDialogBalanco.ShowDialog() == DialogResult.OK)
            {
                listBoxBalanco.DataSource = null;

                for (int i = 0; i < openFileDialogBalanco.FileNames.Count(); i++)
                {
                    StatementList.Add(new KeyValuePair<string, string>(openFileDialogBalanco.SafeFileNames[i], openFileDialogBalanco.FileNames[i]));
                }

                listBoxBalanco.DisplayMember = "Key";
                listBoxBalanco.ValueMember = "Value";
                listBoxBalanco.DataSource = StatementList;
            }
        }

        private void ButtonSelectNotaExplicativa_Click(object sender, EventArgs e)
        {
            if (openFileDialogNotaExplicativa.ShowDialog() == DialogResult.OK)
            {
                listBoxNotaExplicativa.DataSource = null;

                for (int i = 0; i < openFileDialogBalanco.FileNames.Count(); i++)
                {
                    ExplanatoryNoteList.Add(new KeyValuePair<string, string>(openFileDialogNotaExplicativa.SafeFileNames[i], openFileDialogNotaExplicativa.FileNames[i]));
                }

                listBoxNotaExplicativa.DisplayMember = "Key";
                listBoxNotaExplicativa.ValueMember = "Value";
                listBoxNotaExplicativa.DataSource = ExplanatoryNoteList;
            }
        }

        private void FormMigracaoLegado_Load(object sender, EventArgs e)
        {
            statusStrip.Items.Add("Aplicação inicializada");
            UpdatePaths();
        }

        private void RadioButtonCsvSerasa_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("MF", "SERASA");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("SQL", "SERASA");
            UpdateJsonOutputPath();
        }

        private void RadioButtonCsvSqlServer_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("MF", "SQL");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("SERASA", "SQL");
            UpdateJsonOutputPath();
        }

        private void RadioButtonCsvMainframe_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("SQL", "MF");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("SERASA", "MF");
            UpdateJsonOutputPath();
        }

        private void RadioButtonAmbienteTI_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("PROD", "TI");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("HOMOL", "TI");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("TODOS", "TI");
            UpdateJsonOutputPath();
        }

        private void RadioButtonAmbienteHomologacao_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("PROD", "HOMOL");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("TI", "HOMOL");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("TODOS", "HOMOL");
            UpdateJsonOutputPath();
        }

        private void RadioButtonAmbienteProducao_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("TI", "PROD");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("HOMOL", "PROD");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("TODOS", "PROD");
            UpdateJsonOutputPath();
        }

        private void radioButtonAmbienteTodos_CheckedChanged(object sender, EventArgs e)
        {
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("TI", "Todos");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("HOMOL", "Todos");
            Helper.Generic.Global.JsonOutputPath = Helper.Generic.Global.JsonOutputPath.Replace("PROD", "Todos");
            UpdateJsonOutputPath();
        }

        private void TextBoxJsonOutputPath_Leave(object sender, EventArgs e)
        {
            if (!textBoxJsonSaida.Text.EndsWith(@"\"))
                textBoxJsonSaida.Text += @"\";

            Helper.Generic.Global.JsonOutputPath = textBoxJsonSaida.Text;
        }

        private void UpdateJsonOutputPath()
        {
            textBoxJsonSaida.Text = Helper.Generic.Global.JsonOutputPath;
        }

        private void UpdatePaths()
        {
            textBoxDePara.Text = Global.AccountPlanPath + textBoxDePara.Text;
            openFileDialogDePara.InitialDirectory = Global.AccountPlanPath;
            openFileDialogBalanco.InitialDirectory = Global.DataPath;
            openFileDialogNotaExplicativa.InitialDirectory = Global.DataPath;
            UpdateJsonOutputPath();
        }

        private void ButtonLimparListas_Click(object sender, EventArgs e)
        {
            StatementList.Clear();
            listBoxBalanco.DataSource = null;

            ExplanatoryNoteList.Clear();
            listBoxNotaExplicativa.DataSource = null;
        }

        private void ButtonRemoverBalanco_Click(object sender, EventArgs e)
        {
            if (listBoxBalanco.SelectedItem != null)
            {
                StatementList.RemoveAt(listBoxBalanco.SelectedIndex);
                listBoxBalanco.DataSource = null;

                listBoxBalanco.DisplayMember = "Key";
                listBoxBalanco.ValueMember = "Value";
                listBoxBalanco.DataSource = StatementList;
            }
        }

        private void buttonRemoverNotaExplicativa_Click(object sender, EventArgs e)
        {
            if (listBoxNotaExplicativa.SelectedItem != null)
            {
                ExplanatoryNoteList.RemoveAt(listBoxNotaExplicativa.SelectedIndex);
                listBoxNotaExplicativa.DataSource = null;

                listBoxNotaExplicativa.DisplayMember = "Key";
                listBoxNotaExplicativa.ValueMember = "Value";
                listBoxNotaExplicativa.DataSource = ExplanatoryNoteList;
            }
        }
    }
}

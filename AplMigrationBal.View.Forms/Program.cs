﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace AplMigrationBal.View.Forms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);



            if (args.Length >= 8)
            {
                Model.DTO.InputImportSettings entradaImportacao = new Model.DTO.InputImportSettings();
                entradaImportacao.Environment.Add((Helper.Enum.Environment)System.Enum.Parse(typeof(Helper.Enum.Environment), args[7].Trim(), true));
                entradaImportacao.FromTo = args[1];
                entradaImportacao.Statement = args[3].Split(';').ToList();
                entradaImportacao.ExplanatoryNote = args[4].Split(';').ToList();
                entradaImportacao.JsonOutputPath = args[5];
                entradaImportacao.StatementSource = (Helper.Enum.StatementSource)System.Enum.Parse(typeof(Helper.Enum.StatementSource), args[6].Trim(), true); ;

                entradaImportacao.StatementStatus = Helper.Enum.StatementStatus.Todos;
                entradaImportacao.ClientType = Helper.Enum.ClienteType.Todos;

                //PlanoContasBAL.csv; PlanoContasDePara.csv; TodosClientes.csv; sql\BAL_SQL_2018_10_03.csv; sql\NEX_SQL_2018_10_03.csv; C:\Temp\Balanco\Json\Prod\SQL\Teste\; false; TI
                Application.Run(new FormMigracaoLegado(entradaImportacao));
            }
            else
            {
                Application.Run(new FormMigracaoLegado());
            }
        }
    }
}

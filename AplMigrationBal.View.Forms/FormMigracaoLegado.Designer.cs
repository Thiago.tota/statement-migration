﻿namespace AplMigrationBal.View.Forms
{
    partial class FormMigracaoLegado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonInicioImportacao = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonRemoverNotaExplicativa = new System.Windows.Forms.Button();
            this.buttonRemoverBalanco = new System.Windows.Forms.Button();
            this.listBoxBalanco = new System.Windows.Forms.ListBox();
            this.listBoxNotaExplicativa = new System.Windows.Forms.ListBox();
            this.textBoxJsonSaida = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonSelectPlanoContasDePara = new System.Windows.Forms.Button();
            this.textBoxDePara = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonSelectNotaExplicativa = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSelectBalanco = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonCsvMainframe = new System.Windows.Forms.RadioButton();
            this.radioButtonCsvSqlServer = new System.Windows.Forms.RadioButton();
            this.radioButtonCsvSerasa = new System.Windows.Forms.RadioButton();
            this.openFileDialogDePara = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogBalanco = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogNotaExplicativa = new System.Windows.Forms.OpenFileDialog();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButtonTipoBalancoTodos = new System.Windows.Forms.RadioButton();
            this.radioButtonTipoBalancoProvisorio = new System.Windows.Forms.RadioButton();
            this.radioButtonTipoBalancoNormal = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButtonTipoClienteGrupo = new System.Windows.Forms.RadioButton();
            this.radioButtonTipoClienteEmpresa = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxClientesEspecificos = new System.Windows.Forms.TextBox();
            this.checkBoxClientesEspecificos = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.checkBoxAmbienteProducao = new System.Windows.Forms.CheckBox();
            this.checkBoxAmbienteHomologacao = new System.Windows.Forms.CheckBox();
            this.checkBoxAmbienteTI = new System.Windows.Forms.CheckBox();
            this.buttonLimparListas = new System.Windows.Forms.Button();
            this.statusStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar,
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 450);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip.Size = new System.Drawing.Size(799, 22);
            this.statusStrip.TabIndex = 8;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(300, 16);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // buttonInicioImportacao
            // 
            this.buttonInicioImportacao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInicioImportacao.Location = new System.Drawing.Point(674, 424);
            this.buttonInicioImportacao.Name = "buttonInicioImportacao";
            this.buttonInicioImportacao.Size = new System.Drawing.Size(113, 23);
            this.buttonInicioImportacao.TabIndex = 5;
            this.buttonInicioImportacao.Text = "Iniciar Importação";
            this.buttonInicioImportacao.UseVisualStyleBackColor = true;
            this.buttonInicioImportacao.Click += new System.EventHandler(this.ButtonInicioImportacao_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.buttonRemoverNotaExplicativa);
            this.groupBox1.Controls.Add(this.buttonRemoverBalanco);
            this.groupBox1.Controls.Add(this.listBoxBalanco);
            this.groupBox1.Controls.Add(this.listBoxNotaExplicativa);
            this.groupBox1.Controls.Add(this.textBoxJsonSaida);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.buttonSelectPlanoContasDePara);
            this.groupBox1.Controls.Add(this.textBoxDePara);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.buttonSelectNotaExplicativa);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.buttonSelectBalanco);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 146);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 272);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Arquivos CSV";
            // 
            // buttonRemoverNotaExplicativa
            // 
            this.buttonRemoverNotaExplicativa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoverNotaExplicativa.Location = new System.Drawing.Point(686, 121);
            this.buttonRemoverNotaExplicativa.Name = "buttonRemoverNotaExplicativa";
            this.buttonRemoverNotaExplicativa.Size = new System.Drawing.Size(72, 20);
            this.buttonRemoverNotaExplicativa.TabIndex = 24;
            this.buttonRemoverNotaExplicativa.Text = "Remover";
            this.buttonRemoverNotaExplicativa.UseVisualStyleBackColor = true;
            this.buttonRemoverNotaExplicativa.Click += new System.EventHandler(this.buttonRemoverNotaExplicativa_Click);
            // 
            // buttonRemoverBalanco
            // 
            this.buttonRemoverBalanco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoverBalanco.Location = new System.Drawing.Point(304, 121);
            this.buttonRemoverBalanco.Name = "buttonRemoverBalanco";
            this.buttonRemoverBalanco.Size = new System.Drawing.Size(72, 20);
            this.buttonRemoverBalanco.TabIndex = 23;
            this.buttonRemoverBalanco.Text = "Remover";
            this.buttonRemoverBalanco.UseVisualStyleBackColor = true;
            this.buttonRemoverBalanco.Click += new System.EventHandler(this.ButtonRemoverBalanco_Click);
            // 
            // listBoxBalanco
            // 
            this.listBoxBalanco.FormattingEnabled = true;
            this.listBoxBalanco.Location = new System.Drawing.Point(18, 95);
            this.listBoxBalanco.Name = "listBoxBalanco";
            this.listBoxBalanco.Size = new System.Drawing.Size(280, 173);
            this.listBoxBalanco.TabIndex = 22;
            // 
            // listBoxNotaExplicativa
            // 
            this.listBoxNotaExplicativa.FormattingEnabled = true;
            this.listBoxNotaExplicativa.Location = new System.Drawing.Point(400, 95);
            this.listBoxNotaExplicativa.Name = "listBoxNotaExplicativa";
            this.listBoxNotaExplicativa.Size = new System.Drawing.Size(280, 173);
            this.listBoxNotaExplicativa.TabIndex = 21;
            // 
            // textBoxJsonSaida
            // 
            this.textBoxJsonSaida.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxJsonSaida.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxJsonSaida.Location = new System.Drawing.Point(107, 39);
            this.textBoxJsonSaida.Name = "textBoxJsonSaida";
            this.textBoxJsonSaida.Size = new System.Drawing.Size(550, 20);
            this.textBoxJsonSaida.TabIndex = 17;
            this.textBoxJsonSaida.Leave += new System.EventHandler(this.TextBoxJsonOutputPath_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Json saída:";
            // 
            // buttonSelectPlanoContasDePara
            // 
            this.buttonSelectPlanoContasDePara.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectPlanoContasDePara.Location = new System.Drawing.Point(664, 13);
            this.buttonSelectPlanoContasDePara.Name = "buttonSelectPlanoContasDePara";
            this.buttonSelectPlanoContasDePara.Size = new System.Drawing.Size(24, 20);
            this.buttonSelectPlanoContasDePara.TabIndex = 16;
            this.buttonSelectPlanoContasDePara.Text = "...";
            this.buttonSelectPlanoContasDePara.UseVisualStyleBackColor = true;
            this.buttonSelectPlanoContasDePara.Click += new System.EventHandler(this.ButtonSelectPlanoContasDePara_Click);
            // 
            // textBoxDePara
            // 
            this.textBoxDePara.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDePara.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxDePara.Enabled = false;
            this.textBoxDePara.Location = new System.Drawing.Point(107, 13);
            this.textBoxDePara.Name = "textBoxDePara";
            this.textBoxDePara.Size = new System.Drawing.Size(550, 20);
            this.textBoxDePara.TabIndex = 14;
            this.textBoxDePara.Text = "PlanoContasDeParaSQL.csv";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "De Para:";
            // 
            // buttonSelectNotaExplicativa
            // 
            this.buttonSelectNotaExplicativa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectNotaExplicativa.Location = new System.Drawing.Point(686, 95);
            this.buttonSelectNotaExplicativa.Name = "buttonSelectNotaExplicativa";
            this.buttonSelectNotaExplicativa.Size = new System.Drawing.Size(72, 20);
            this.buttonSelectNotaExplicativa.TabIndex = 10;
            this.buttonSelectNotaExplicativa.Text = "Adicionar";
            this.buttonSelectNotaExplicativa.UseVisualStyleBackColor = true;
            this.buttonSelectNotaExplicativa.Click += new System.EventHandler(this.ButtonSelectNotaExplicativa_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(397, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nota explicativa:";
            // 
            // buttonSelectBalanco
            // 
            this.buttonSelectBalanco.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectBalanco.Location = new System.Drawing.Point(304, 95);
            this.buttonSelectBalanco.Name = "buttonSelectBalanco";
            this.buttonSelectBalanco.Size = new System.Drawing.Size(72, 20);
            this.buttonSelectBalanco.TabIndex = 7;
            this.buttonSelectBalanco.Text = "Adicionar";
            this.buttonSelectBalanco.UseVisualStyleBackColor = true;
            this.buttonSelectBalanco.Click += new System.EventHandler(this.ButtonSelectBalanco_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Balanço:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonCsvMainframe);
            this.groupBox3.Controls.Add(this.radioButtonCsvSqlServer);
            this.groupBox3.Controls.Add(this.radioButtonCsvSerasa);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(306, 47);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados de origem";
            // 
            // radioButtonCsvMainframe
            // 
            this.radioButtonCsvMainframe.AutoSize = true;
            this.radioButtonCsvMainframe.Location = new System.Drawing.Point(202, 19);
            this.radioButtonCsvMainframe.Name = "radioButtonCsvMainframe";
            this.radioButtonCsvMainframe.Size = new System.Drawing.Size(98, 17);
            this.radioButtonCsvMainframe.TabIndex = 2;
            this.radioButtonCsvMainframe.Text = "CSV Mainframe";
            this.radioButtonCsvMainframe.UseVisualStyleBackColor = true;
            this.radioButtonCsvMainframe.CheckedChanged += new System.EventHandler(this.RadioButtonCsvMainframe_CheckedChanged);
            // 
            // radioButtonCsvSqlServer
            // 
            this.radioButtonCsvSqlServer.AutoSize = true;
            this.radioButtonCsvSqlServer.Checked = true;
            this.radioButtonCsvSqlServer.Location = new System.Drawing.Point(92, 19);
            this.radioButtonCsvSqlServer.Name = "radioButtonCsvSqlServer";
            this.radioButtonCsvSqlServer.Size = new System.Drawing.Size(104, 17);
            this.radioButtonCsvSqlServer.TabIndex = 1;
            this.radioButtonCsvSqlServer.TabStop = true;
            this.radioButtonCsvSqlServer.Text = "CSV SQL Server";
            this.radioButtonCsvSqlServer.UseVisualStyleBackColor = true;
            this.radioButtonCsvSqlServer.CheckedChanged += new System.EventHandler(this.RadioButtonCsvSqlServer_CheckedChanged);
            // 
            // radioButtonCsvSerasa
            // 
            this.radioButtonCsvSerasa.AutoSize = true;
            this.radioButtonCsvSerasa.Location = new System.Drawing.Point(6, 19);
            this.radioButtonCsvSerasa.Name = "radioButtonCsvSerasa";
            this.radioButtonCsvSerasa.Size = new System.Drawing.Size(58, 17);
            this.radioButtonCsvSerasa.TabIndex = 0;
            this.radioButtonCsvSerasa.Text = "Serasa";
            this.radioButtonCsvSerasa.UseVisualStyleBackColor = true;
            this.radioButtonCsvSerasa.CheckedChanged += new System.EventHandler(this.RadioButtonCsvSerasa_CheckedChanged);
            // 
            // openFileDialogDePara
            // 
            this.openFileDialogDePara.Filter = "CSV|*.csv";
            // 
            // openFileDialogBalanco
            // 
            this.openFileDialogBalanco.Filter = "CSV|*.csv";
            this.openFileDialogBalanco.Multiselect = true;
            // 
            // openFileDialogNotaExplicativa
            // 
            this.openFileDialogNotaExplicativa.Filter = "CSV|*.csv";
            this.openFileDialogNotaExplicativa.Multiselect = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButtonTipoBalancoTodos);
            this.groupBox4.Controls.Add(this.radioButtonTipoBalancoProvisorio);
            this.groupBox4.Controls.Add(this.radioButtonTipoBalancoNormal);
            this.groupBox4.Location = new System.Drawing.Point(324, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(208, 47);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tipo de balanço";
            // 
            // radioButtonTipoBalancoTodos
            // 
            this.radioButtonTipoBalancoTodos.AutoSize = true;
            this.radioButtonTipoBalancoTodos.Checked = true;
            this.radioButtonTipoBalancoTodos.Location = new System.Drawing.Point(147, 19);
            this.radioButtonTipoBalancoTodos.Name = "radioButtonTipoBalancoTodos";
            this.radioButtonTipoBalancoTodos.Size = new System.Drawing.Size(55, 17);
            this.radioButtonTipoBalancoTodos.TabIndex = 2;
            this.radioButtonTipoBalancoTodos.TabStop = true;
            this.radioButtonTipoBalancoTodos.Text = "Todos";
            this.radioButtonTipoBalancoTodos.UseVisualStyleBackColor = true;
            // 
            // radioButtonTipoBalancoProvisorio
            // 
            this.radioButtonTipoBalancoProvisorio.AutoSize = true;
            this.radioButtonTipoBalancoProvisorio.Location = new System.Drawing.Point(70, 19);
            this.radioButtonTipoBalancoProvisorio.Name = "radioButtonTipoBalancoProvisorio";
            this.radioButtonTipoBalancoProvisorio.Size = new System.Drawing.Size(71, 17);
            this.radioButtonTipoBalancoProvisorio.TabIndex = 1;
            this.radioButtonTipoBalancoProvisorio.Text = "Provisório";
            this.radioButtonTipoBalancoProvisorio.UseVisualStyleBackColor = true;
            // 
            // radioButtonTipoBalancoNormal
            // 
            this.radioButtonTipoBalancoNormal.AutoSize = true;
            this.radioButtonTipoBalancoNormal.Location = new System.Drawing.Point(6, 19);
            this.radioButtonTipoBalancoNormal.Name = "radioButtonTipoBalancoNormal";
            this.radioButtonTipoBalancoNormal.Size = new System.Drawing.Size(69, 17);
            this.radioButtonTipoBalancoNormal.TabIndex = 0;
            this.radioButtonTipoBalancoNormal.Text = "Definitivo";
            this.radioButtonTipoBalancoNormal.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton1);
            this.groupBox5.Controls.Add(this.radioButtonTipoClienteGrupo);
            this.groupBox5.Controls.Add(this.radioButtonTipoClienteEmpresa);
            this.groupBox5.Location = new System.Drawing.Point(538, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(253, 47);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tipo de cliente";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(194, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(55, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Todos";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButtonTipoClienteGrupo
            // 
            this.radioButtonTipoClienteGrupo.AutoSize = true;
            this.radioButtonTipoClienteGrupo.Location = new System.Drawing.Point(78, 19);
            this.radioButtonTipoClienteGrupo.Name = "radioButtonTipoClienteGrupo";
            this.radioButtonTipoClienteGrupo.Size = new System.Drawing.Size(110, 17);
            this.radioButtonTipoClienteGrupo.TabIndex = 1;
            this.radioButtonTipoClienteGrupo.Text = "Grupo Econômico";
            this.radioButtonTipoClienteGrupo.UseVisualStyleBackColor = true;
            // 
            // radioButtonTipoClienteEmpresa
            // 
            this.radioButtonTipoClienteEmpresa.AutoSize = true;
            this.radioButtonTipoClienteEmpresa.Location = new System.Drawing.Point(6, 19);
            this.radioButtonTipoClienteEmpresa.Name = "radioButtonTipoClienteEmpresa";
            this.radioButtonTipoClienteEmpresa.Size = new System.Drawing.Size(66, 17);
            this.radioButtonTipoClienteEmpresa.TabIndex = 0;
            this.radioButtonTipoClienteEmpresa.Text = "Empresa";
            this.radioButtonTipoClienteEmpresa.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxClientesEspecificos);
            this.groupBox6.Controls.Add(this.checkBoxClientesEspecificos);
            this.groupBox6.Location = new System.Drawing.Point(12, 65);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(388, 75);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Migrar cliente(s) específico(s) (separar por ; (ponto e vírgula))";
            // 
            // textBoxClientesEspecificos
            // 
            this.textBoxClientesEspecificos.Location = new System.Drawing.Point(139, 16);
            this.textBoxClientesEspecificos.Multiline = true;
            this.textBoxClientesEspecificos.Name = "textBoxClientesEspecificos";
            this.textBoxClientesEspecificos.Size = new System.Drawing.Size(237, 53);
            this.textBoxClientesEspecificos.TabIndex = 18;
            this.textBoxClientesEspecificos.Text = "11309576;";
            // 
            // checkBoxClientesEspecificos
            // 
            this.checkBoxClientesEspecificos.AutoSize = true;
            this.checkBoxClientesEspecificos.Location = new System.Drawing.Point(6, 19);
            this.checkBoxClientesEspecificos.Name = "checkBoxClientesEspecificos";
            this.checkBoxClientesEspecificos.Size = new System.Drawing.Size(127, 17);
            this.checkBoxClientesEspecificos.TabIndex = 17;
            this.checkBoxClientesEspecificos.Text = "Cliente(s) espcífico(s)";
            this.checkBoxClientesEspecificos.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.checkBoxAmbienteProducao);
            this.groupBox7.Controls.Add(this.checkBoxAmbienteHomologacao);
            this.groupBox7.Controls.Add(this.checkBoxAmbienteTI);
            this.groupBox7.Location = new System.Drawing.Point(406, 65);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(219, 47);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ambiente";
            // 
            // checkBoxAmbienteProducao
            // 
            this.checkBoxAmbienteProducao.AutoSize = true;
            this.checkBoxAmbienteProducao.Location = new System.Drawing.Point(144, 19);
            this.checkBoxAmbienteProducao.Name = "checkBoxAmbienteProducao";
            this.checkBoxAmbienteProducao.Size = new System.Drawing.Size(72, 17);
            this.checkBoxAmbienteProducao.TabIndex = 2;
            this.checkBoxAmbienteProducao.Text = "Produção";
            this.checkBoxAmbienteProducao.UseVisualStyleBackColor = true;
            this.checkBoxAmbienteProducao.CheckedChanged += new System.EventHandler(this.RadioButtonAmbienteProducao_CheckedChanged);
            // 
            // checkBoxAmbienteHomologacao
            // 
            this.checkBoxAmbienteHomologacao.AutoSize = true;
            this.checkBoxAmbienteHomologacao.Location = new System.Drawing.Point(47, 19);
            this.checkBoxAmbienteHomologacao.Name = "checkBoxAmbienteHomologacao";
            this.checkBoxAmbienteHomologacao.Size = new System.Drawing.Size(92, 17);
            this.checkBoxAmbienteHomologacao.TabIndex = 1;
            this.checkBoxAmbienteHomologacao.Text = "Homologação";
            this.checkBoxAmbienteHomologacao.UseVisualStyleBackColor = true;
            this.checkBoxAmbienteHomologacao.CheckedChanged += new System.EventHandler(this.RadioButtonAmbienteHomologacao_CheckedChanged);
            // 
            // checkBoxAmbienteTI
            // 
            this.checkBoxAmbienteTI.AutoSize = true;
            this.checkBoxAmbienteTI.Checked = true;
            this.checkBoxAmbienteTI.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAmbienteTI.Location = new System.Drawing.Point(6, 19);
            this.checkBoxAmbienteTI.Name = "checkBoxAmbienteTI";
            this.checkBoxAmbienteTI.Size = new System.Drawing.Size(36, 17);
            this.checkBoxAmbienteTI.TabIndex = 0;
            this.checkBoxAmbienteTI.Text = "TI";
            this.checkBoxAmbienteTI.UseVisualStyleBackColor = true;
            this.checkBoxAmbienteTI.CheckedChanged += new System.EventHandler(this.RadioButtonAmbienteTI_CheckedChanged);
            // 
            // buttonLimparListas
            // 
            this.buttonLimparListas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLimparListas.Location = new System.Drawing.Point(555, 424);
            this.buttonLimparListas.Name = "buttonLimparListas";
            this.buttonLimparListas.Size = new System.Drawing.Size(113, 23);
            this.buttonLimparListas.TabIndex = 20;
            this.buttonLimparListas.Text = "Limpar listas";
            this.buttonLimparListas.UseVisualStyleBackColor = true;
            this.buttonLimparListas.Click += new System.EventHandler(this.ButtonLimparListas_Click);
            // 
            // FormMigracaoLegado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 472);
            this.Controls.Add(this.buttonLimparListas);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonInicioImportacao);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormMigracaoLegado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Migrar dados legado";
            this.Load += new System.EventHandler(this.FormMigracaoLegado_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.Button buttonInicioImportacao;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonCsvMainframe;
        private System.Windows.Forms.RadioButton radioButtonCsvSqlServer;
        private System.Windows.Forms.RadioButton radioButtonCsvSerasa;
        private System.Windows.Forms.Button buttonSelectNotaExplicativa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSelectBalanco;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialogDePara;
        private System.Windows.Forms.Button buttonSelectPlanoContasDePara;
        private System.Windows.Forms.TextBox textBoxDePara;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.OpenFileDialog openFileDialogBalanco;
        private System.Windows.Forms.OpenFileDialog openFileDialogNotaExplicativa;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButtonTipoBalancoTodos;
        private System.Windows.Forms.RadioButton radioButtonTipoBalancoProvisorio;
        private System.Windows.Forms.RadioButton radioButtonTipoBalancoNormal;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButtonTipoClienteGrupo;
        private System.Windows.Forms.RadioButton radioButtonTipoClienteEmpresa;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBoxClientesEspecificos;
        private System.Windows.Forms.CheckBox checkBoxClientesEspecificos;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox checkBoxAmbienteProducao;
        private System.Windows.Forms.CheckBox checkBoxAmbienteHomologacao;
        private System.Windows.Forms.CheckBox checkBoxAmbienteTI;
        private System.Windows.Forms.TextBox textBoxJsonSaida;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBoxBalanco;
        private System.Windows.Forms.ListBox listBoxNotaExplicativa;
        private System.Windows.Forms.Button buttonLimparListas;
        private System.Windows.Forms.Button buttonRemoverNotaExplicativa;
        private System.Windows.Forms.Button buttonRemoverBalanco;
    }
}


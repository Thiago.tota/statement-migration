﻿using AplMigrationBal.View.Forms;
using System.Windows;

namespace AplMigrationBal.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonMigration_Click(object sender, RoutedEventArgs e)
        {
            new FormMigracaoLegado().Show();
        }

        private void ButtonStatementCheck_Click(object sender, RoutedEventArgs e)
        {
            new AplMigrationBal.StatementCheck.MainWindow { Owner = this }.Show();
        }

        private void ButtonAccountPlan_Click(object sender, RoutedEventArgs e)
        {
            new AccountPlan { Owner = this }.Show();
        }

        private void ButtonStatementDate_Click(object sender, RoutedEventArgs e)
        {
            new StatementDate { Owner = this }.Show();
        }
    }
}

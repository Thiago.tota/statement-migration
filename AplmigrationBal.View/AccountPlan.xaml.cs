﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Windows;

namespace AplMigrationBal.View
{
    /// <summary>
    /// Interaction logic for AccountPlan.xaml
    /// </summary>
    public partial class AccountPlan : Window
    {
        enum Action : byte
        {
            LoadFromDatabase,
            LoadFromCsv,
            SaveOntoDatabase,
            SaveOntoCsv
        }

        public AccountPlan()
        {
            InitializeComponent();
        }

        private void ButtonProcess_Click(object sender, RoutedEventArgs e)
        {
            Action action;

            try
            {
                if (radioButtonReadDatabase.IsChecked == true)
                    action = Action.LoadFromDatabase;
                else if (radioButtonReadCSV.IsChecked == true)
                    action = Action.LoadFromCsv;
                else if (radioButtonSaveDatabse.IsChecked == true)
                    action = Action.SaveOntoDatabase;
                else
                    action = Action.SaveOntoCsv;

                textBoxReturnMessage.Text = string.Empty;
                switch (action)
                {
                    case Action.LoadFromDatabase:
                        textBoxAccoutPlan.Text = ReadAccountPlanFromDatabase();
                        break;
                    case Action.LoadFromCsv:
                        if (string.IsNullOrEmpty(textBoxNewAccoutPlan.Text.Trim()) || !textBoxNewAccoutPlan.Text.Trim().EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                            textBoxReturnMessage.Text = "Selecione o aruquivo CSV plano de contas";
                        else
                            textBoxAccoutPlan.Text = ReadAccountPlanFromCsv(textBoxNewAccoutPlan.Text);
                        break;
                    case Action.SaveOntoDatabase:
                        if (SaveAccountPlanOntoDatabase())
                            textBoxReturnMessage.Text = "Plano de contas salvo com sucesso";
                        else
                            textBoxReturnMessage.Text = "Erro ao tentar salvar Plano de contas";
                        break;
                    case Action.SaveOntoCsv:
                        if (SaveAccountPlanOntoCsv())
                            textBoxReturnMessage.Text = "Plano de contas salvo com sucesso em " + Global.AccountPlanPath;
                        else
                            textBoxReturnMessage.Text = "Erro ao tentar salvar Plano de contas";
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
        }

        private void ButtonSelectAccoutPlan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.DefaultExt = ".csv";

                var dialogResult = openFileDialog.ShowDialog();

                if (dialogResult == true)
                {
                    textBoxNewAccoutPlan.Text = openFileDialog.FileName;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
        }

        private string ReadAccountPlanFromCsv(string filePath)
        {
            string result = string.Empty;

            try
            {
                result = Json.Serialize(new Business.AccountPlan().GenerateAccountPlain(filePath), Helper.Enum.JsonFormatting.Indented);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        private string ReadAccountPlanFromDatabase()
        {
            string result = string.Empty;

            try
            {
                result = Json.Serialize(new Business.AccountPlan().GetAccountPlanJson(Global.CollectionAccountPlan), Helper.Enum.JsonFormatting.Indented);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        private bool SaveAccountPlanOntoCsv()
        {
            bool result = false;

            try
            {
                Model.Json.AccountPlan accountPlanJson = Json.Deserialize<Model.Json.AccountPlan>(textBoxAccoutPlan.Text);
                accountPlanJson.CreatedAt = DateTime.UtcNow;
                accountPlanJson.UpdatedAt = DateTime.UtcNow;
                result = new Business.AccountPlan().SaveAccountPlan(accountPlanJson, Global.AccountPlanPath + "PlanoContas_vX.X.csv");
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        private bool SaveAccountPlanOntoDatabase()
        {
            bool result = false;

            try
            {
                Model.Json.AccountPlan accountPlanJson = Json.Deserialize<Model.Json.AccountPlan>(textBoxAccoutPlan.Text);
                accountPlanJson.CreatedAt = DateTime.UtcNow;
                accountPlanJson.UpdatedAt = DateTime.UtcNow;
                accountPlanJson._id = Global.NewMongoGuid;
                result = new Business.AccountPlan().SaveAccountPlan(accountPlanJson, Global.CollectionAccountPlan, true);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        private void RadioButtonTI_Checked(object sender, RoutedEventArgs e)
        {
            Global.SetEnvironment(Helper.Enum.Environment.TI);
        }

        private void RadioButtonHML_Checked(object sender, RoutedEventArgs e)
        {
            Global.SetEnvironment(Helper.Enum.Environment.Homologacao);
        }

        private void RadioButtonPRD_Checked(object sender, RoutedEventArgs e)
        {
            Global.SetEnvironment(Helper.Enum.Environment.Producao);
        }
    }
}

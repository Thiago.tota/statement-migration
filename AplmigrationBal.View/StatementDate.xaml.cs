﻿using AplMigrationBal.Helper.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AplMigrationBal.View
{
    /// <summary>
    /// Interaction logic for StatementDate.xaml
    /// </summary>
    public partial class StatementDate : Window
    {
        public StatementDate()
        {
            InitializeComponent();
        }

        private void ButtonGenerateStatementDateFile_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonHML.IsChecked == true)
                Global.SetEnvironment(Helper.Enum.Environment.Homologacao);
            else if (radioButtonPRD.IsChecked == true)
                Global.SetEnvironment(Helper.Enum.Environment.Producao);
            else
                Global.SetEnvironment(Helper.Enum.Environment.TI);

            string path = textBoxStatementCheckFile.Text.Substring(0, textBoxStatementCheckFile.Text.LastIndexOf(@"\") + 1);
            string fileName = textBoxStatementCheckFile.Text.Substring(textBoxStatementCheckFile.Text.LastIndexOf(@"\") + 1);

            if (new Business.Statement().SaveStatementDateFile(path, fileName))
                MessageBox.Show(string.Format("Arquivo gerado com sucesso em {0}", textBoxStatementCheckFile.Text), "Data de balanços", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show("Erro ao gerar arquivo", "Data de balanços", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxStatementCheckFile.Text = Global.StatementDatePath + Global.StatementDateFile;
        }
    }
}

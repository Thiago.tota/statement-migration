﻿using System;

namespace AplMigrationBal.Model.Interface
{
    public interface IStatementDataFile
    {
        string Analist { get; set; }
        ulong? AccountId { get; set; }
        double ClientId { get; set; }
        Helper.Enum.ClienteType ClientType { get; set; }
        byte Month { get; set; }
        int Year { get; set; }
        Helper.Enum.StatementStatus Status { get; set; }
        Helper.Enum.StatementType Type { get; set; }
        double? Percentage { get; set; }
        double Value { get; set; }
        Helper.Enum.StatementSource Source { get; set; }
        DateTime UpdatedDate { get; set; }
    }
}
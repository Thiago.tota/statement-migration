﻿using AplMigrationBal.Model.Json;
using System.Collections.Generic;

namespace AplMigrationBal.Model.Interface
{
    public interface IStatement
    {
        ulong? Conta { get; set; }
        string Valor { get; set; }
        IEnumerable<Note> NotaVinculadaValor { get; set; }
        IEnumerable<Note> NotaVinculadaConta { get; set; }
        uint Ordem { get; set; }
    }
}

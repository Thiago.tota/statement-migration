﻿namespace AplMigrationBal.Model.Interface
{
    public interface IExplanatoryNoteDataFile
    {
        int Year { get; set; }
        byte Month { get; set; }
        Helper.Enum.StatementType StatementType { get; set; }
        Helper.Enum.StatementStatus StatementStatus { get; set; }
        double ClientId { get; set; }
        Helper.Enum.ClienteType ClientType { get; set; }
        string TypeCode { get; set; }
        ulong? AccountId { get; set; }
        string Value { get; set; }
        Helper.Enum.StatementSource Source { get; set; }
        uint Order { get; set; }
    }
}

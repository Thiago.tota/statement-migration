﻿namespace AplMigrationBal.Model.Attribute
{
    public class FieldCsvOrder : System.Attribute
    {
        public uint Order { get; set; }
    }
}

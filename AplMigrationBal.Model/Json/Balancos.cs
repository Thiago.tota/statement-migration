﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace AplMigrationBal.Model.Json
{
    [BsonIgnoreExtraElements]
    public class Contas
    {
        public List<Ativo> Ativo { get; set; }
        public List<Passivo> Passivo { get; set; }
        public List<Dre> Dre { get; set; }
        public List<Mutacao> Mutacao { get; set; }
        public List<FluxoCaixa> FluxoCaixa { get; set; }
        public List<Indicadores> Indicadores { get; set; }
        public List<Highlights> Highlights { get; set; }
        public List<Note> NotaExplicativa { get; set; }
        public List<Note> Auditoria { get; set; }

        public Contas()
        {
            Ativo = new List<Ativo>();
            Passivo = new List<Passivo>();
            Dre = new List<Dre>();
            Mutacao = new List<Mutacao>();
            FluxoCaixa = new List<FluxoCaixa>();
            Indicadores = new List<Indicadores>();
            Highlights = new List<Highlights>();
            NotaExplicativa = new List<Note>();
            Auditoria = new List<Note>();
        }
    }
}
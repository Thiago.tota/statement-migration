﻿using AplMigrationBal.Helper.Serializer;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace AplMigrationBal.Model.Json
{
    [BsonIgnoreExtraElements]
    public class Dados
    {
        public string Analista { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime Referencia { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public Helper.Enum.StatementType Tipo { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public Helper.Enum.StatementStatus Status { get; set; }
        
        public string Fonte { get; set; }

        public string Moeda { get; set; }

        public string Confidencial { get; set; }

        public bool HighlightAuditoria { get; set; }

        public bool HighlightNotaExplicativa { get; set; }

        public string PlanoDeContas { get; set; }

        [BsonDateTimeOptions( Kind = DateTimeKind.Utc)]
        public DateTime DataAtualizacao { get; set; }

        public Contas Contas { get; set; }

        public Dados()
        {
            Contas = new Contas();
        }
    }
}

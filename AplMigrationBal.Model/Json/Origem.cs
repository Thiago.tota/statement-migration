﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AplMigrationBal.Model.Json
{
    [BsonIgnoreExtraElements]
    public class Origem
    {
        public string Conta { get; set; }
        public string Descricao { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public Helper.Enum.StatementSource Fonte { get; set; }
    }
}

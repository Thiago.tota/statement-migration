﻿using System.Collections.Generic;

namespace AplMigrationBal.Model.Json
{
    public class Account
    {
        public ulong? AccountCode { get; set; }
        public byte Level { get; set; }
        public string Description { get; set; }
        public ulong? Parent { get; set; }
        public string Group { get; set; }
        public uint Order { get; set; }
        public bool Show { get; set; }
        public bool Expand { get; set; }
        public bool Blank { get; set; }
        public List<Account> Accounts { get; set; }

        public Account()
        {
            Accounts = new List<Account>();
        }
    }
}

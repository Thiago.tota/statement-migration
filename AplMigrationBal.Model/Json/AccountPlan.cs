﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace AplMigrationBal.Model.Json
{
    public class AccountPlan
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string Id { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public Group Group { get; set; }

        public AccountPlan()
        {
            Group = new Group();
        }
    }
}

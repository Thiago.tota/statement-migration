﻿using AplMigrationBal.Model.DTO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace AplMigrationBal.Model.Json
{
    [BsonIgnoreExtraElements]
    public class Statement
    {
        [JsonIgnore]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public Cliente Cliente { get; set; }
        public Origem Origem { get; set; }
        public Dados Dados { get; set; }

        public Statement()
        {
            Cliente = new Cliente();
            Origem = new Origem();
            Dados = new Dados();
        }

        public StatementKey ToStatementKey()
        {
            return new StatementKey();
        }
    }
}

﻿using AplMigrationBal.Helper.Serializer;
using AplMigrationBal.Model.Interface;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AplMigrationBal.Model.Json
{
    [BsonIgnoreExtraElements]
    public class Ativo : IStatement
    {
        [BsonIgnoreIfNull]
        public ulong? Conta { get; set; }

        [BsonSerializer(typeof(StringOrNumericSerializer))]
        public string Valor { get; set; }
        public IEnumerable<Note> NotaVinculadaValor { get; set; }
        public IEnumerable<Note> NotaVinculadaConta { get; set; }

        [JsonIgnore]
        [BsonIgnore]
        public uint Ordem { get; set; }

        public Ativo()
        {
            NotaVinculadaValor = new List<Json.Note>();
            NotaVinculadaConta = new List<Json.Note>();
        }
    }
}
﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AplMigrationBal.Model.Json
{
    [BsonIgnoreExtraElements]
    public class Cliente
    {
        public double? Cnpj { get; set; }
        public double? CodigoGrupoEconomico { get; set; }
        public string NomeControlador { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [BsonRepresentation(BsonType.String)]
        public Helper.Enum.ClienteType TipoCliente { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace AplMigrationBal.Model.Json
{
    public class Group
    {
        public List<Account> Ativo { get; set; }
        public List<Account> Passivo { get; set; }
        public List<Account> Dre { get; set; }
        public List<Account> Mutacao { get; set; }
        public List<Account> FluxoCaixa { get; set; }
        public List<Account> Indicadores { get; set; }
        public List<Account> Highlights { get; set; }

        public Group()
        {
            Ativo = new List<Account>();
            Passivo = new List<Account>();
            Dre = new List<Account>();
            Mutacao = new List<Account>();
            FluxoCaixa = new List<Account>();
            Indicadores = new List<Account>();
            Highlights = new List<Account>();

        }
    }
}
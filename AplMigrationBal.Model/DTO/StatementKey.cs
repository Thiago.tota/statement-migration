﻿namespace AplMigrationBal.Model.DTO
{
    public class StatementKey
    {
        #region Public Properties

        public double ClientId { get; set; }
        public Helper.Enum.ClienteType ClientType { get; set; }
        public byte Month { get; set; }
        public int Year { get; set; }
        public Helper.Enum.StatementStatus Status { get; set; }
        public Helper.Enum.StatementType Type { get; set; }
        public Helper.Enum.StatementSource Source { get; set; }
        
        #endregion Public Properties
    }
}
﻿using System.Collections.Generic;

namespace AplMigrationBal.Model.DTO
{
    public class AccountPlan
    {
        public string Name { get; set; }

        public List<AccountPlanItens> Accounts { get; set; }

        public AccountPlan()
        {
            Accounts = new List<AccountPlanItens>();
        }
    }
}

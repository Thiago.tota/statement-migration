﻿using System.Collections.Generic;

namespace AplMigrationBal.Model.DTO
{
    public class InputImportSettings
    {
        public List<Helper.Enum.Environment> Environment { get; set; }

        public string FromTo { get; set; }
        public List<string> Statement { get; set; }
        public bool HasExplanatoryNotes { get; set; }
        public List<string> ExplanatoryNote { get; set; }
        public string JsonOutputPath { get; set; }

        public Helper.Enum.StatementStatus StatementStatus { get; set; }
        public Helper.Enum.ClienteType ClientType { get; set; }
        public Helper.Enum.StatementSource StatementSource { get; set; }
        public List<double> ClientId { get; set; }

        public InputImportSettings()
        {
            Environment = new List<Helper.Enum.Environment>();
            Statement = new List<string>();
            ExplanatoryNote = new List<string>();
        }
    }
}

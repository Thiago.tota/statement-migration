﻿using AplMigrationBal.Helper.Extensions;
using AplMigrationBal.Model.DTO.Abstract;

namespace AplMigrationBal.Model.DTO.Serasa
{
    public class Statement : BaseStatement
    {
        #region Public Methods

        public override T FromCsv<T>(string line, char[] separator)
        {
            string[] rowFields = line.Split(separator);

            if (rowFields.Length < 16)
                rowFields = "0;0;Balancete;;Provisorio;G;0;0;0;;;;;;;0".Split(separator);

            double value = double.Parse(rowFields[8].Clean());

            Statement row = new Statement
            {
                Year = ushort.Parse(rowFields[0].Clean()),
                Month = byte.Parse(rowFields[1].Clean()),
                Type = (Helper.Enum.StatementType)System.Enum.Parse(typeof(Helper.Enum.StatementType), rowFields[2].Clean(), true),
                Analist = rowFields[3].Clean(),
                Status = (Helper.Enum.StatementStatus)System.Enum.Parse(typeof(Helper.Enum.StatementStatus), rowFields[4].Clean().Substring(0, 10), true),
                ClientType = (Helper.Enum.ClienteType)System.Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[5].Clean() == "C" ? "E" : rowFields[5].Clean(), true),
                ClientId = double.Parse(rowFields[6].Clean()),
                AccountId = ulong.Parse(rowFields[7].Clean()),
                Value = value,
                Percentage = (rowFields.Length > 15) ? double.Parse(rowFields[15].Clean()) : (double?)null,
                Source = Helper.Enum.StatementSource.ImportacaoSerasa
            };

            return (T)(object)row;
        }

        #endregion Public Methods
    }
}
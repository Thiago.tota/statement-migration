﻿using AplMigrationBal.Model.Interface;
using System.Collections.Generic;

namespace AplMigrationBal.Model.DTO
{
    public class StatementToProcess
    {
        public List<StatementDataFile> Statements;
        public List<ExplanatoryNoteDataFile> ExplanatoryNotes;

        public StatementToProcess()
        {
            Statements = new List<StatementDataFile>();
            ExplanatoryNotes = new List<ExplanatoryNoteDataFile>();
        }
    }
}

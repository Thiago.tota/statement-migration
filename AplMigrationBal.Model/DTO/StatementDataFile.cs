﻿using AplMigrationBal.Model.Interface;
using System;

namespace AplMigrationBal.Model.DTO
{
    public class StatementDataFile : IStatementDataFile
    {
        public object Id { get; set; }
        public string Analist { get; set; }
        public ulong? AccountId { get; set; }
        public double ClientId { get; set; }
        public Helper.Enum.ClienteType ClientType { get; set; }
        public byte Month { get; set; }
        public int Year { get; set; }
        public Helper.Enum.StatementStatus Status { get; set; }
        public Helper.Enum.StatementType Type { get; set; }
        public double? Percentage { get; set; }
        public double Value { get; set; }
        public Helper.Enum.StatementSource Source { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}

﻿using AplMigrationBal.Model.DTO.Abstract;

namespace AplMigrationBal.Model.DTO.SQL
{
    public class Statement : BaseStatement
    {
        #region Public Methods

        public override T FromCsv<T>(string line, char[] separator)
        {
            string[] rowFields = line.Split(separator);
            if (rowFields.Length < 10)
                rowFields = "0;0;BALANCETE;;C;0;0;;0;0".Split(separator);
            double value = double.Parse(rowFields[9]);

            Statement row = new Statement
            {
                Year = ushort.Parse(rowFields[0]),
                Month = byte.Parse(rowFields[1]),
                Type = (Helper.Enum.StatementType)System.Enum.Parse(typeof(Helper.Enum.StatementType), rowFields[2].Trim(), true),
                Analist = rowFields[3].Trim(),
                Status = (Helper.Enum.StatementStatus)System.Enum.Parse(typeof(Helper.Enum.StatementStatus), rowFields[4].Trim().Substring(0, 10), true),
                ClientType = (Helper.Enum.ClienteType)System.Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[5].Trim() == "C" ? "E" : rowFields[5].Trim(), true),
                ClientId = double.Parse(rowFields[6].Trim()),
                AccountId = ulong.Parse(rowFields[7].Trim()),
                Value = value,
                Percentage = (rowFields.Length > 10) ? double.Parse(rowFields[10].Replace('.', ',')) : (double?)null,
                Source = Helper.Enum.StatementSource.ImportacaoSQL
            };

            return (T)(object)row;
        }

        #endregion Public Methods
    }
}
﻿using AplMigrationBal.Model.DTO.Abstract;

namespace AplMigrationBal.Model.DTO.SQL
{
    public class ExplanatoryNote : BaseExplanatoryNote
    {
        #region Public Methods

        public override T FromCsv<T>(string line, char[] separator)
        {
            string[] rowFields = line.Split(separator);

            ExplanatoryNote row = new ExplanatoryNote
            {
                Year = ushort.Parse(rowFields[0]),
                Month = byte.Parse(rowFields[1]),
                StatementType = (Helper.Enum.StatementType)System.Enum.Parse(typeof(Helper.Enum.StatementType), rowFields[2].Trim(), true),
                StatementStatus = (Helper.Enum.StatementStatus)System.Enum.Parse(typeof(Helper.Enum.StatementStatus), rowFields[3].Trim().Substring(0, 10), true),
                ClientType = (Helper.Enum.ClienteType)System.Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[4].Trim() == "C" ? "E" : rowFields[4].Trim(), true),
                ClientId = double.Parse(rowFields[5]),
                TypeCode = rowFields[6].Trim(),
                AccountId = ulong.Parse(rowFields[7].Trim()),
                Value = rowFields[8],
                Source = Helper.Enum.StatementSource.ImportacaoSQL
            };

            return (T)(object)row;
        }

        #endregion Public Methods
    }
}
﻿using AplMigrationBal.Model.Interface;
using AplMigrationBal.Model.Json;
using System.Collections.Generic;

namespace AplMigrationBal.Model.DTO
{
    public class Statement : IStatement
    {
        public ulong? Conta { get; set; }
        public string Valor { get; set; }
        public double? Porcentagem { get; set; }
        public IEnumerable<Note> NotaVinculadaValor { get; set; }
        public IEnumerable<Note> NotaVinculadaConta { get; set; }
        public uint Ordem { get; set; }
        public Helper.Enum.AccountGroup Grupo { get; set; }

        public Statement()
        {
            NotaVinculadaValor = new List<Json.Note>();
            NotaVinculadaConta = new List<Json.Note>();
        }
    }
}
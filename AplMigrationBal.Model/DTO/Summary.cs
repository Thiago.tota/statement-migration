﻿using System;
using System.Collections.Generic;

namespace AplMigrationBal.Model.DTO
{
    public class Summary
    {
        public List<StatementClient> StatementClient { get; set; }
        public List<int> StatementYear { get; set; }
        public TimeSpan ProcessingTime { get; set; }
        public int TotalLines { get; set; }
        public int TotalStatements { get; set; }
        public int TotalClients { get; set; }
        public string StatementSavingMessage { get; set; }

        public Summary()
        {
            StatementClient = new List<StatementClient>();
            StatementYear = new List<int>();
        }
    }

    public class StatementClient
    {
        public Helper.Enum.ClienteType ClienteType { get; set; }
        public double ClientId { get; set; }
        public int Year { get; set; }
    }
}

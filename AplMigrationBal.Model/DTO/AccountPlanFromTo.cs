﻿using AplMigrationBal.Helper.Interface;
using AplMigrationBal.Model.Attribute;
using System;

namespace AplMigrationBal.Model.DTO
{
    public class AccountPlanFromTo : ICsv
    {
        #region Public Properties
        [FieldCsvOrder(Order = 0)]
        public ulong? Account { get; set; }

        [FieldCsvOrder(Order = 1)]
        public ulong? SumAccount { get; set; }

        [FieldCsvOrder(Order = 2)]
        public ulong? OriginAccount { get; set; }

        [FieldCsvOrder(Order = 3)]
        public double? Mutiplier { get; set; }

        public Helper.Enum.AccountPlanFromTo Type { get; set; }

        #endregion Public Properties

        #region Public Methods

        public T FromCsv<T>(string line, char[] separator) where T : new()
        {
            double multiplicador;
            var rowFields = line.Split(separator);

            return (T)(object)new AccountPlanFromTo
            {
                Account = string.IsNullOrEmpty(rowFields[0].Trim()) ? (ulong?)null : Convert.ToUInt64(rowFields[0].Trim()),
                OriginAccount = string.IsNullOrEmpty(rowFields[1].Trim()) ? (ulong?)null : Convert.ToUInt64(rowFields[1].Trim()),
                SumAccount = string.IsNullOrEmpty(rowFields[2].Trim()) ? (ulong?)null : Convert.ToUInt64(rowFields[2].Trim()),
                Mutiplier = double.TryParse(rowFields[3].Trim(), out multiplicador) ? multiplicador : (double?)null
            };
        }

        public string WriteCsv(char separator)
        {
            throw new NotImplementedException();
        }

        public string WriteHeader(char separator)
        {
            return "CONTA; CONTA ORIGEM; CONTA SOMATORIA; MULTIPLICADOR";
        }

        #endregion
    }
}
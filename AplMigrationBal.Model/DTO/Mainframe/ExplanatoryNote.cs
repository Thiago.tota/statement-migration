﻿using AplMigrationBal.Helper.Extensions;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO.Abstract;
using System;
using System.Diagnostics;

namespace AplMigrationBal.Model.DTO.Mainframe
{
    public class ExplanatoryNote : BaseExplanatoryNote
    {
        #region Public Methods

        public override T FromCsv<T>(string line, char[] separator)
        {
            string[] rowFields = line.Split(separator);
            ExplanatoryNote row = null;
            if (rowFields.Length < 10)
                rowFields = "0;0;Balancete;Provisorio;G;0;;99999;1;".Split(separator);
            try
            {
                row = new ExplanatoryNote
                {
                    Year = ushort.Parse(rowFields[0].Clean()),
                    Month = byte.Parse(rowFields[1].Clean()),
                    StatementType = (Helper.Enum.StatementType)System.Enum.Parse(typeof(Helper.Enum.StatementType), rowFields[2].Clean(), true),
                    StatementStatus = (Helper.Enum.StatementStatus)System.Enum.Parse(typeof(Helper.Enum.StatementStatus), rowFields[3].Clean().Substring(0, 10), true),
                    ClientType = (Helper.Enum.ClienteType)System.Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[4].Clean() == "C" ? "E" : rowFields[4].Clean(), true),
                    ClientId = double.Parse(rowFields[5].Clean()),
                    TypeCode = rowFields[6].Clean(),
                    AccountId = 0,
                    Order = uint.Parse(rowFields[7].Clean()) * 100 + uint.Parse(rowFields[8].Clean()),
                    Value = rowFields[9].CleanWithSpaces(),
                    Source = Helper.Enum.StatementSource.ImportacaoMainframe
                };
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
            return (T)(object)row;
        }

        #endregion Public Methods
    }
}
﻿using AplMigrationBal.Helper.Extensions;
using AplMigrationBal.Model.DTO.Abstract;
using AplMigrationBal.Model.Interface;
using System;

namespace AplMigrationBal.Model.DTO.Mainframe
{
    public class Statement : BaseStatement
    {
        #region Public Methods

        public override T FromCsv<T>(string line, char[] separator)
        {
            string[] rowFields = line.Split(separator);

            if (rowFields.Length < 16)
                rowFields = "0;0;Balancete;;Provisorio;G;0;0;0;;;;;;;0".Split(separator);

            double value = double.Parse(rowFields[8].Clean());

            Statement row = new Statement
            {
                Year = ushort.Parse(rowFields[0].Clean()),
                Month = byte.Parse(rowFields[1].Clean()),
                Type = (Helper.Enum.StatementType)System.Enum.Parse(typeof(Helper.Enum.StatementType), rowFields[2].Clean(), true),
                Analist = rowFields[3].Clean(),
                Status = (Helper.Enum.StatementStatus)System.Enum.Parse(typeof(Helper.Enum.StatementStatus), rowFields[4].Clean().Substring(0, 10), true),
                ClientType = (Helper.Enum.ClienteType)System.Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[5].Clean() == "C" ? "E" : rowFields[5].Clean(), true),
                ClientId = double.Parse(rowFields[6].Clean()),
                AccountId = ulong.Parse(rowFields[7].Length >= 5 ? rowFields[7].Substring(5).Clean() : rowFields[7].Clean()),
                Value = value,
                Percentage = (rowFields.Length > 15) ? double.Parse(rowFields[15].Clean()) : (double?)null,
                Source = Helper.Enum.StatementSource.ImportacaoMainframe,
                UpdatedDate = rowFields[13].Clean().Split(new char[] { '.' }).Length < 3 ? DateTime.Now :
                    new DateTime(Convert.ToInt32(rowFields[13].Clean().Split(new char[] { '.' })[2]),
                                Convert.ToInt32(rowFields[13].Clean().Split(new char[] { '.' })[1]),
                                Convert.ToInt32(rowFields[13].Clean().Split(new char[] { '.' })[0]))
            };

            return (T)(object)row;
        }

        #endregion Public Methods
    }
}
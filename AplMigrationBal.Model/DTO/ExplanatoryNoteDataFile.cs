﻿using AplMigrationBal.Model.Interface;

namespace AplMigrationBal.Model.DTO
{
    public class ExplanatoryNoteDataFile : IExplanatoryNoteDataFile
    {
        public object Id { get; set; }
        public int Year { get; set; }
        public byte Month { get; set; }
        public Helper.Enum.StatementType StatementType { get; set; }
        public Helper.Enum.StatementStatus StatementStatus { get; set; }
        public double ClientId { get; set; }
        public Helper.Enum.ClienteType ClientType { get; set; }
        public string TypeCode { get; set; }
        public ulong? AccountId { get; set; }
        public string Value { get; set; }
        public Helper.Enum.StatementSource Source { get; set; }
        public uint Order { get; set; }
    }
}

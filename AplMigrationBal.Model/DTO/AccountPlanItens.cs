﻿using AplMigrationBal.Helper.Interface;
using AplMigrationBal.Model.Attribute;
using System;
using System.Linq;

namespace AplMigrationBal.Model.DTO
{
    public class AccountPlanItens : ICsv
    {
        [FieldCsvOrder(Order = 0)]
        public ulong? Account { get; set; }

        [FieldCsvOrder(Order = 1)]
        public byte Level { get; set; }

        [FieldCsvOrder(Order = 2)]
        public string Description { get; set; }

        [FieldCsvOrder(Order = 3)]
        public ulong? Parent { get; set; }

        [FieldCsvOrder(Order = 4)]
        public Helper.Enum.AccountGroup Group { get; set; }

        [FieldCsvOrder(Order = 5)]
        public uint Order { get; set; }

        [FieldCsvOrder(Order = 6)]
        public bool Show { get; set; }

        [FieldCsvOrder(Order = 7)]
        public bool Expand { get; set; }

        [FieldCsvOrder(Order = 8)]
        public bool Blank { get; set; }

        public T FromCsv<T>(string line, char[] separator) where T : new()
        {
            string[] rowFields = line.Split(separator);
            AccountPlanItens row = new AccountPlanItens();

            row.Account = Convert.ToUInt64(rowFields[GetOrder("Account")].Trim());
            row.Level = Convert.ToByte(rowFields[GetOrder("Level")].Trim());
            row.Description = rowFields[GetOrder("Description")].Trim();
            row.Parent = string.IsNullOrEmpty(rowFields[GetOrder("Parent")].Trim()) ? (ulong?)null : Convert.ToUInt64(rowFields[GetOrder("Parent")].Trim());
            row.Group = (Helper.Enum.AccountGroup)System.Enum.Parse(typeof(Helper.Enum.AccountGroup), rowFields[GetOrder("Group")].Trim(), true);
            row.Order = Convert.ToUInt32(rowFields[GetOrder("Order")]);
            row.Show = rowFields[GetOrder("Show")].ToUpper() == "Y" ? true : false;
            row.Expand = rowFields[GetOrder("Expand")].ToUpper() == "Y" ? true : false;
            row.Blank = rowFields[GetOrder("Blank")].ToUpper() == "Y" ? true : false;

            return (T)(object)row;
        }

        public string WriteCsv(char separator)
        {
            return "CONTA;NIVEL;DESCRICAO;PAI;GRUPO;ORDEM;EXIBIR;EXPANDIR;BRANCO";
        }

        public string WriteHeader(char separator)
        {
            throw new NotImplementedException();
        }

        private int GetOrder(string field)
        {
            var order = ((FieldCsvOrder)typeof(AccountPlanItens).GetProperty(field).GetCustomAttributes(false).FirstOrDefault()).Order;
            return (int)order;
        }
    }
}

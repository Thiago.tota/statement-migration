﻿using AplMigrationBal.Helper.Interface;
using AplMigrationBal.Model.Interface;

namespace AplMigrationBal.Model.DTO.Abstract
{
    public abstract class BaseExplanatoryNote : ICsv, IExplanatoryNoteDataFile
    {
        #region Public Properties

        public virtual ulong? AccountId { get; set; }
        public virtual double ClientId { get; set; }
        public virtual Helper.Enum.ClienteType ClientType { get; set; }
        public virtual byte Month { get; set; }
        public virtual Helper.Enum.StatementType StatementType { get; set; }
        public virtual Helper.Enum.StatementStatus StatementStatus { get; set; }
        public virtual string TypeCode { get; set; }
        public virtual string Value { get; set; }
        public virtual int Year { get; set; }
        public virtual Helper.Enum.StatementSource Source { get; set; }
        public virtual uint Order { get; set; }

        #endregion Public Properties

        #region Public Methods

        public abstract T FromCsv<T>(string line, char[] separator) where T : new();

        public string WriteCsv(char separator)
        {
            throw new System.NotImplementedException();
        }

        public string WriteHeader(char separator)
        {
            throw new System.NotImplementedException();
        }

        #endregion Public Methods
    }
}
﻿using AplMigrationBal.Helper.Interface;
using AplMigrationBal.Model.Interface;
using System;

namespace AplMigrationBal.Model.DTO.Abstract
{
    public abstract class BaseStatement : ICsv, IStatementDataFile
    {
        #region Public Properties

        public virtual ulong? AccountId { get; set; }
        public virtual double ClientId { get; set; }        
        public virtual Helper.Enum.ClienteType ClientType { get; set; }
        public virtual byte Month { get; set; }
        public virtual double? Percentage { get; set; }
        public virtual Helper.Enum.StatementStatus Status { get; set; }
        public virtual Helper.Enum.StatementType Type { get; set; }
        public virtual string Analist { get; set; }
        public virtual double Value { get; set; }
        public virtual int Year { get; set; }
        public virtual Helper.Enum.StatementSource Source { get; set; }
        public virtual DateTime UpdatedDate { get; set; }

        #endregion Public Properties

        #region Public Methods

        public abstract T FromCsv<T>(string line, char[] separator) where T : new();

        public string WriteCsv(char separator)
        {
            throw new System.NotImplementedException();
        }

        public string WriteHeader(char separator)
        {
            throw new System.NotImplementedException();
        }

        #endregion Public Methods
    }
}
﻿using AplMigrationBal.Comunication.Internal;
using System;
using System.Collections.Generic;

namespace AplMigrationBal.Comunication
{
    public class AccountPlan
    {
        #region Private Fields

        private readonly IEnumerable<Tuple<string, string>> header = new List<Tuple<string, string>>
        {
            Tuple.Create("amc-message-id", Guid.NewGuid().ToString()),
            Tuple.Create("amc-aplicacao", "BAL"),
            Tuple.Create("amc-session-id", Guid.NewGuid().ToString()),
            Tuple.Create("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4")
        };

        #endregion Private Fields

        #region Public Methods

        public T GetAccountPlan<T>(string uri) where T : Model.Json.AccountPlan
        {
            return new WebRequest().ComunicaHttp<T>(uri, header);
        }

        public T SaveAccountPlan<T>(string uri, object accountPlan) where T : Model.Json.AccountPlan
        {
            return new WebRequest().ComunicaHttp<T>(uri, header, "POST", accountPlan);
        }

        public T GetAccountPlanFromTo<T>(string uri) where T : Model.DTO.AccountPlanFromTo
        {
            throw new NotImplementedException();
            return new WebRequest().ComunicaHttp<T>(uri, header);
        }

        public T SaveAccountPlanFromTo<T>(string uri, object accountPlan) where T : Model.DTO.AccountPlanFromTo
        {
            throw new NotImplementedException();
            return new WebRequest().ComunicaHttp<T>(uri, header, "POST", accountPlan);
        }
        #endregion Public Methods
    }
}
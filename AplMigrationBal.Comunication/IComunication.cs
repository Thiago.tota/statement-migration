﻿using AplMigrationBal.Model.Interface;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
namespace AplMigrationBal.Comunication
{
    public interface IComunication
    {
        bool SaveStatement(List<Model.Json.Statement> statements);
        bool DeleteStatement(List<Model.DTO.StatementKey> statementKeys);
        bool DeleteStatementToProcess(List<Model.DTO.StatementKey> statementKeys);
        bool CleanStatementToProcess();
        bool LoadStatementsToDataBase(IEnumerable<IStatementDataFile> statements);
        bool LoadExplanatoryNotesToDataBase(IEnumerable<IExplanatoryNoteDataFile> explanatoryNotes);
        List<Model.Json.Statement> GetStatement(List<Model.DTO.StatementKey> statementKeys, bool onlyKeyFields = false);
        List<Model.DTO.StatementKey> GetStatementKeys();
        Model.DTO.StatementToProcess GetStatementToProcess(List<Model.DTO.StatementKey> statementKeys);
        List<Model.Json.Statement> GetLatestStatementByClient();
        IEnumerable<T> Find<T>(Expression<Func<T, bool>> filter, string collection) where T : class;
        bool UpdateOne<T>(Expression<Func<T, bool>> filter, List<KeyValuePair<Expression<Func<T, object>>, object>> update, string collection, bool async = false) where T : class;
        bool SaveOne<T>(T data, string collection, bool async = false) where T : class;
        bool SaveMany<T>(List<T> data, string collection, bool async = false) where T : class;
        bool DeleteMany<T>(Expression<Func<T, bool>> filter, string collection, bool async = false) where T : class;
        bool DeleteOne<T>(Expression<Func<T, bool>> filter, string collection, bool async = false) where T : class;
        IEnumerable<AplMigrationBal.Model.Json.Statement> FindStatmentByDate(DateTime initialDate, DateTime endDate);
        List<Model.Json.Statement> GetDuplicatedStatements();
    }
}
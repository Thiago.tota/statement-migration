﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO;
using AplMigrationBal.Model.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace AplMigrationBal.Comunication.Internal
{
    internal abstract class BaseComunication : IComunication
    {
        public virtual void SaveJsonFile(List<Model.Json.Statement> statements)
        {
            if (Global.JsonOutputSave)
            {
                statements.ForEach(f => File.WriteFile(Global.GetJsonOutputName(f.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ?
                    f.Cliente.Cnpj.ToString() : f.Cliente.CodigoGrupoEconomico.ToString()), Json.Serialize(f, Helper.Enum.JsonFormatting.Indented)));
            }
        }

        public virtual bool SaveStatement(List<Model.Json.Statement> statements)
        {
            bool result = false;
            try
            {
                SaveJsonFile(statements.ToList());
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        public abstract List<Model.Json.Statement> GetStatement(List<StatementKey> statementKeys, bool onlyKeyFields = false);
        public abstract bool DeleteStatement(List<StatementKey> statementKeys);
        public abstract bool LoadStatementsToDataBase(IEnumerable<IStatementDataFile> statements);
        public abstract bool LoadExplanatoryNotesToDataBase(IEnumerable<IExplanatoryNoteDataFile> explanatoryNotes);
        public abstract List<StatementKey> GetStatementKeys();
        public abstract bool DeleteStatementToProcess(List<StatementKey> statementKeys);
        public abstract bool CleanStatementToProcess();
        public abstract StatementToProcess GetStatementToProcess(List<StatementKey> statementKeys);
        public abstract List<Model.Json.Statement> GetLatestStatementByClient();
        public abstract IEnumerable<T> Find<T>(Expression<Func<T, bool>> filter, string collection) where T : class;
        public abstract bool UpdateOne<T>(Expression<Func<T, bool>> filter, List<KeyValuePair<Expression<Func<T, object>>, object>> update, string collection, bool async = false) where T : class;
        public abstract bool SaveOne<T>(T data, string collection, bool async = true) where T : class;
        public abstract bool SaveMany<T>(List<T> data, string collection, bool async = false) where T : class;
        public abstract bool DeleteMany<T>(Expression<Func<T, bool>> filter, string collection, bool async = false) where T : class;
        public abstract bool DeleteOne<T>(Expression<Func<T, bool>> filter, string collection, bool async = false) where T : class;
        public abstract IEnumerable<AplMigrationBal.Model.Json.Statement> FindStatmentByDate(DateTime inicialDate, DateTime endDate);
        public abstract List<Model.Json.Statement> GetDuplicatedStatements();
    }
}

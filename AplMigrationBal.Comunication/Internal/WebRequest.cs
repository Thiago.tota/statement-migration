﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO;
using AplMigrationBal.Model.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;

namespace AplMigrationBal.Comunication.Internal
{
    internal class WebRequest : BaseComunication
    {
        #region Private Methods

        /// <summary>
        /// Realiza comunicação via HTTP
        /// </summary>
        /// <param name="endereco">URL de comunicação</param>
        /// <returns>Classe de Saída serializada</returns>
        public TSaida ComunicaHttp<TSaida>(string endereco)
        {
            return ComunicaHttp<TSaida>(endereco, null);
        }

        /// <summary>
        /// Realiza comunicação via HTTP
        /// </summary>
        /// <param name="endereco">URL de comunicação</param>
        /// <returns>Classe de Saída serializada</returns>
        public TSaida ComunicaHttp<TSaida>(string endereco, IEnumerable<Tuple<string, string>> cabecalhosHttp)
        {
            return ComunicaHttp<TSaida>(endereco, cabecalhosHttp, "GET", null);
        }

        /// <summary>
        /// Realiza comunicação via HTTP
        /// </summary>
        /// <param name="endereco">URL de comunicação</param>
        /// <param name="cabecalhosHttp"></param>
        /// <param name="metodo">Método da Requisicao (POST/GET/PUT/DELETE)</param>
        /// <param name="dadosEntrada">Classe de entrada que será serializada para JSON</param>
        /// <returns>Classe de Saída serializada</returns>
        public TSaida ComunicaHttp<TSaida>(string endereco, IEnumerable<Tuple<string, string>> cabecalhosHttp, string metodo, object dadosEntrada)
        {
            try
            {
                var http = (HttpWebRequest)System.Net.WebRequest.Create(new Uri(endereco));
                string content;

                http.Accept = "application/json";
                http.ContentType = "application/json";
                http.Method = metodo;

                (cabecalhosHttp ?? new List<Tuple<string, string>>()).ToList().ForEach(header => http.Headers.Add(header.Item1, header.Item2));

                if (!http.Method.Equals("GET"))
                {
                    var parsedContent = Helper.Generic.Json.Serialize(dadosEntrada, Helper.Enum.JsonFormatting.Indented);
                    var encoding = new UTF8Encoding();//ASCIIEncoding();
                    var bytes = encoding.GetBytes(parsedContent);

                    var newStream = http.GetRequestStream();
                    newStream.Write(bytes, 0, bytes.Length);
                    newStream.Close();
                }

                using (var response = http.GetResponse())
                {
                    var stream = response.GetResponseStream();
                    var sr = new StreamReader(stream);
                    content = sr.ReadToEnd();
                }

                if (typeof(TSaida).Equals(content.GetType()))
                    return (TSaida)Convert.ChangeType(content, typeof(TSaida));
                else
                {
                    var retorno = Helper.Generic.Json.Deserialize<TSaida>(content);
                    return retorno;
                }
            }
            catch (WebException ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        public override List<Model.Json.Statement> GetStatement(List<StatementKey> statementKeys, bool onlyKeyFields = false)
        {
            return new List<Model.Json.Statement>();
        }

        public override bool DeleteStatement(List<Model.DTO.StatementKey> statementKeys)
        {
            return false;
        }

        public override bool SaveStatement(List<Model.Json.Statement> statements)
        {
            bool result = false;
            try
            {
                base.SaveStatement(statements);

                var header = new List<Tuple<string, string>>
                {
                    Tuple.Create("amc-message-id", Guid.NewGuid().ToString()),
                    Tuple.Create("amc-aplicacao", "BAL"),
                    Tuple.Create("amc-session-id", Guid.NewGuid().ToString()),
                    Tuple.Create("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4")
                };

                result = ComunicaHttp<string>(Global.ApiUrlBal, header, "POST", statements).Count() > 0;
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }

            return result;
        }

        public override bool LoadStatementsToDataBase(IEnumerable<IStatementDataFile> statements)
        {
            throw new NotImplementedException();
        }

        public override bool LoadExplanatoryNotesToDataBase(IEnumerable<IExplanatoryNoteDataFile> explanatoryNotes)
        {
            throw new NotImplementedException();
        }

        public override List<StatementKey> GetStatementKeys()
        {
            throw new NotImplementedException();
        }

        public override bool DeleteStatementToProcess(List<StatementKey> statementKeys)
        {
            throw new NotImplementedException();
        }

        public override StatementToProcess GetStatementToProcess(List<StatementKey> statementKeys)
        {
            throw new NotImplementedException();
        }

        public override bool CleanStatementToProcess()
        {
            throw new NotImplementedException();
        }

        public override bool SaveOne<T>(T data, string collection, bool async = true)
        {
            throw new NotImplementedException();
        }

        public override bool SaveMany<T>(List<T> data, string collection, bool async = true)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<T> Find<T>(Expression<Func<T, bool>> filter, string collection)
        {
            throw new NotImplementedException();
        }

        public override bool UpdateOne<T>(Expression<Func<T, bool>> filter, List<KeyValuePair<Expression<Func<T, object>>, object>> update, string collection, bool async = true)
        {
            throw new NotImplementedException();
        }

        public override List<Model.Json.Statement> GetLatestStatementByClient()
        {
            throw new NotImplementedException();
        }

        public override bool DeleteMany<T>(Expression<Func<T, bool>> filter, string collection, bool async = false)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<AplMigrationBal.Model.Json.Statement> FindStatmentByDate(DateTime inicialDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public override List<Model.Json.Statement> GetDuplicatedStatements()
        {
            throw new NotImplementedException();
        }

        public override bool DeleteOne<T>(Expression<Func<T, bool>> filter, string collection, bool async = false)
        {
            throw new NotImplementedException();
        }
    }
}

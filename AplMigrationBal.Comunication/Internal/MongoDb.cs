﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO;
using AplMigrationBal.Model.Interface;
using AplMigrationBal.Persistence.MongoDb;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace AplMigrationBal.Comunication.Internal
{
    internal class MongoDb : BaseComunication
    {
        private readonly string _serverUrl = Global.MongoDbUrlBal;
        private readonly string _databaseName = Global.MongoDbDatabaseNameBal;
        private readonly string _collectionName = Global.MongoCollectionNameBal;
        private const int _maxRegistersToReturn = 5000;

        private MongoDbSchema<Model.Json.Statement> mongoDbSchemaStatement = null;
        private MongoDbSchema<Model.DTO.StatementDataFile> mongoDbSchemaStatementToProcess = null;
        private MongoDbSchema<Model.DTO.ExplanatoryNoteDataFile> mongoDbSchemaExplanatoryNoteToProcess = null;

        private ProjectionDefinition<Model.Json.Statement> statmentProjectionExclude = Builders<Model.Json.Statement>.Projection.Exclude("index").Exclude("fileInfo").Exclude("dados.codigoUpload").Exclude("dados.cotacao")
                                                                    .Exclude("dados.imagemBalanco").Exclude("dados.contas.infoMercado").Exclude("dados.contas.auditoria").Exclude("dados.contas.highlights.notaVinculadaValor")
                                                                    .Exclude("dados.contas.highlights.notaVinculadaConta").Exclude("dados.contas.fluxoCaixa.notaVinculadaValor")
                                                                    .Exclude("dados.contas.fluxoCaixa.notaVinculadaConta").Exclude("cliente.analista").Exclude("cliente.valoresEm")
                                                                    .Exclude("dados.somatorio").Exclude("__v").Exclude("index");

        private ProjectionDefinition<Model.Json.Statement> statmentProjectionExcludeOnlyKeys = Builders<Model.Json.Statement>.Projection.Exclude("index").Exclude("fileInfo").Exclude("dados.codigoUpload").Exclude("dados.cotacao")
                                                                    .Exclude("dados.imagemBalanco").Exclude("dados.contas.infoMercado").Exclude("dados.contas.auditoria").Exclude("dados.contas.highlights.notaVinculadaValor")
                                                                    .Exclude("dados.contas.highlights.notaVinculadaConta").Exclude("dados.contas.fluxoCaixa.notaVinculadaValor")
                                                                    .Exclude("dados.contas.fluxoCaixa.notaVinculadaConta").Exclude("cliente.analista").Exclude("cliente.valoresEm")
                                                                    .Exclude("dados.somatorio").Exclude("__v").Exclude("index").Exclude("dados.contas");

        public MongoDb()
        {
            var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
            ConventionRegistry.Register("camelCase", conventionPack, t => true);
        }

        public override List<Model.Json.Statement> GetStatement(List<Model.DTO.StatementKey> statementKeys, bool onlyKeyFields = false)
        {
            List<Model.Json.Statement> result = new List<Model.Json.Statement>();

            if (mongoDbSchemaStatement == null) mongoDbSchemaStatement = new MongoDbSchema<Model.Json.Statement>(_serverUrl, _databaseName, _collectionName);

            if (mongoDbSchemaStatement.Documents.Indexes.List().ToList().Count < 8)
            {
                IEnumerable<CreateIndexModel<Model.Json.Statement>> indexModel = new List<CreateIndexModel<Model.Json.Statement>>()
                {
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("cliente.cnpj")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("cliente.codigoGrupoEconomico")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("cliente.tipoCliente")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("origem.fonte")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("dados.referencia")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("dados.tipo")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("dados.status"))
                };
                mongoDbSchemaStatement.Documents.Indexes.CreateManyAsync(indexModel);
            }

            var empresa = statementKeys.Where(f => f.ClientType.Equals(Helper.Enum.ClienteType.E)).Select(s => new { s.ClientId, Reference = new DateTime(s.Year, s.Month, 1, 0, 0, 0, DateTimeKind.Utc) });
            var grupo = statementKeys.Where(f => f.ClientType.Equals(Helper.Enum.ClienteType.G)).Select(s => new { s.ClientId, Reference = new DateTime(s.Year, s.Month, 1, 0, 0, 0, DateTimeKind.Utc) });


            var builder = Builders<Model.Json.Statement>.Filter;
            var filter = builder.And(builder.Eq("origem.conta", "B"), builder.Eq("cliente.tipoCliente", Helper.Enum.ClienteType.E.ToString()),
                            builder.In("cliente.cnpj", empresa.Select(s => s.ClientId).Distinct().ToArray()),
                            builder.In("dados.referencia", empresa.Select(s => s.Reference).Distinct().ToArray())
                            );

            if (onlyKeyFields)
            {
                mongoDbSchemaStatement.Documents.Find(filter).Project(statmentProjectionExcludeOnlyKeys).ToList().ForEach(f =>
                        result.Add(BsonSerializer.Deserialize<Model.Json.Statement>(f)));
            }
            else
            {
                mongoDbSchemaStatement.Documents.Find(filter).Project(statmentProjectionExclude).ToList().ForEach(f =>
                        result.Add(BsonSerializer.Deserialize<Model.Json.Statement>(f)));
            }

            builder = Builders<Model.Json.Statement>.Filter;
            filter = builder.And(builder.Eq("origem.conta", "B"), builder.Eq("cliente.tipoCliente", Helper.Enum.ClienteType.G.ToString()),
                            builder.In("cliente.codigoGrupoEconomico", grupo.Select(s => s.ClientId).Distinct().ToArray()),
                            builder.In("dados.referencia", grupo.Select(s => s.Reference).Distinct().ToArray())
                          );

            if (onlyKeyFields)
            {
                mongoDbSchemaStatement.Documents.Find(filter).Project(statmentProjectionExcludeOnlyKeys).ToList().ForEach(f =>
                        result.Add(BsonSerializer.Deserialize<Model.Json.Statement>(f)));
            }
            else
            {
                mongoDbSchemaStatement.Documents.Find(filter).Project(statmentProjectionExclude).ToList().ForEach(f =>
                        result.Add(BsonSerializer.Deserialize<Model.Json.Statement>(f)));
            }


            result.RemoveAll(r => r.Cliente.TipoCliente == Helper.Enum.ClienteType.E
                                        && !statementKeys.Exists(s => s.Year == r.Dados.Referencia.Year && s.Month == r.Dados.Referencia.Month
                                         && s.ClientId == r.Cliente.Cnpj && s.ClientType == Helper.Enum.ClienteType.E));

            result.RemoveAll(r => r.Cliente.TipoCliente == Helper.Enum.ClienteType.G
                                        && !statementKeys.Exists(s => s.Year == r.Dados.Referencia.Year && s.Month == r.Dados.Referencia.Month
                                        && s.ClientId == r.Cliente.CodigoGrupoEconomico && s.ClientType == Helper.Enum.ClienteType.G));

            return result;
        }

        public override List<StatementKey> GetStatementKeys()
        {
            List<StatementKey> result = new List<StatementKey>();

            if (mongoDbSchemaStatementToProcess == null) mongoDbSchemaStatementToProcess = new MongoDbSchema<Model.DTO.StatementDataFile>(_serverUrl, _databaseName, Global.CollectionTempStatementToProcess);

            var group = new BsonDocument
            {
                {
                    "$group", new BsonDocument
                    {
                        { "_id", new BsonDocument
                            {
                                { "clientId", "$clientId" },
                                { "clientType", "$clientType" },
                                { "month", "$month" },
                                { "year", "$year" },
                                { "source", "$source" },
                                { "type", "$type"},
                                { "status", "$status"},
                            }
                        }
                    }
                }
            };

            var limit = new BsonDocument
            {
                {
                    "$limit", Global.BufferStatementToProcess
                }
            };

            var pipeline = new[] { group, limit };
            var aggregated = mongoDbSchemaStatementToProcess.Documents.Aggregate<dynamic>(pipeline).ToList();

            foreach (var item in aggregated)
            {
                result.Add(new StatementKey
                {
                    ClientId = (double)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["clientId"].AsDouble,
                    ClientType = (Helper.Enum.ClienteType)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["clientType"].AsInt32,
                    Month = (byte)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["month"].AsInt32,
                    Year = ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["year"].AsInt32,
                    Source = (Helper.Enum.StatementSource)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["source"].AsInt32,
                    Type = (Helper.Enum.StatementType)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["type"].AsInt32,
                    Status = (Helper.Enum.StatementStatus)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["status"].AsInt32
                });
            }

            return result;
        }

        public override Model.DTO.StatementToProcess GetStatementToProcess(List<Model.DTO.StatementKey> statementKeys)
        {
            Model.DTO.StatementToProcess result = new Model.DTO.StatementToProcess();

            if (mongoDbSchemaStatementToProcess == null) mongoDbSchemaStatementToProcess =
                        new MongoDbSchema<Model.DTO.StatementDataFile>(_serverUrl, _databaseName, Global.CollectionTempStatementToProcess);
            if (mongoDbSchemaExplanatoryNoteToProcess == null) mongoDbSchemaExplanatoryNoteToProcess =
                        new MongoDbSchema<Model.DTO.ExplanatoryNoteDataFile>(_serverUrl, _databaseName, Global.CollectionTempExplanatoryNoteToProcess);

            statementKeys.ForEach(s => result.Statements.AddRange(mongoDbSchemaStatementToProcess.Documents.Find(f => f.Month.Equals(s.Month) && f.Year.Equals(s.Year)
                                                                                 && f.ClientId.Equals(s.ClientId)
                                                                                 && f.ClientType.Equals(s.ClientType)).ToList()));

            statementKeys.ForEach(s => result.ExplanatoryNotes.AddRange(mongoDbSchemaExplanatoryNoteToProcess.Documents.Find(f => f.Month.Equals(s.Month) && f.Year.Equals(s.Year)
                                                                                 && f.ClientId.Equals(s.ClientId)
                                                                                 && f.ClientType.Equals(s.ClientType)).ToList()));

            var duplicatedRecords = result.Statements.GroupBy(g => new { g.ClientId, g.Year, g.Month, g.Status, g.Source, g.Type, g.AccountId }).Where(w => w.Count() > 1)
                                                .GroupBy(g => new { g.Key.ClientId, g.Key.Year, g.Key.Month, g.Key.Status, g.Key.Source, g.Key.Type }).Where(w => w.Count() > 1);

            foreach (var item in duplicatedRecords)
            {
                var updateDates = result.Statements.Where(g => g.ClientId == item.Key.ClientId && g.Year == item.Key.Year && g.Month == item.Key.Month
                                                    && g.Status == item.Key.Status && g.Source == item.Key.Source && g.Type == item.Key.Type)
                                                    .Select(s => s.UpdatedDate).Distinct().OrderByDescending(o => o).Skip(1);

                foreach (var itemToRemove in updateDates)
                {
                    result.Statements.RemoveAll(r => r.ClientId == item.Key.ClientId && r.Year == item.Key.Year && r.Month == item.Key.Month
                                                    && r.Status == item.Key.Status && r.Source == item.Key.Source && r.Type == item.Key.Type
                                                    && r.UpdatedDate == itemToRemove);
                }

            }

            return result;
        }

        public override List<Model.Json.Statement> GetLatestStatementByClient()
        {
            List<Model.Json.Statement> result = new List<Model.Json.Statement>();

            if (mongoDbSchemaStatementToProcess == null) mongoDbSchemaStatementToProcess = new MongoDbSchema<Model.DTO.StatementDataFile>(_serverUrl, _databaseName, _collectionName);

            #region Aggregations

            #region Match
            BsonArray bsonArray = new BsonArray();
            bsonArray.Add(Helper.Enum.StatementSource.upload.ToString());
            bsonArray.Add(Helper.Enum.StatementSource.Upload.ToString());

            BsonArray typeOfBalance = new BsonArray();
            typeOfBalance.Add("B");

            var match = new BsonDocument
            {
                {
                    "$match", new BsonDocument
                    {
                        {
                            "origem.fonte", new BsonDocument
                            {
                                {
                                    "$in", bsonArray
                                }
                            }
                        },
                        {
                             "origem.conta", new BsonDocument
                            {
                                {
                                    "$in", typeOfBalance
                                }
                            }
                        }
                    }
                }
            };
            #endregion

            #region Project
            var project = new BsonDocument
            {
                {
                    "$project", new BsonDocument
                    {
                        {"_id", 0 },
                        {"cliente.cnpj", 1 },
                        {"cliente.codigoGrupoEconomico", 1 },
                        {"cliente.tipoCliente", 1 },
                        {"dados.referencia", 1 },
                        {"dados.tipo", 1 },
                        {"dados.dataAtualizacao", 1 }
                    }
                }
            };
            #endregion

            #region Sort
            var sort = new BsonDocument
            {
                {
                    "$sort", new BsonDocument ("dados.referencia", -1)
                }
            };
            #endregion

            #region Group
            var group = new BsonDocument
            {
                {
                    "$group", new BsonDocument
                    {
                        { "_id", new BsonDocument
                            {
                                { "cnpj", "$cliente.cnpj" },
                                { "codigoGrupoEconomico", "$cliente.codigoGrupoEconomico" },
                                { "tipoCliente", "$cliente.tipoCliente" },
                            }
                        },
                        new BsonDocument
                        {
                            {
                                "Reference",  new BsonDocument
                                {
                                    { "$first", "$dados.referencia" }
                                }
                            }
                        },
                        new BsonDocument
                        {
                            {
                                "Type",  new BsonDocument
                                {
                                    { "$first", "$dados.tipo" }
                                }
                            }
                        },
                        new BsonDocument
                        {
                            {
                                "Created",  new BsonDocument
                                {
                                    { "$first", "$dados.dataAtualizacao" }
                                }
                            }
                        },
                    }
                }
            };
            #endregion

            #endregion

            var pipeline = new[] { match, project, sort, group };
            var aggregated = mongoDbSchemaStatementToProcess.Documents.Aggregate<dynamic>(pipeline).ToList();

            foreach (var item in aggregated)
            {
                Model.Json.Statement statement = new Model.Json.Statement();
                try
                {
                    var tipoCliente = ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["tipoCliente"].AsString;
                    var cnpj = ((IEnumerable<KeyValuePair<string, object>>)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value).ToList()[0].Value;
                    var codigoGrupoEconomico = ((IEnumerable<KeyValuePair<string, object>>)((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value).ToList()[1].Value;
                    var referencia = ((IEnumerable<KeyValuePair<string, object>>)item).ToList()[1].Value;
                    var tipo = ((IEnumerable<KeyValuePair<string, object>>)item).ToList()[2].Value;
                    var dataAtualizacao = ((IEnumerable<KeyValuePair<string, object>>)item).ToList()[3].Value;


                    statement.Cliente.TipoCliente = (Helper.Enum.ClienteType)Enum.Parse(typeof(Helper.Enum.ClienteType), tipoCliente, true);
                    if (double.TryParse(cnpj.ToString(), out double auxUlong))
                        statement.Cliente.Cnpj = auxUlong;

                    if (double.TryParse(codigoGrupoEconomico.ToString(), out auxUlong))
                        statement.Cliente.CodigoGrupoEconomico = auxUlong;

                    statement.Dados.Referencia = Convert.ToDateTime(referencia);
                    statement.Dados.Tipo = (Helper.Enum.StatementType)Enum.Parse(typeof(Helper.Enum.StatementType), tipo.ToString().Replace('ç', 'c').Replace('Ç', 'C'), true);
                    statement.Dados.DataAtualizacao = Convert.ToDateTime(dataAtualizacao);

                    result.Add(statement);
                }
                catch (Exception ex)
                {
                    Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                }
            }

            return result;
        }

        public override bool DeleteStatement(List<Model.DTO.StatementKey> statementKeys)
        {
            if (mongoDbSchemaStatement == null) mongoDbSchemaStatement = new MongoDbSchema<Model.Json.Statement>(_serverUrl, _databaseName, _collectionName);

            statementKeys.ForEach(s => mongoDbSchemaStatement.Documents.DeleteMany(f => s.ClientType.Equals(Helper.Enum.ClienteType.E)
                                                         && f.Dados.Referencia.Equals(new DateTime(s.Year, s.Month, 1, 0, 0, 0, DateTimeKind.Utc))
                                                         && f.Cliente.Cnpj.Equals(s.ClientId)
                                                         && f.Cliente.TipoCliente.Equals(s.ClientType)));

            statementKeys.ForEach(s => mongoDbSchemaStatement.Documents.DeleteMany(f => s.ClientType.Equals(Helper.Enum.ClienteType.G)
                                                         && f.Dados.Referencia.Equals(new DateTime(s.Year, s.Month, 1, 0, 0, 0, DateTimeKind.Utc))
                                                         && f.Cliente.CodigoGrupoEconomico.Equals(s.ClientId)
                                                         && f.Cliente.TipoCliente.Equals(s.ClientType)));
            return true;
        }

        public override bool DeleteStatementToProcess(List<Model.DTO.StatementKey> statementKeys)
        {
            if (mongoDbSchemaStatementToProcess == null) mongoDbSchemaStatementToProcess = new MongoDbSchema<Model.DTO.StatementDataFile>(_serverUrl, _databaseName, Global.CollectionTempStatementToProcess);
            if (mongoDbSchemaExplanatoryNoteToProcess == null) mongoDbSchemaExplanatoryNoteToProcess = new MongoDbSchema<Model.DTO.ExplanatoryNoteDataFile>(_serverUrl, _databaseName, Global.CollectionTempExplanatoryNoteToProcess);

            statementKeys.ForEach(s => mongoDbSchemaStatementToProcess.Documents.DeleteMany(f => f.Month.Equals(s.Month) && f.Year.Equals(s.Year)
                                                                                 && f.ClientId.Equals(s.ClientId)
                                                                                 && f.ClientType.Equals(s.ClientType)));

            statementKeys.ForEach(s => mongoDbSchemaExplanatoryNoteToProcess.Documents.DeleteMany(f => f.Month.Equals(s.Month) && f.Year.Equals(s.Year)
                                                                                && f.ClientId.Equals(s.ClientId)
                                                                                && f.ClientType.Equals(s.ClientType)));
            return true;
        }

        public override bool CleanStatementToProcess()
        {
            if (mongoDbSchemaStatementToProcess == null) mongoDbSchemaStatementToProcess = new MongoDbSchema<Model.DTO.StatementDataFile>(_serverUrl, _databaseName, Global.CollectionTempStatementToProcess);
            if (mongoDbSchemaExplanatoryNoteToProcess == null) mongoDbSchemaExplanatoryNoteToProcess = new MongoDbSchema<Model.DTO.ExplanatoryNoteDataFile>(_serverUrl, _databaseName, Global.CollectionTempExplanatoryNoteToProcess);

            mongoDbSchemaStatementToProcess.Documents.DeleteMany(new BsonDocument());
            mongoDbSchemaExplanatoryNoteToProcess.Documents.DeleteMany(new BsonDocument());

            return true;
        }

        public override bool SaveStatement(List<Model.Json.Statement> statements)
        {
            if (mongoDbSchemaStatement == null) mongoDbSchemaStatement = new MongoDbSchema<Model.Json.Statement>(_serverUrl, _databaseName, _collectionName);

            base.SaveStatement(statements);

            List<Model.DTO.StatementKey> statementKeys = new List<Model.DTO.StatementKey>();

            statements.ForEach(f => statementKeys.Add(new Model.DTO.StatementKey
            {
                ClientId = Convert.ToDouble(f.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? f.Cliente.Cnpj : f.Cliente.CodigoGrupoEconomico),
                ClientType = f.Cliente.TipoCliente,
                Month = (byte)f.Dados.Referencia.Month,
                Year = f.Dados.Referencia.Year,
                Status = f.Dados.Status,
                Type = f.Dados.Tipo,
                Source = f.Origem.Fonte
            }));

            DeleteStatement(statementKeys);

            mongoDbSchemaStatement.Documents.InsertMany(statements);

            return true;
        }

        public override bool LoadStatementsToDataBase(IEnumerable<IStatementDataFile> statements)
        {
            var mongoDbSchema = new MongoDbSchema<IStatementDataFile>(_serverUrl, _databaseName, Global.CollectionTempStatementToProcess);

            mongoDbSchema.Documents.InsertMany(statements);

            if (mongoDbSchema.Documents.Indexes.List().ToList().Count < 5)
            {
                IEnumerable<CreateIndexModel<IStatementDataFile>> indexModel = new List<CreateIndexModel<IStatementDataFile>>()
                {
                    new CreateIndexModel<IStatementDataFile>(new IndexKeysDefinitionBuilder<IStatementDataFile>().Ascending("clientId")),
                    new CreateIndexModel<IStatementDataFile>(new IndexKeysDefinitionBuilder<IStatementDataFile>().Ascending("clientType")),
                    new CreateIndexModel<IStatementDataFile>(new IndexKeysDefinitionBuilder<IStatementDataFile>().Ascending("month")),
                    new CreateIndexModel<IStatementDataFile>(new IndexKeysDefinitionBuilder<IStatementDataFile>().Ascending("year"))
                };
                mongoDbSchema.Documents.Indexes.CreateManyAsync(indexModel);
                //mongoDbSchema.Documents.Indexes.CreateOne(new CreateIndexModel<IStatementDataFile>("{ ClientId : 1, ClientType : 1, Month : 1, Year : 1 }"));
            }

            return true;
        }

        public override bool LoadExplanatoryNotesToDataBase(IEnumerable<IExplanatoryNoteDataFile> explanatoryNotes)
        {
            var mongoDbSchema = new MongoDbSchema<IExplanatoryNoteDataFile>(_serverUrl, _databaseName, Global.CollectionTempExplanatoryNoteToProcess);
            mongoDbSchema.Documents.InsertMany(explanatoryNotes);

            if (mongoDbSchema.Documents.Indexes.List().ToList().Count < 5)
            {
                IEnumerable<CreateIndexModel<IExplanatoryNoteDataFile>> indexModel = new List<CreateIndexModel<IExplanatoryNoteDataFile>>()
                {
                    new CreateIndexModel<IExplanatoryNoteDataFile>(new IndexKeysDefinitionBuilder<IExplanatoryNoteDataFile>().Ascending("clientId")),
                    new CreateIndexModel<IExplanatoryNoteDataFile>(new IndexKeysDefinitionBuilder<IExplanatoryNoteDataFile>().Ascending("clientType")),
                    new CreateIndexModel<IExplanatoryNoteDataFile>(new IndexKeysDefinitionBuilder<IExplanatoryNoteDataFile>().Ascending("conth")),
                    new CreateIndexModel<IExplanatoryNoteDataFile>(new IndexKeysDefinitionBuilder<IExplanatoryNoteDataFile>().Ascending("cear"))
                };
                mongoDbSchema.Documents.Indexes.CreateManyAsync(indexModel);
            }

            return true;
        }

        public override IEnumerable<T> Find<T>(Expression<Func<T, bool>> filter, string collection)
        {
            var mongoDbSchema = new MongoDbSchema<T>(_serverUrl, _databaseName, collection);
            return mongoDbSchema.Documents.Find(filter).ToList();
        }

        public override IEnumerable<AplMigrationBal.Model.Json.Statement> FindStatmentByDate(DateTime initialDate, DateTime finalDate)
        {

            List<Model.Json.Statement> result = new List<Model.Json.Statement>();

            if (mongoDbSchemaStatement == null) mongoDbSchemaStatement = new MongoDbSchema<Model.Json.Statement>(_serverUrl, _databaseName, _collectionName);

            if (mongoDbSchemaStatement.Documents.Indexes.List().ToList().Count < 8)
            {
                IEnumerable<CreateIndexModel<Model.Json.Statement>> indexModel = new List<CreateIndexModel<Model.Json.Statement>>()
                {
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("cliente.cnpj")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("cliente.codigoGrupoEconomico")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("cliente.tipoCliente")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("origem.fonte")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("dados.referencia")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("dados.tipo")),
                    new CreateIndexModel<Model.Json.Statement>(new IndexKeysDefinitionBuilder<Model.Json.Statement>().Ascending("dados.status"))
                };
                mongoDbSchemaStatement.Documents.Indexes.CreateManyAsync(indexModel);
            }

            var builder = Builders<Model.Json.Statement>.Filter;
            var filter = builder.And(
                                    builder.Eq("origem.conta", "B"),
                                    builder.Ne("dados.contas.ativo.conta", BsonNull.Value),
                                    builder.Ne("dados.contas.passivo.conta", BsonNull.Value),
                                    builder.Ne("dados.contas.dre.conta", BsonNull.Value),
                                    builder.Ne("dados.contas.mutacao.conta", BsonNull.Value),
                                    builder.Ne("dados.contas.fluxoCaixa.conta", BsonNull.Value),
                                    builder.Ne("dados.contas.indicadores.conta", BsonNull.Value),
                                    builder.Ne("dados.contas.highlights.conta", BsonNull.Value),
                                    builder.Gte("dados.referencia", initialDate.ToUniversalTime()),
                                    builder.Lt("dados.referencia", finalDate.AddDays(1).ToUniversalTime())
                                   );

            int currentPage = 0;

            List<BsonDocument> databaseAllData = new List<BsonDocument>();
            List<BsonDocument> databaseResults = new List<BsonDocument>();
            do
            {
                databaseResults = mongoDbSchemaStatement.Documents.Find(filter).Limit(_maxRegistersToReturn).Skip(currentPage * _maxRegistersToReturn).Project(statmentProjectionExclude).ToList();
                databaseAllData.AddRange(databaseResults);
                currentPage++;
            }
            while (databaseResults.Any());

            databaseAllData.ForEach(d =>
            {
                result.Add(BsonSerializer.Deserialize<Model.Json.Statement>(d));
            });


            result.ForEach(f => f.Dados.Referencia = f.Dados.Referencia.AddHours(24 - f.Dados.Referencia.Hour));

            return result;
        }

        public override bool UpdateOne<T>(Expression<Func<T, bool>> filter, List<KeyValuePair<Expression<Func<T, object>>, object>> update, string collection, bool async = true)
        {
            var mongoDbSchema = new MongoDbSchema<T>(_serverUrl, _databaseName, collection);

            foreach (var item in update)
            {
                if (async)
                    mongoDbSchema.Documents.FindOneAndUpdateAsync(filter, Builders<T>.Update.Set(item.Key, item.Value));
                else
                    mongoDbSchema.Documents.FindOneAndUpdate(filter, Builders<T>.Update.Set(item.Key, item.Value));
            }

            return true;
        }

        public override bool SaveOne<T>(T data, string collection, bool async = false)
        {
            var mongoDbSchema = new MongoDbSchema<T>(_serverUrl, _databaseName, collection);

            if (async)
                mongoDbSchema.Documents.InsertOneAsync(data);
            else
                mongoDbSchema.Documents.InsertOne(data);

            return true;
        }

        public override bool SaveMany<T>(List<T> data, string collection, bool async = false)
        {
            var mongoDbSchema = new MongoDbSchema<T>(_serverUrl, _databaseName, collection);

            if (async)
                mongoDbSchema.Documents.InsertManyAsync(data);
            else
                mongoDbSchema.Documents.InsertMany(data);

            return true;
        }

        public override bool DeleteMany<T>(Expression<Func<T, bool>> filter, string collection, bool async = false)
        {
            var mongoDbSchema = new MongoDbSchema<T>(_serverUrl, _databaseName, collection);

            if (async)
                mongoDbSchema.Documents.DeleteManyAsync(filter);
            else
                mongoDbSchema.Documents.DeleteMany(filter);

            return true;
        }

        public override bool DeleteOne<T>(Expression<Func<T, bool>> filter, string collection, bool async = false)
        {
            var mongoDbSchema = new MongoDbSchema<T>(_serverUrl, _databaseName, collection);

            if (async)
                mongoDbSchema.Documents.DeleteOneAsync(filter);
            else
                mongoDbSchema.Documents.DeleteOne(filter);

            return true;
        }

        public override List<Model.Json.Statement> GetDuplicatedStatements()
        {
            List<Model.Json.Statement> result = new List<Model.Json.Statement>();

            if (mongoDbSchemaStatement == null) mongoDbSchemaStatement = new MongoDbSchema<Model.Json.Statement>(_serverUrl, _databaseName, _collectionName);

            var group = new BsonDocument
            {
                {
                    "$group", new BsonDocument
                    {
                        { "_id", new BsonDocument
                            {
                                {"tipoCliente", "$cliente.tipoCliente"},
                                { "grupo", "$cliente.codigoGrupoEconomico" },
                                { "cnpj", "$cliente.cnpj" },
                                { "referencia", "$dados.referencia" },
                                { "tipoBalanco", "$dados.tipo" },
                                { "status", "$dados.status" },
                                { "conta", "$origem.conta" },
                                { "fonte", "$origem.fonte" }
                            }
                        },
                        new BsonDocument
                        {
                            {
                                "count",  new BsonDocument
                                {
                                    { "$sum", 1 }
                                }
                            }
                        }
                    }
                }
            };

            var match = new BsonDocument
            {
                {
                    "$match", new BsonDocument
                    {
                        { "count", new BsonDocument
                            {
                                { "$gt", 1 }
                            }
                        }
                    }
                }
            };

            var pipeline = new[] { group, match };

            var aggregated = mongoDbSchemaStatement.Documents.Aggregate<dynamic>(pipeline).ToList();

            foreach (var item in aggregated)
            {
                Model.Json.Statement statement = new Model.Json.Statement();
                statement.Cliente.TipoCliente = (Helper.Enum.ClienteType)Enum.Parse(typeof(Helper.Enum.ClienteType), ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["tipoCliente"].AsString, true);

                if (((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["cnpj"] != BsonNull.Value)
                    statement.Cliente.Cnpj = ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["cnpj"].ToDouble();

                if (((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["grupo"] != BsonNull.Value)
                    statement.Cliente.CodigoGrupoEconomico = ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["grupo"].ToDouble();

                statement.Dados.Referencia = ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["referencia"].ToUniversalTime();
                statement.Dados.Tipo = (Helper.Enum.StatementType)Enum.Parse(typeof(Helper.Enum.StatementType), ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["tipoBalanco"].AsString, true);
                statement.Dados.Status = (Helper.Enum.StatementStatus)Enum.Parse(typeof(Helper.Enum.StatementStatus), ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["status"].AsString, true);
                statement.Origem.Conta = ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["conta"].AsString;
                statement.Origem.Fonte = (Helper.Enum.StatementSource)Enum.Parse(typeof(Helper.Enum.StatementSource), ((IEnumerable<KeyValuePair<string, object>>)item).ToList().First().Value.ToBsonDocument()["fonte"].AsString, true);

                result.Add(statement);
            }

            return result;
        }
    }
}

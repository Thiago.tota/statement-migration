﻿using AplMigrationBal.Comunication.Internal;
using AplMigrationBal.Helper.Generic;

namespace AplMigrationBal.Comunication
{
    public static class Singleton
    {
        public static IComunication Instance()
        {
            IComunication instance = null;

            switch (Global.SaveStatementBy)
            {
                case Helper.Enum.ComunicationBy.MongoDb:
                    instance = new MongoDb();
                    break;
                case Helper.Enum.ComunicationBy.WebAPI:
                    instance = new WebRequest();
                    break;
            }

            return instance;
        }
    }
}

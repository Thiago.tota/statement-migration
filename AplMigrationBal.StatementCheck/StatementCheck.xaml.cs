﻿using AplMigrationBal.Helper.Generic;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace AplMigrationBal.StatementCheck
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<KeyValuePair<string, string>> StatementCheckList = new List<KeyValuePair<string, string>>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonStarCheck_Click(object sender, RoutedEventArgs e)
        {
            if (radioButtonHML.IsChecked == true)
                Global.SetEnvironment(Helper.Enum.Environment.Homologacao);
            else if (radioButtonPRD.IsChecked == true)
                Global.SetEnvironment(Helper.Enum.Environment.Producao);
            else
                Global.SetEnvironment(Helper.Enum.Environment.TI);

            int level;
            if (radioButtonLevel0.IsChecked == true)
                level = 0;
            else if (radioButtonLevel1.IsChecked == true)
                level = 1;
            else if (radioButtonLevel2.IsChecked == true)
                level = 2;
            else
                level = 3;

            var result = new Business.Statement().Check(StatementCheckList.Select(s => s.Value).ToList(), level);

            MessageBox.Show("Batimento finalizado");
        }

        private void ButtonSelectStatementCheckFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == true)
            {
                for (int i = 0; i < openFileDialog.FileNames.Length; i++)
                {
                    StatementCheckList.Add(new KeyValuePair<string, string>(openFileDialog.SafeFileNames[i], openFileDialog.FileNames[i]));
                }
                listBoxStatementCheckFile.ItemsSource = null;
                listBoxStatementCheckFile.ItemsSource = StatementCheckList;
            }
        }

        private void ButtonRemoveStatementCheckFile_Click(object sender, RoutedEventArgs e)
        {
            if (listBoxStatementCheckFile.SelectedItem != null)
            {
                StatementCheckList.RemoveAt(listBoxStatementCheckFile.SelectedIndex);
                listBoxStatementCheckFile.ItemsSource = null;
                listBoxStatementCheckFile.ItemsSource = StatementCheckList;
            }
        }
    }
}

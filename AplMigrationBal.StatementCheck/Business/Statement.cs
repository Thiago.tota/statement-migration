﻿using AplMigrationBal.Comunication;
using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using AplMigrationBal.Helper.Extensions;

namespace AplMigrationBal.StatementCheck.Business
{
    public class Statement
    {
        private KeyValuePair<Helper.Enum.StatementType, string> GetStatementTypeAndStatus(string statementInfo)
        {
            Helper.Enum.StatementType statementType;
            if (string.IsNullOrEmpty(statementInfo))
            {
                return new KeyValuePair<Helper.Enum.StatementType, string>();
            }
            else
            {
                switch (statementInfo.Substring(0, 3))
                {
                    case "BAL":
                        statementType = Helper.Enum.StatementType.Balanco;
                        break;
                    default:
                        statementType = Helper.Enum.StatementType.Balancete;
                        break;
                }

                return new KeyValuePair<Helper.Enum.StatementType, string>(statementType, statementInfo.Substring(3, 3).ToUpper().Equals("SER") ? "Serasa" : statementInfo.Substring(3, 3).ToUpper());
            }
        }

        #region Public Methods

        public bool Check(List<string> statementCheckList, int level = 0)
        {
            StringBuilder summary = new StringBuilder();
            IComunication comunication = Singleton.Instance();

            foreach (var statementFile in statementCheckList)
            {
                try
                {
                    List<Model.Json.Statement> statementsToCheck = ReadFile(statementFile);
                    List<Model.DTO.StatementKey> statementKeys = new List<Model.DTO.StatementKey>();
                    List<string> inconsistency = new List<string>();
                    List<Model.Json.Statement> clientsNotOk = new List<Model.Json.Statement>();

                    var accountPlan = new AplMigrationBal.Business.AccountPlan().GetAccountPlan(Global.CollectionAccountPlan).Accounts.Where(a => a.Level == level);

                    statementsToCheck.RemoveAll(f => f.Dados.Referencia.Year < 2013);

                    statementsToCheck.ForEach(s => statementKeys.Add(new Model.DTO.StatementKey
                    {
                        ClientType = s.Cliente.TipoCliente,
                        ClientId = Convert.ToDouble(s.Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E) ? s.Cliente.Cnpj : s.Cliente.CodigoGrupoEconomico),
                        Month = (byte)s.Dados.Referencia.Month,
                        Year = s.Dados.Referencia.Year
                    }));

                    List<Model.Json.Statement> statementsFromDatabase = comunication.GetStatement(statementKeys);

                    bool gravarAtivoPassivo = false;
                    foreach (var item in statementsToCheck)
                    {
                        try
                        {
                            var allStatements = statementsFromDatabase.Where(f => f.Cliente.Cnpj.Equals(item.Cliente.Cnpj) && f.Cliente.CodigoGrupoEconomico.Equals(item.Cliente.CodigoGrupoEconomico) &&
                                                f.Cliente.TipoCliente.Equals(item.Cliente.TipoCliente) && f.Dados.Referencia.Equals(item.Dados.Referencia) && f.Dados.Tipo.Equals(item.Dados.Tipo)).ToList();

                            Model.Json.Statement statement;
                            if (allStatements.Count > 1)
                            {
                                statement = allStatements.Where(f => f.Dados.Status == Helper.Enum.StatementStatus.Definitivo).FirstOrDefault();
                                statement = statement ?? allStatements.FirstOrDefault();
                            }
                            else
                                statement = allStatements.FirstOrDefault();

                            if (statement == null)
                            {
                                inconsistency.Add(string.Format(Global.StatementCheckNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                    item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status));
                                clientsNotOk.Add(item);
                                gravarAtivoPassivo = true;
                                continue;
                            }

                            #region Ativo
                            foreach (var validate in item.Dados.Contas.Ativo.Where(f => f.Valor != "0" && accountPlan.Where
                                                        (a => a.Group == Helper.Enum.AccountGroup.Ativo).Select(s => s.Account).Contains(f.Conta)))
                            {
                                var record = statement.Dados.Contas.Ativo.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Ativo, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double.TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double.TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Ativo, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region Passivo
                            foreach (var validate in item.Dados.Contas.Passivo.Where(f => f.Valor != "0" && accountPlan.Where
                                                        (a => a.Group == Helper.Enum.AccountGroup.Passivo).Select(s => s.Account).Contains(f.Conta)))
                            {

                                var record = statement.Dados.Contas.Passivo.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Passivo, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double .TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double.TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Passivo, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region DRE
                            foreach (var validate in item.Dados.Contas.Dre.Where(f => f.Valor != "0" && accountPlan.Where
                                                        (a => a.Group == Helper.Enum.AccountGroup.Dre).Select(s => s.Account).Contains(f.Conta)))
                            {

                                var record = statement.Dados.Contas.Dre.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Dre, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double .TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double.TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Dre, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region Mutação
                            foreach (var validate in item.Dados.Contas.Mutacao.Where(f => f.Valor != "0" && accountPlan.Where
                                                        (a => a.Group == Helper.Enum.AccountGroup.Mutacao).Select(s => s.Account).Contains(f.Conta)))
                            {

                                var record = statement.Dados.Contas.Mutacao.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Mutacao, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double .TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double.TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Mutacao, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region Fluxo de Caixa
                            foreach (var validate in item.Dados.Contas.FluxoCaixa.Where(f => f.Valor != "0" && accountPlan.Where
                                                        (a => a.Group == Helper.Enum.AccountGroup.FluxoCaixa).Select(s => s.Account).Contains(f.Conta)))
                            {

                                var record = statement.Dados.Contas.FluxoCaixa.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.FluxoCaixa, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double.TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double .TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.FluxoCaixa, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region Indicadores
                            foreach (var validate in item.Dados.Contas.Indicadores.Where(f => f.Valor != "0" && accountPlan.Where
                                                       (a => a.Group == Helper.Enum.AccountGroup.Indicadores).Select(s => s.Account).Contains(f.Conta)))
                            {

                                var record = statement.Dados.Contas.Indicadores.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Indicadores, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double .TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double.TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Indicadores, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region Highlights
                            foreach (var validate in item.Dados.Contas.Highlights.Where(f => f.Valor != "0" && accountPlan.Where
                                                       (a => a.Group == Helper.Enum.AccountGroup.Highlights).Select(s => s.Account).Contains(f.Conta)))
                            {

                                var record = statement.Dados.Contas.Highlights.Where(f => f.Conta.Equals(validate.Conta)).FirstOrDefault();
                                if (record == null)
                                {
                                    gravarAtivoPassivo = true;
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                        item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Highlights, validate.Conta, validate.Valor));
                                }
                                else
                                {
                                    double.TryParse(record.Valor.Replace(".", ""), out double valor1);
                                    double.TryParse(validate.Valor.Replace(".", ""), out double valor2);

                                    if (!valor1.Equals(valor2))
                                    {
                                        gravarAtivoPassivo = true;
                                        clientsNotOk.Add(item);
                                        inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Highlights, validate.Conta, valor2, valor1));
                                    }
                                }
                            }
                            #endregion

                            #region Compare Ativo x Passivo
                            var ativo = statement.Dados.Contas.Ativo.Where(f => f.Conta.Equals(14000)).FirstOrDefault();
                            var passivo = statement.Dados.Contas.Passivo.Where(f => f.Conta.Equals(26000)).FirstOrDefault();

                            if (ativo == null)
                            {
                                clientsNotOk.Add(item);
                                inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                    item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Ativo, "14000", "-"));
                            }

                            if (passivo == null)
                            {
                                clientsNotOk.Add(item);
                                inconsistency.Add(string.Format(Global.StatementCheckAccountNotFoundMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                    item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, Helper.Enum.AccountGroup.Passivo, "26000", "-"));
                            }

                            if (ativo != null && passivo != null)
                            {
                                double .TryParse(ativo.Valor.Replace(".", ""), out double valor1);
                                double.TryParse(passivo.Valor.Replace(".", ""), out double valor2);

                                if (!valor1.Equals(valor2))
                                {
                                    clientsNotOk.Add(item);
                                    inconsistency.Add(string.Format(Global.StatementCheckValueErrorMsg, item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, "Ativo x Passivo", "14000 x 26000", valor1, valor2));
                                }
                                else if (gravarAtivoPassivo)
                                {
                                    inconsistency.Add(string.Format("{0};{1};{2};{3};{4};{5};{6}Ativo x Passivo batido;{7}; Valor ativo;{8};Valor passivo;{9}", item.Cliente.TipoCliente, item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? item.Cliente.Cnpj : item.Cliente.CodigoGrupoEconomico,
                                            item.Dados.Referencia.ToShortDateString(), item.Dados.Tipo, item.Dados.Status, statement.Origem.Fonte, "Ativo x Passivo", "14000 x 26000", valor1, valor2));

                                    clientsNotOk.RemoveAll(f => f.Cliente.TipoCliente.Equals(item.Cliente.TipoCliente) && item.Cliente.TipoCliente == Helper.Enum.ClienteType.E ? f.Cliente.Cnpj.Equals(item.Cliente.Cnpj) : f.Cliente.CodigoGrupoEconomico.Equals(item.Cliente.CodigoGrupoEconomico)
                                                && f.Dados.Referencia.Equals(item.Dados.Referencia) && f.Dados.Tipo.Equals(item.Dados.Tipo));
                                }
                            }
                            #endregion

                            #region Nota Explicativa
                            //stringBuilder.AppendLine("NOTA EXPLICATIVA");
                            //stringBuilder.AppendLine("******************************************************************");
                            //foreach (var validate in item.Dados.Contas.NotaExplicativa)
                            //{
                            //    var record = statement.Dados.Contas.NotaExplicativa;
                            //    if (record == null)
                            //    {
                            //        stringBuilder.AppendLine(string.Format("Contra {0} não encontrada. Valor esperado: {1}", validate.Conta, validate.Valor));
                            //        occur++;
                            //    }
                            //    else
                            //    {
                            //        double .TryParse(record.Valor.Replace(".", ""), out double valor1);
                            //        double .TryParse(validate.Valor.Replace(".", ""), out double valor2);

                            //        if (!valor1.Equals(valor2))
                            //        {
                            //            stringBuilder.AppendLine(string.Format("Contra {0} divergente. Valor esperado: {1}; Valor econtrado: {2}", validate.Conta, validate.Valor, record.Valor));
                            //            occur++;
                            //        }
                            //    }
                            //}

                            //if (occur == 0)
                            //    stringBuilder.AppendLine("OK");

                            //occur = 0;
                            //stringBuilder.AppendLine("******************************************************************");
                            #endregion

                            gravarAtivoPassivo = false;
                        }
                        catch (Exception ex)
                        {
                            Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                            throw;
                        }
                    }

                    //Remove duplicated entries
                    inconsistency = inconsistency.Distinct().ToList();

                    #region Statistics
                    var clientsNok = clientsNotOk.Select(s => new { s.Cliente.TipoCliente, s.Cliente.Cnpj, s.Cliente.CodigoGrupoEconomico }).Distinct();
                    int qtdClientsNok = clientsNok.Count();

                    var clientsOk = statementsToCheck.Select(s => new { s.Cliente.TipoCliente, s.Cliente.Cnpj, s.Cliente.CodigoGrupoEconomico }).Distinct().Except(clientsNok);
                    int qtdClientsOk = clientsOk.Count();

                    var statementsNok = clientsNotOk.Select(s => new { s.Cliente.TipoCliente, s.Cliente.Cnpj, s.Cliente.CodigoGrupoEconomico, s.Dados.Referencia, s.Dados.Status, s.Dados.Tipo }).Distinct();
                    int qtdStatementsNok = statementsNok.Count();

                    var statementsOk = statementsToCheck.Select(s => new { s.Cliente.TipoCliente, s.Cliente.Cnpj, s.Cliente.CodigoGrupoEconomico, s.Dados.Referencia, s.Dados.Status, s.Dados.Tipo }).Distinct().Except(statementsNok);
                    int qtdStatementsOk = statementsOk.Count();

                    summary.AppendLine(string.Format("{0};{1};{2};{3};{4};{5};{6}", statementFile.Substring(0, statementFile.Length - 4).Substring(statementFile.LastIndexOf('\\') + 1),
                                                    qtdClientsOk + qtdClientsNok, qtdStatementsOk + qtdStatementsNok, qtdClientsOk, qtdStatementsOk, qtdClientsNok, qtdStatementsNok));

                    StringBuilder finalReport = new StringBuilder();
                    finalReport.AppendLine(string.Format("*******************************************************************"));
                    finalReport.AppendLine(string.Format("Total de clientes: {0}", qtdClientsOk + qtdClientsNok));
                    finalReport.AppendLine(string.Format("Total de clientes OK: {0}", qtdClientsOk));
                    finalReport.AppendLine(string.Format("Total de clientes não Ok: {0}", qtdClientsNok));
                    finalReport.AppendLine(string.Format("Total de balanços: {0}", qtdStatementsOk + qtdStatementsNok));
                    finalReport.AppendLine(string.Format("Total de balanços Ok: {0}", qtdStatementsOk));
                    finalReport.AppendLine(string.Format("Total de balanços não Ok: {0}", qtdStatementsNok));
                    finalReport.AppendLine(string.Format("*******************************************************************"));

                    finalReport.AppendLine(string.Format("Mensagens"));

                    inconsistency.Add(string.Format("*******************************************************************"));
                    inconsistency.Add(string.Format("Lista de clientes não OK"));
                    foreach (var item in clientsNok)
                    {
                        inconsistency.Add(string.Format("{0};{1}", item.TipoCliente, item.TipoCliente == Helper.Enum.ClienteType.E ? item.Cnpj : item.CodigoGrupoEconomico));
                    }

                    inconsistency.Add(string.Format("*******************************************************************"));
                    inconsistency.Add(string.Format("Lista de balanços não Ok"));
                    foreach (var item in statementsNok)
                    {
                        inconsistency.Add(string.Format("{0};{1};{2};{3};{4};", item.TipoCliente, item.TipoCliente == Helper.Enum.ClienteType.E ? item.Cnpj : item.CodigoGrupoEconomico, item.Referencia.ToShortDateString(), item.Status, item.Tipo));
                    }

                    inconsistency.Add(string.Format("*******************************************************************"));


                    inconsistency.Add(string.Format("Lista de todos os clientes"));
                    foreach (var item in statementKeys)
                    {
                        inconsistency.Add(string.Format("{0};{1}", item.ClientType, item.ClientId));
                    }

                    inconsistency.Add(string.Format("*******************************************************************"));

                    inconsistency.ForEach(f => finalReport.AppendLine(f));

                    string fileName = Global.StatementCheckSummaryFileName(statementFile.Substring(0, statementFile.Length - 4).Substring(statementFile.LastIndexOf('\\') + 1) + "_");

                    File.WriteFile(fileName, Regex.Replace(finalReport.ToString(), @"[\r\n]*^\s*$[\r\n]*", string.Empty, RegexOptions.Multiline), true);
                    #endregion
                }
                catch (Exception ex)
                {
                    Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                    continue;
                }
            }

            string generalReportFileName = Global.StatementCheckSummaryFileName("ResumoGeral_");
            File.WriteFile(generalReportFileName, Regex.Replace(summary.ToString(), @"[\r\n]*^\s*$[\r\n]*", string.Empty, RegexOptions.Multiline), true);

            return true;
        }

        private List<Model.Json.Statement> ReadFile(string path)
        {
            List<Model.Json.Statement> result = new List<Model.Json.Statement>();
            IEnumerable<string> fileReader = null;
            Model.Json.Statement[] statements = null;
            Model.DTO.Statement[] fullStatement = new Model.DTO.Statement[3] { new Model.DTO.Statement(), new Model.DTO.Statement(), new Model.DTO.Statement() };

            uint ordem = 1;
            bool isExplanatoryNote = false;
            bool isDRE = false;
            uint contLine = 0;
            string lineToCheck = string.Empty;
            try
            {
                fileReader = Helper.Generic.File.ReadFile(path);
                foreach (var line in fileReader)
                {
                    lineToCheck = line;
                    if(line== "00000;Circulante;;;7.658")
                    {
                        //aqui
                        var a = "a";
                    }
                    contLine++;
                    var rowFields = line.Split(';');

                    if (rowFields.Length < 2)
                        continue;

                    switch (rowFields[0])
                    {
                        #region Cliente
                        case "CLIENTE":
                            if (statements != null)
                            {
                                result.AddRange(statements);
                            }

                            statements = new Model.Json.Statement[3] { new Model.Json.Statement(), new Model.Json.Statement(), new Model.Json.Statement() };
                            ordem = 1;

                            statements[0].Cliente.TipoCliente = (Helper.Enum.ClienteType)Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[1].Trim() == "C" ? "E" : rowFields[1].Trim(), true);
                            statements[1].Cliente.TipoCliente = (Helper.Enum.ClienteType)Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[1].Trim() == "C" ? "E" : rowFields[1].Trim(), true);
                            statements[2].Cliente.TipoCliente = (Helper.Enum.ClienteType)Enum.Parse(typeof(Helper.Enum.ClienteType), rowFields[1].Trim() == "C" ? "E" : rowFields[1].Trim(), true);
                            if (statements[0].Cliente.TipoCliente.Equals(Helper.Enum.ClienteType.E))
                            {
                                statements[0].Cliente.Cnpj = double.Parse(rowFields[2]);
                                statements[1].Cliente.Cnpj = double.Parse(rowFields[2]);
                                statements[2].Cliente.Cnpj = double.Parse(rowFields[2]);
                            }
                            else
                            {
                                statements[0].Cliente.CodigoGrupoEconomico = double.Parse(rowFields[2]);
                                statements[1].Cliente.CodigoGrupoEconomico = double.Parse(rowFields[2]);
                                statements[2].Cliente.CodigoGrupoEconomico = double.Parse(rowFields[2]);
                            }

                            statements[0].Dados.Status = Helper.Enum.StatementStatus.Definitivo;
                            statements[1].Dados.Status = Helper.Enum.StatementStatus.Definitivo;
                            statements[2].Dados.Status = Helper.Enum.StatementStatus.Definitivo;
                            break;
                        #endregion

                        #region Aba
                        case "ABA":
                            if (rowFields[1].ToUpper().Equals("NOTAEXPLICATIVA"))
                            {
                                isExplanatoryNote = true;
                            }
                            else if (rowFields[1].ToUpper().Equals("DEMONSTRATIVO"))
                            {
                                isDRE = true;
                            }
                            else
                            {
                                isExplanatoryNote = false;
                                isDRE = false;
                            }

                            if (!isExplanatoryNote)
                            {
                                var typeStatus = GetStatementTypeAndStatus(rowFields[2]);
                                statements[0].Dados.Tipo = typeStatus.Key;
                                statements[0].Origem.Descricao = typeStatus.Value;

                                typeStatus = GetStatementTypeAndStatus(rowFields[3]);
                                statements[1].Dados.Tipo = typeStatus.Key;
                                statements[1].Origem.Descricao = typeStatus.Value;

                                typeStatus = GetStatementTypeAndStatus(rowFields[4]);
                                statements[2].Dados.Tipo = typeStatus.Key;
                                statements[2].Origem.Descricao = typeStatus.Value;
                            }
                            break;
                        #endregion

                        #region Code 00000
                        case "00000":
                            Helper.Enum.AccountGroup accountGroup = Helper.Enum.AccountGroup.Ativo;

                            switch (rowFields[1].ToUpper())
                            {
                                case "ATIVO":
                                    accountGroup = Helper.Enum.AccountGroup.Ativo;
                                    break;
                                case "PASSIVO":
                                    accountGroup = Helper.Enum.AccountGroup.Passivo;
                                    break;
                                case "DEMONSTRATIVO":
                                    accountGroup = Helper.Enum.AccountGroup.Dre;
                                    break;
                            }
                            if (isDRE)
                            {
                                statements[0].Dados.Referencia = string.IsNullOrEmpty(rowFields[2].Trim()) ? DateTime.MinValue : new DateTime(int.Parse(rowFields[2].Substring(6, 4)), int.Parse(rowFields[2].Substring(3, 2)), 1, 0, 0, 0, DateTimeKind.Utc);
                                statements[1].Dados.Referencia = string.IsNullOrEmpty(rowFields[3].Trim()) ? DateTime.MinValue : new DateTime(int.Parse(rowFields[4].Substring(6, 4)), int.Parse(rowFields[4].Substring(3, 2)), 1, 0, 0, 0, DateTimeKind.Utc);
                                statements[2].Dados.Referencia = string.IsNullOrEmpty(rowFields[4].Trim()) ? DateTime.MinValue : new DateTime(int.Parse(rowFields[6].Substring(6, 4)), int.Parse(rowFields[6].Substring(3, 2)), 1, 0, 0, 0, DateTimeKind.Utc);
                            }
                            else
                            {
                                statements[0].Dados.Referencia = string.IsNullOrEmpty(rowFields[2].Trim()) ? DateTime.MinValue : new DateTime(int.Parse(rowFields[2].Substring(6, 4)), int.Parse(rowFields[2].Substring(3, 2)), 1, 0, 0, 0, DateTimeKind.Utc);
                                statements[1].Dados.Referencia = string.IsNullOrEmpty(rowFields[3].Trim()) ? DateTime.MinValue : new DateTime(int.Parse(rowFields[3].Substring(6, 4)), int.Parse(rowFields[3].Substring(3, 2)), 1, 0, 0, 0, DateTimeKind.Utc);
                                statements[2].Dados.Referencia = string.IsNullOrEmpty(rowFields[4].Trim()) ? DateTime.MinValue : new DateTime(int.Parse(rowFields[4].Substring(6, 4)), int.Parse(rowFields[4].Substring(3, 2)), 1, 0, 0, 0, DateTimeKind.Utc);
                            }
                            fullStatement[0].Grupo = accountGroup;
                            fullStatement[1].Grupo = accountGroup;
                            fullStatement[2].Grupo = accountGroup;
                            break;
                        #endregion

                        #region Default
                        default:
                            if (isExplanatoryNote)
                            {
                                //Attribute explanatory note only for last period.
                                statements[2].Dados.Contas.NotaExplicativa.Add(new Model.Json.Note { Valor = rowFields[1] });
                            }
                            else
                            {
                                fullStatement[0].Conta = ulong.Parse(rowFields[0].Clean());
                                fullStatement[1].Conta = ulong.Parse(rowFields[0].Clean());
                                fullStatement[2].Conta = ulong.Parse(rowFields[0].Clean());
                                fullStatement[0].Ordem = ordem;
                                fullStatement[1].Ordem = ordem;
                                fullStatement[2].Ordem = ordem;

                                ordem += 1;

                                if (isDRE)
                                {
                                    fullStatement[0].Valor = string.IsNullOrEmpty(rowFields[2].Clean()) ? null : rowFields[2].Clean();
                                    fullStatement[1].Valor = string.IsNullOrEmpty(rowFields[4].Clean()) ? null : rowFields[4].Clean();
                                    fullStatement[2].Valor = string.IsNullOrEmpty(rowFields[6].Clean()) ? null : rowFields[6].Clean();
                                    fullStatement[0].Porcentagem = string.IsNullOrEmpty(rowFields[3].Clean()) ? (double?)null : double.Parse(rowFields[3].Clean().Replace('.', ','));
                                    fullStatement[1].Porcentagem = string.IsNullOrEmpty(rowFields[5].Clean()) ? (double?)null : double.Parse(rowFields[5].Clean().Replace('.', ','));
                                    fullStatement[2].Porcentagem = string.IsNullOrEmpty(rowFields[7].Clean()) ? (double?)null : double.Parse(rowFields[7].Clean().Replace('.', ','));
                                }
                                else
                                {
                                    fullStatement[0].Valor = string.IsNullOrEmpty(rowFields[2].Clean()) ? null : rowFields[2].Clean();
                                    fullStatement[1].Valor = string.IsNullOrEmpty(rowFields[3].Clean()) ? null : rowFields[3].Clean();
                                    fullStatement[2].Valor = string.IsNullOrEmpty(rowFields[4].Clean()) ? null : rowFields[4].Clean();
                                }
                                AddStatementLine(fullStatement, ref statements);
                                fullStatement = new Model.DTO.Statement[3] { new Model.DTO.Statement(), new Model.DTO.Statement(), new Model.DTO.Statement() };
                            }
                            break;
                            #endregion
                    }
                }
                result.AddRange(statements);
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, string.Format("Erro na leitura da linha {0} - {1} em {2}", contLine, lineToCheck, new StackTrace().GetFrame(0).GetMethod().Name));
                throw;
            }

            var removed = result.RemoveAll(f => f.Origem.Descricao == null);
            return result;
        }

        private bool AddStatementLine(Model.DTO.Statement[] statementToAdd, ref Model.Json.Statement[] statements)
        {
            #region  Ordena as constas existentes em cada entiade antes de adicionar o balanço
            for (int i = 0; i < 3; i++)
            {

                switch (statementToAdd[i].Grupo)
                {
                    case Helper.Enum.AccountGroup.Ativo:
                        statements[i].Dados.Contas.Ativo.Add(new Model.Json.Ativo
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                    case Helper.Enum.AccountGroup.Passivo:
                        statements[i].Dados.Contas.Passivo.Add(new Model.Json.Passivo
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                    case Helper.Enum.AccountGroup.Dre:
                        statements[i].Dados.Contas.Dre.Add(new Model.Json.Dre
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Porcentagem = statementToAdd[i].Porcentagem,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                    case Helper.Enum.AccountGroup.Mutacao:
                        statements[i].Dados.Contas.Mutacao.Add(new Model.Json.Mutacao
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                    case Helper.Enum.AccountGroup.FluxoCaixa:
                        statements[i].Dados.Contas.FluxoCaixa.Add(new Model.Json.FluxoCaixa
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                    case Helper.Enum.AccountGroup.Indicadores:
                        statements[i].Dados.Contas.Indicadores.Add(new Model.Json.Indicadores
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                    case Helper.Enum.AccountGroup.Highlights:
                        statements[i].Dados.Contas.Highlights.Add(new Model.Json.Highlights
                        {
                            Conta = statementToAdd[i].Conta,
                            Valor = statementToAdd[i].Valor,
                            Ordem = statementToAdd[i].Ordem
                        });
                        break;
                }
            }
            #endregion
            return true;
        }
        #endregion Public Methods
    }
}
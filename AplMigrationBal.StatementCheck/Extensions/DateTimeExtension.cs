using System;
using System.Globalization;

namespace AplMigrationBal.Helper.Extensions
{
    public class DateTimeExtension
    {
        #region Public Methods

        public static DateTime ParseExact(string @string, CultureInfo formatProvider)
        {
            return DateTime.ParseExact(@string,
                                       formatProvider.DateTimeFormat.GetAllDateTimePatterns(),
                                       formatProvider,
                                       DateTimeStyles.None);
        }

        #endregion Public Methods
    }
}
﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AplMigrationBal.Business;
using AplMigrationBal.Helper;
using System.Collections.Generic;
using AplMigrationBal.Business.RiskIntegration;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class RiskIntervalSemesterGeneratorTest
    {
        private RiskIntervalPeriodsOfTimeGenerator _riskDateGenerator;
        private DateTime _today;

        [TestInitialize]
        public void TestInitialize()
        {
            this._today = new DateTime(2018, 08, 29, 0, 0, 0);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackOneSemester()
        {
            this._riskDateGenerator = new RiskIntervalSemesterGenerator(1,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2018, 06, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[1]);
            Assert.AreEqual(dates.Count, 2);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackTwoSemesters()
        {
            this._riskDateGenerator = new RiskIntervalSemesterGenerator(2,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2018, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[3]);
            Assert.AreEqual(dates.Count, 4);
        }


        [TestMethod]
        public void TestWhenGenerateDatesCommingBackFiveSemesters()
        {
            this._riskDateGenerator = new RiskIntervalSemesterGenerator(5,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2016, 06, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[9]);
            Assert.AreEqual(dates.Count, 10);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackTenSemesters()
        {
            this._riskDateGenerator = new RiskIntervalSemesterGenerator(10,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2014, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[19]);
            Assert.AreEqual(dates.Count, 20);
        }


        [TestMethod]
        public void TestWhenGenerateDatesCommingBackZeroSemesters()
        {
            this._riskDateGenerator = new RiskIntervalSemesterGenerator(0, this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2003, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[63]);
            Assert.AreEqual(dates.Count, 64);
        }
    }
}

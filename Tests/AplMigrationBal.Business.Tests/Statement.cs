﻿using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Model.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class Statement
    {
        [TestMethod]
        public void GenerateStatementDateFile()
        {
            Global.SetEnvironment(Helper.Enum.Environment.Producao);
            Assert.IsTrue(new Business.Statement().SaveStatementDateFile(Global.StatementDatePath, Global.StatementDateFile));
        }

        [TestMethod]
        public void StartImport()
        {
            InputImportSettings inputImportSettings = new InputImportSettings();

            inputImportSettings.Environment.Add(Helper.Enum.Environment.Homologacao);
            inputImportSettings.FromTo = @"C:\Projetos\apl-migracao-bal-dev\CSVFiles\Config\PlanoContasDeParaMF.csv";

            inputImportSettings.StatementSource = Helper.Enum.StatementSource.ImportacaoMainframe;
            inputImportSettings.Statement.Add(@"C:\Projetos\apl-migracao-bal-dev\CSVFiles\Data\MF\MF2016.csv");
            inputImportSettings.ExplanatoryNote.Add(@"C:\Projetos\apl-migracao-bal-dev\CSVFiles\Data\MF\NEX_MF2016.csv");

            inputImportSettings.JsonOutputPath = Global.JsonOutputPath;
            inputImportSettings.StatementStatus = Helper.Enum.StatementStatus.Todos;
            inputImportSettings.ClientType = Helper.Enum.ClienteType.Todos;

            inputImportSettings.ClientId = new System.Collections.Generic.List<double>
            {
                21314559
            };

            Assert.IsTrue(new Business.Import().StartImport(inputImportSettings));
        }

        [TestMethod()]
        public void CheckDuplicatedRecords()
        {
            Global.SetEnvironment(Helper.Enum.Environment.Homologacao);
            Assert.IsFalse(new Business.Statement().CheckDuplicatedStatements());
        }

        [TestMethod()]
        public void DeleteDuplicatedRecords()
        {
            Global.SetEnvironment(Helper.Enum.Environment.Producao);
            Assert.IsTrue(new Business.Statement().DeleteDuplicatedStatements());
        }
    }
}

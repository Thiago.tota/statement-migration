﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AplMigrationBal.Business;
using AplMigrationBal.Helper;
using System.Collections.Generic;
using AplMigrationBal.Business.RiskIntegration;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class RiskIntervalTrimesterGeneratorTest
    {
        private RiskIntervalPeriodsOfTimeGenerator _riskDateGenerator;
        private DateTime _today;

        [TestInitialize]
        public void TestInitialize()
        {
            this._today = new DateTime(2018, 09, 29, 0, 0, 0);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackOneTrimester()
        {
            this._riskDateGenerator = new RiskIntervalTrimesterGenerator(1,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            Assert.AreEqual(dates.Count, 2);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackTwoTrimester()
        {
            this._riskDateGenerator = new RiskIntervalTrimesterGenerator(2,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            Assert.AreEqual(dates.Count, 4);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackFiveTrimesters()
        {
            this._riskDateGenerator = new RiskIntervalTrimesterGenerator(5,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            Assert.AreEqual(dates.Count, 7);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackThirtyTrimesters()
        {
            this._riskDateGenerator = new RiskIntervalTrimesterGenerator(30,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            Assert.AreEqual(dates.Count, 32);
        }
    }
}

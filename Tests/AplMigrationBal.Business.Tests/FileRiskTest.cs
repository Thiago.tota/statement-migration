﻿using AplMigrationBal.Business.RiskIntegration.FileGeneratorComponents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AplMigrationBal.Model.Json;
using AplMigrationBal.Model.Interface;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class FileRiskTest
    {
        private List<AplMigrationBal.Model.Json.Statement> data;
        private DateTime baseDate;

        [TestInitialize]
        public void TestInitialize()
        {
            if(data==null)
            {
                data = JsonConvert.DeserializeObject<List<AplMigrationBal.Model.Json.Statement>>(File.ReadAllText("dados.json"));
            }
           
            baseDate = new DateTime(2018, 08, 29, 0, 0, 0);
        }

        [TestMethod]
        public void TestHeaderGeneration()
        {
         
            RiskContentGeneratorBase headerFile = new HeaderRiskFileContentGenerator(baseDate);

            string header = headerFile.GenerateContent();

            string expectedHeader = "0020180829";

            Assert.AreEqual(header, expectedHeader);
        }

        [TestMethod]
        public void TestHeaderClientGeneration()
        {
            var balance = data[0];
            RiskContentGeneratorBase headerClient = new HeaderRiskClientContentGenerator(balance);
            string header = headerClient.GenerateContent();

            string expectedHeader = "01C002918654";

            Assert.AreEqual(header, expectedHeader);

        }

        [TestMethod]
        public void TestHeaderBalanceClientGeneration()
        {
            var balance = data[0];

            RiskContentGeneratorBase headerBalance = new HeaderRiskBalanceContentGeneration(balance);

            string header = headerBalance.GenerateContent();

            string expectedHeader = "02201807BLCDAC20180818P";

            Assert.AreEqual(header, expectedHeader);
        }

        [TestMethod]
        public void TestAtivoGeneration()
        {
            var balance = data[0];

            var ativo = balance.Dados.Contas.Ativo[0];

            RiskContentGeneratorBase account = new ContentAccountRiskGenerator((IStatement)ativo, 0);
            string content = "0311000+0000000119225000,0";

            string expectedContent = account.GenerateContent();

            Assert.AreEqual(content, expectedContent);

        }

        [TestMethod]
        public void TestPassivoGeneration()
        {
            var balance = data[0];
            var passivo = balance.Dados.Contas.Passivo[0];

            RiskContentGeneratorBase account = new ContentAccountRiskGenerator((IStatement)passivo, 0);
            string content = "0321000+0000000065136000,0";

            string expectedContent = account.GenerateContent();

            Assert.AreEqual(content, expectedContent);
        }

        [TestMethod]
        public void TestDreGeneration()
        {
            var balance = data[0];
            var dre = balance.Dados.Contas.Dre[0];

            RiskContentGeneratorBase account = new ContentAccountRiskGenerator((IStatement)dre, dre.Porcentagem);
            string content = "0331000+0000000294576011,3";

            string expectedContent = account.GenerateContent();

            Assert.AreEqual(content, expectedContent);

        }

        [TestMethod]
        public void TestMutacaoGeneration()
        {
            var balance = data[0];
            var mutacao = balance.Dados.Contas.Mutacao[0];
            RiskContentGeneratorBase account = new ContentAccountRiskGenerator((IStatement)mutacao, 0);
            string content = "0341000+0000000061445000,0";

            string expectedContent = account.GenerateContent();

            Assert.AreEqual(content, expectedContent);

        }

        [TestMethod]
        public void TestTraillerGeneration()
        {
            RiskContentGeneratorBase trailler = new TrailerRiskContentGenerator(15);
            string content = "99000015";

            string expectedContent =trailler.GenerateContent();

            Assert.AreEqual(content, expectedContent);
        }
    }
}

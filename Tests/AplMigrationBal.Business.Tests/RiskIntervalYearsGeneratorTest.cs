﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AplMigrationBal.Business;
using AplMigrationBal.Helper;
using System.Collections.Generic;
using AplMigrationBal.Business.RiskIntegration;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class RiskIntervalYearsGeneratorTest
    {
        private RiskIntervalPeriodsOfTimeGenerator _riskDateGenerator;
        private DateTime _today;

        [TestInitialize]
        public void TestInitialize()
        {
            this._today = new DateTime(2018, 08, 29, 0, 0, 0);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackOneYer()
        {
            this._riskDateGenerator = new RiskIntervalYearsGenerator(1,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2018, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[1]);

            Assert.AreEqual(dates.Count, 2);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackTwoYears()
        {
            this._riskDateGenerator = new RiskIntervalYearsGenerator(2,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2017, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[3]);

            Assert.AreEqual(dates.Count, 4);
        }


        [TestMethod]
        public void TestWhenGenerateDatesCommingBackFiveYears()
        {
            this._riskDateGenerator = new RiskIntervalYearsGenerator(5,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();

            var firstExpectedDate = new DateTime(2014, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[9]);

            Assert.AreEqual(dates.Count, 10);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackTenYears()
        {
            this._riskDateGenerator = new RiskIntervalYearsGenerator(10,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();

            var firstExpectedDate = new DateTime(2009, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[19]);

            Assert.AreEqual(dates.Count, 20);
        }
    }
}

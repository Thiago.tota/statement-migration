﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AplMigrationBal.Business;
using AplMigrationBal.Helper;
using System.Collections.Generic;
using AplMigrationBal.Business.RiskIntegration;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class RiskIntervalMonthGeneratorTest
    {
        private RiskIntervalPeriodsOfTimeGenerator _riskDateGenerator;
        private DateTime _today;

        [TestInitialize]
        public void TestInitialize()
        {
            this._today = new DateTime(2018, 08, 29, 0, 0, 0);
        }

        [TestMethod]
        public void TestWhenGenerateWithOneMonth()
        {
            this._riskDateGenerator = new RiskIntervalMonthsGenerator(1,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();

            var firstExpectedDate = new DateTime(2018, 08, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[1]);

            Assert.AreEqual(dates.Count, 2);
        }

        [TestMethod]
        public void TestWhenGenerateWithTwoMonth()
        {
            this._riskDateGenerator = new RiskIntervalMonthsGenerator(2,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2018, 07, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[3]);

            Assert.AreEqual(dates.Count, 4);
        }


        [TestMethod]
        public void TestWhenGenerateDatesCommingBackFiveMonths()
        {
            this._riskDateGenerator = new RiskIntervalMonthsGenerator(5,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2018, 04, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[9]);

            Assert.AreEqual(dates.Count, 10);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingBackTenMonths()
        {
            this._riskDateGenerator = new RiskIntervalMonthsGenerator(10,this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2017, 11, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[19]);

            Assert.AreEqual(dates.Count, 20);
        }

        [TestMethod]
        public void TestWhenGenerateDatesCommingZeroMonths()
        {
            this._riskDateGenerator = new RiskIntervalMonthsGenerator(0, this._today);
            List<DateTime> dates = this._riskDateGenerator.GenerateDates();
            var firstExpectedDate = new DateTime(2003, 01, 01);

            var endExpectedDate = new DateTime(2018, 08, 29);

            Assert.AreEqual(firstExpectedDate, dates[0]);
            Assert.AreEqual(endExpectedDate, dates[375]);

            Assert.AreEqual(dates.Count, 376);
        }
    }
}

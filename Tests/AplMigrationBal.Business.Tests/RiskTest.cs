﻿using AplMigrationBal.Business.RiskIntegration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AplMigrationBal.Business.Tests
{
    [TestClass]
    public class RiskTest
    {
        private Risk _risk;
        private DateTime EndDate;

        [TestInitialize]
        public void InitializeTest()
        {
            EndDate = DateTime.Today;
        }

        [TestMethod]
        public void TestGenerateFileByOneMonth()
        {
            var datesGenerator = RiskIntervalPeriodFactory.GetInstance(EndDate, 2, Helper.Enum.RiskSplitFile.Y);
            _risk = new Risk(new Business.Statement(), datesGenerator, Helper.Enum.Environment.Producao);
            bool result = _risk.GenerateFile();
            Assert.AreEqual(true, result);
        }
    }
}

﻿using AplMigrationBal.Helper.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AplMigrationBal.Persistence.MongoDb.Tests
{
    internal class Teste
    {
        #region Public Properties

        [BsonId()]
        [BsonElement("_id")]
        public ObjectId Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        #endregion Public Properties
    }

    [TestClass()]
    public class MongoDbSchemaTests
    {
        #region Private Fields

        private readonly string databaseName = Global.MongoDbDatabaseNameBal;
        private readonly string serverUrl = Global.MongoDbUrlBal;

        #endregion Private Fields

        #region Public Methods

        [TestMethod()]
        public void MongoDbSchemaTest()
        {
            var connection = new MongoDbConnection(serverUrl);
            var database = new MongoDbDatabase(connection, databaseName);
            var schema = new MongoDbSchema<Teste>(database);
            var documents = schema.Documents;

            documents.InsertOne(new Teste { Name = "Teste" });
        }

        [TestMethod()]
        public void MongoDbSchemaTest2()
        {
            var database = new MongoDbDatabase(serverUrl, databaseName);
            var schema = new MongoDbSchema<Teste>(database);
            var documents = schema.Documents;

            documents.InsertOne(new Teste { Name = "Teste2" });
        }

        [TestMethod()]
        public void MongoDbSchemaTest3()
        {
            var documents = (new MongoDbSchema<Teste>(serverUrl, databaseName)).Documents;

            documents.InsertOne(new Teste { Name = "Teste3" });
        }

        [TestMethod()]
        public void MongoDbSchemaTest4()
        {
            var documents = (new MongoDbSchema<Teste>(serverUrl, databaseName)).Documents;

            documents.InsertOne(new Teste { Name = "Teste4" });
        }

        #endregion Public Methods
    }
}
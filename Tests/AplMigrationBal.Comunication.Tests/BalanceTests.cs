﻿using AplMigrationBal.Helper.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace AplMigrationBal.Comunication.Tests
{
    [TestClass()]
    public class BalanceTests
    {
        #region Public Methods

        [TestMethod()]
        public void SendTest()
        {
            var statement = Singleton.Instance();
            var fileContent = string.Join("", File.ReadFile("Balanco.json"));
            var statements = Json.Deserialize<List<Model.Json.Statement>>(fileContent);

            statement.SaveStatement(statements);
        }

        #endregion Public Methods
    }
}
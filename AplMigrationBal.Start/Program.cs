﻿using AplMigrationBal.Business.RiskIntegration;
using AplMigrationBal.Helper.Generic;
using AplMigrationBal.Helper.Logger;
using AplMigrationBal.Model.DTO;
using AplMigrationBal.View;
using System;
using System.Diagnostics;
using System.Linq;

namespace AplMigrationBal.Start
{
    class Program
    {
        enum Action : byte
        {
            Migration = 1,
            StatementDate = 2,
            Risk = 3
        }

        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Inicializando aplicação");
                if (args.Length <= 0)
                {
                    Console.WriteLine("Parâmetros não informados, abrindo menu visual");
                    System.Windows.Application app = new System.Windows.Application();
                    app.Run(new MainWindow());
                }
                else
                {
                    Action action = (Action)Enum.Parse(typeof(Action), args[0], true);

                    switch (action)
                    {
                        case Action.Migration: //1 TI PlanoContasDePara.csv ImportacaoSQL SQL2018_BAL.csv SQL2018_NEX.csv (Action | Environment | FromTo | StatementFile | ExplanatoryNoteFile)
                            InputImportSettings inputImportSettings = new InputImportSettings();

                            inputImportSettings.Environment.Add((Helper.Enum.Environment)System.Enum.Parse(typeof(Helper.Enum.Environment), args[1].Trim(), true));
                            inputImportSettings.FromTo = Global.AccountPlanPath + args[2];//Banco

                            //ImportacaoMainframe, ImportacaoSQL, ImportacaoSerasa
                            inputImportSettings.StatementSource = (Helper.Enum.StatementSource)System.Enum.Parse(typeof(Helper.Enum.StatementSource), args[3].Trim(), true);
                            args[4].Split(';').ToList().ForEach(f => inputImportSettings.Statement.Add(Global.DataPath + f.Trim()));

                            inputImportSettings.JsonOutputPath = Global.JsonOutputPath;
                            inputImportSettings.StatementStatus = Helper.Enum.StatementStatus.Todos;
                            inputImportSettings.ClientType = Helper.Enum.ClienteType.Todos;

                            if (args.Length > 5)
                            {
                                args[5].Split(';').ToList().ForEach(f => inputImportSettings.ExplanatoryNote.Add(Global.DataPath + f.Trim()));
                                inputImportSettings.HasExplanatoryNotes = true;
                            }
                            else
                            {
                                inputImportSettings.ExplanatoryNote = new System.Collections.Generic.List<string>();
                                inputImportSettings.HasExplanatoryNotes = false;
                            }

                            new Business.Import().StartImport(inputImportSettings);
                            break;

                        case Action.StatementDate: //2 TI (Action Environment)
                            Global.SetEnvironment((Helper.Enum.Environment)System.Enum.Parse(typeof(Helper.Enum.Environment), args[1].Trim(), true));
                            new Business.Statement().SaveStatementDateFile(Global.StatementDatePath, Global.StatementDateFile);
                            break;

                        case Action.Risk://3 TI 0 M (Action | Environment | PeriodsLength (if 0 get all) | Split (Y: yearly, S: semiannually,  M: monthly)
                            {
                                Helper.Enum.Environment enviroment = (Helper.Enum.Environment)System.Enum.Parse(typeof(Helper.Enum.Environment), args[1].Trim(), true);
                                int periodOfTime = Int32.Parse(args[2].Trim());
                                Helper.Enum.RiskSplitFile riskSplitFile= (Helper.Enum.RiskSplitFile)System.Enum.Parse(typeof(Helper.Enum.RiskSplitFile), args[3].Trim(), true);

                                RiskIntervalPeriodsOfTimeGenerator datesGenerator = RiskIntervalPeriodFactory.GetInstance(DateTime.Now, periodOfTime ,riskSplitFile);
                                new Business.Risk(new Business.Statement(),datesGenerator, enviroment ).GenerateFile();
                            break;
                            }
                           
                    }
                }
                //Console.WriteLine("");
                //Console.WriteLine("Pressione qualquer tecla para finalizar");
                //Console.ReadLine();
            }
            catch (Exception ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw;
            }
        }
    }
}

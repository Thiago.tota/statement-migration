﻿using AplMigrationBal.Helper.Logger;
using MongoDB.Driver;
using System;
using System.Diagnostics;

namespace AplMigrationBal.Persistence.MongoDb
{
    public sealed class MongoDbSchema<TSchema>
        where TSchema : class
    {
        #region Private Fields

        private MongoDbDatabase _database;
        private IMongoCollection<TSchema> _documents;
        private string _collection;

        #endregion Private Fields

        #region Public Constructors

        public MongoDbSchema(MongoDbDatabase database)
        {
            _database = database;
            _collection = typeof(TSchema).Name;
        }

        public MongoDbSchema(MongoDbDatabase database, string collection)
        {
            _database = database;
            _collection = collection;
        }

        public MongoDbSchema(MongoDbConnection connection, string databaseName)
            : this(new MongoDbDatabase(connection, databaseName)) { }

        public MongoDbSchema(MongoDbConnection connection, string databaseName, string collection)
            : this(new MongoDbDatabase(connection, databaseName), collection) { }

        public MongoDbSchema(MongoClientSettings connectionSettings, string databaseName)
            : this(new MongoDbConnection(connectionSettings), databaseName) { }

        public MongoDbSchema(MongoClientSettings connectionSettings, string databaseName, string collection)
            : this(new MongoDbConnection(connectionSettings), databaseName, collection) { }

        public MongoDbSchema(MongoUrl connectionUrl, string databaseName)
            : this(new MongoDbConnection(connectionUrl), databaseName) { }

        public MongoDbSchema(MongoUrl connectionUrl, string databaseName, string collection)
            : this(new MongoDbConnection(connectionUrl), databaseName, collection) { }

        public MongoDbSchema(string connectionString, string databaseName)
            : this(new MongoDbConnection(connectionString), databaseName) { }

        public MongoDbSchema(string connectionString, string databaseName, string collection)
            : this(new MongoDbConnection(connectionString), databaseName, collection) { }

        #endregion Public Constructors

        #region Public Properties

        public IMongoCollection<TSchema> Documents
        {
            get { return GetDocuments(); }
        }

        #endregion Public Properties

        #region Private Methods

        private IMongoCollection<TSchema> GetDocuments()
        {
            try
            {
                _documents = _documents
                           ?? _database
                                .GetDatabase()
                                .GetCollection<TSchema>(_collection);
            }
            catch (MongoClientException ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw (new ArgumentException($"Error trying to get documents with the collection name '{_collection}'!", ex));
            }

            return _documents;
        }

        #endregion Private Methods
    }
}
﻿using AplMigrationBal.Helper.Logger;
using MongoDB.Driver;
using System;
using System.Diagnostics;

namespace AplMigrationBal.Persistence.MongoDb
{
    public sealed class MongoDbDatabase
    {
        #region Private Fields

        private MongoDbConnection _connection;
        private IMongoDatabase _database;
        private string _name;

        #endregion Private Fields

        #region Private Constructors

        private MongoDbDatabase()
        {
        }

        #endregion Private Constructors

        #region Public Constructors

        public MongoDbDatabase(MongoDbConnection connection, string name)
        {
            _connection = connection;
            _name = name;
        }

        public MongoDbDatabase(MongoClientSettings connectionSettings, string name)
            : this(new MongoDbConnection(connectionSettings), name) { }

        public MongoDbDatabase(MongoUrl connectionUrl, string name)
            : this(new MongoDbConnection(connectionUrl), name) { }

        public MongoDbDatabase(string connectionString, string name)
            : this(new MongoDbConnection(connectionString), name) { }

        #endregion Public Constructors

        #region Internal Methods

        internal IMongoDatabase GetDatabase()
        {
            try
            {
                _database = _database
                         ?? _connection
                                .GetClient()
                                .GetDatabase(_name);
            }
            catch (MongoClientException ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw (new ArgumentException($"Error trying to get database '{_name}'!", ex));
            }

            return _database;
        }

        #endregion Internal Methods
    }
}
﻿using AplMigrationBal.Helper.Logger;
using MongoDB.Driver;
using System;
using System.Diagnostics;

namespace AplMigrationBal.Persistence.MongoDb
{
    public sealed class MongoDbConnection
    {
        #region Private Fields

        private IMongoClient _client;

        #endregion Private Fields

        #region Public Constructors

        public MongoDbConnection(MongoClientSettings settings)
        {
            Settings = settings;
        }

        public MongoDbConnection(MongoUrl url)
            : this(MongoClientSettings.FromUrl(url)) { }

        public MongoDbConnection(string connectionString)
            : this(MongoClientSettings.FromUrl(MongoUrl.Create(connectionString))) { }

        #endregion Public Constructors

        #region Public Properties

        public MongoClientSettings Settings { get; private set; }

        #endregion Public Properties

        #region Internal Methods

        internal IMongoClient GetClient()
        {
            try
            {
                _client = _client ?? new MongoClient(Settings);
            }
            catch (MongoClientException ex)
            {
                Log.Instance.Error(ex, new StackTrace().GetFrame(0).GetMethod().Name);
                throw (new ArgumentException($"Error trying to connect to server using current construction argument!\n\nSettings identified:\n{Settings?.ToString()}", ex));
            }

            return _client;
        }

        #endregion Internal Methods
    }
}